import os
import importlib
from invoke import task, Collection
from termcolor import colored

import ado
project_path = os.path.dirname(ado.__file__)


# Colors
green = lambda s: colored(s, 'green', attrs=['bold']) # noqa
red = lambda s: colored(s, 'red', attrs=['bold']) # noqa
yellow = lambda s: colored(s, 'yellow', attrs=['bold']) # noqa


def django_setup():
    import django
    django.setup()


# Utils
def get_git_hash(c):
    """
    Get the current hash for the git HEAD
    """
    result = c.run('git rev-parse --short HEAD', hide=True)
    return result.stdout.strip()


def _generate_secret_key():
    from django.utils.crypto import get_random_string
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(50, chars)
    return secret_key


@task
def generate_secret_key(c):
    """
    Print a random string for use in django SECRET_KEY setting
    """
    print(_generate_secret_key())


@task
def add_superuser(c, username, pw):
    import django
    from django.apps import apps
    django.setup()
    auth_app = apps.get_app_config('auth')
    User = auth_app.get_model('User')

    try:
        User.objects.get(username=username)
        print(yellow(f"User '{username}' already exists"))
    except User.DoesNotExist:
        user = User(
            username=username,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            email='')
        user.set_password(pw)
        user.save()


def load_initial_data(c):
    """
    Loads test data into the database for development
    """
    add_superuser(c, 'admin', 'admin')

    path = os.path.join(project_path, 'local', 'initial_data.json')
    if os.path.exists(path):
        c.run('manage.py loaddata', '-i', path)


@task
def bootstrap(c):
    """
    Initializes the django project for development
    """
    print(green('\nSyncing database & running migrations:'))
    c.run('./manage.py migrate')

    print(green('\nLoading initial data:'))
    load_initial_data(c)

    print(green('\nAll Done!'))
    print(green('Next steps:'))
    print('1) ./manage.py runserver')
    print('2) visit http://localhost:8000/')


# Testing
@task(help={
    'module': 'python module to find tests',
    'settings': 'django settings module',
    'watch': 'run tests when code changes',
    'opts': 'options to pass to django tests',
})
def test(c,
         module='ado',
         settings='ado.config.tests',
         watch=False,
         opts='--traceback --failfast'):
    """
    Run django tests
    """
    django_setup()

    test_command = f'django-admin test --settings={settings} {module} {opts}'

    c.run(test_command, warn=True, echo=True, pty=True)
    if watch:
        module_to_test = importlib.import_module(module.split('.')[0])
        inot_paths = set([
            os.path.dirname(module_to_test.__file__),
            project_path,
        ])
        inot_path_arg = ' '.join(list(inot_paths))
        inot_command = (
            'while true; do '  # noqa
            'inotifywait -qr --exclude \'.*[^\.py]$\' '
            '-e modify,attrib,close_write,move,create,delete '
            f'{inot_path_arg} && {test_command}; done;')

        c.run(inot_command, warn=True, pty=True)


# root namespace
ns = Collection(
    test,
    generate_secret_key,
)
