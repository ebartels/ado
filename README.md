Introduction
============

The ado package is a set of Django apps and utilities for adding some more CMS-like
functionality to the django admin.


Dependencies
------------

The works with Django 2.2.x and Python 3.6.x


ado.media
==============

The media app allows handling of images, files and videos in a central media
library.  Any time one of these is required, you can create a link to the one
of the media apps's models rather than making a separate file upload field.

Models:

`media.models.MediaItem`

This is the base class for other media types.  The media app uses Django's
model inheritance, so you can query for all media items with
`MediaItem.objects.all()`

`media.models.File`

A generic file type.  This requires an uploaded file, which is stored in the media library.

`media.models.Image`

An image type.  This also requires the image to be uploaded and stored in the media library.


`media.models.Video`

This type is a reference to an external video.  Currently youtube & vimeo are
supported.  A Video is created by giving the youtube/vimeo url, and the model
will fetch the video information using oembed.


The media library should be used any time a one of these media types is
required.  Rather than making a separate file field on a model, use a foreign
key link to the media library instead.

As an example, a gallery site might have a an Artist model, where each artist
has a representative image.


    class Artist(models.Model):
        name = models.CharField(max_length=255)
        image = models.ForeignKey('media.Image')


`media.fields.related.RelatedMediaField`  
`media.models.RelatedFilesField`  
`media.models.RelatedImagesField`  
`media.models.RelatedVideosField`  

These fields allow you to create a many-to-many association between media
library instances and another model.  This is useful if you need a set of items
for a model, like a set of images for a gallery model. By default, these fields
use Django's generic relations under the hood, so there are no separate
database tables created

To extend the Artist example, if you wanted to add a set of images to your Artist model:


    from ado.media.models import RelatedImagesField

    class Artist(models.Model):
        name = models.CharField(max_length=255)
        image = models.ForeignKey('media.Image')
        images = RelatedImagesField()


Now you can treat the `Artist.images` field as though it were a Django ManyToManyField.

    artist = Artist.objects.all()[0]
    print artist.images.all()
    [<Image: Image: Spelterini Al Ashraf (Spelterini_Al_Ashraf.jpg)>,
     <Image: Image: Hieroglyphs (Hieroglyphs.jpg)>, ...]

    artist.images.count()
    10

    artist.images.add(image)

    artist.images.clear()
    artist.images.count()
    0


`ado.media.admin.options.RelatedFilesInline`  
`ado.media.admin.options.RelatedImagesInline`  
`ado.media.admin.options.RelatedVideosInline`  

Provides a custom admin inline for related media items:

example:
    

    from ado.media.admin.options import RelatedImagesInline

    class ArtistImagesInline(RelatedImagesInline):
        verbose_name_plural = 'Images'
        verbose_name = 'Image'

    class ArtistAdmin(admin.ModelAdmin):
        ...
        inlines = [CollectionImagesInline]


ado.customadmin
============

Provides a custom theme for the Django admin, as well as customization for the admin index pages.

ado.examples
========

Contains a few example apps.

ado.examples.content
===============
A simple app that defines a page model.

ado.examples.blog
=============
A simple blog app.  Should be pretty self-explanatory if you look at the code.

ado.examples.portfolio
==================
An example image portfolio app

ado.examples.artists
================
An example artists app

ado.examples.exhibitions
====================
An example app used for gallery exhibitions
