"""
Project settings file for ado

This is the bootstrap settings file. settings below are configurable using
environment variables. Most settings changes should not be made here but
instead in one of the proper settings files in the "config" directory.

config directory files:

    config/defaults.py:      base settings
    config/dev.py:           settings overrides for development
    config/production.py     settings overrides for production/deployment
    config/tests.py          settings overrides for test runner
    config/local.py          Not to be checked into version control, use for
                             private settings (e.g. passwords, SECRET_KEY, etc)

"""
import os
import environ
import warnings
from django.core.exceptions import ImproperlyConfigured


# read environment vars from .env files
root = environ.Path(__file__) - 1
env_paths = [root('../../.env'), root('../.env')]
for env_path in env_paths:
    if os.path.exists(root(env_path)):
        environ.Env.read_env(env_path)

# import either production or dev settings
# Set environment variable DJANGO_ENV (defaults to dev)
# e.g. export DJANGO_ENV=production
ENVIRONMENT = environ.Env()('DJANGO_ENV', default='dev')
if ENVIRONMENT == 'dev':
    from .config.dev import *
else:
    from .config.production import *

# import any local settings
try:
    from .config.local import *
except ImportError:
    pass

# Default environment settings
env = environ.Env(
    DEBUG=(bool, DEBUG),
    INTERNAL_IPS=(list, locals().get('INTERNAL_IPS', tuple())),
    CANONICAL_HTTP_HOST=(str, locals().get('CANONICAL_HTTP_HOST', '')),
    VALID_HTTP_HOSTS=(list, locals().get('VALID_HTTP_HOSTS', [])),
    AWS_ACCESS_KEY_ID=(str, locals().get('AWS_ACCESS_KEY_ID', '')),
    AWS_SECRET_ACCESS_KEY=(str, locals().get('AWS_SECRET_ACCESS_KEY', '')),
)

# deubg
DEBUG = env("DEBUG", default=ENVIRONMENT == 'dev')

# secret key
try:
    SECRET_KEY = env('SECRET_KEY')
except ImproperlyConfigured:
    SECRET_KEY = locals().get('SECRET_KEY', None)
    if SECRET_KEY is None:
        SECRET_KEY = '__SECRET_KEY__'
        if ENVIRONMENT != 'dev':
            warnings.warn('The SECRET_KEY variable is not set')

# Databases
try:
    DATABASES = {
        'default': env.db()
    }
except ImproperlyConfigured:
    if locals().get('DATABASES') is None:
        raise

# Caches
try:
    default_cache = env.cache()
    if locals().get('CACHES'):
        for key, val in default_cache.items():
            if val:
                if key == 'OPTIONS' and key in CACHES['default']:
                    CACHES['default'][key].update({key: val})
                else:
                    CACHES['default'][key] = val
    else:
        CACHES = {
            'default': default_cache
        }
except ImproperlyConfigured:
    if locals().get('CACHES') is None:
        raise


INTERNAL_IPS = env('INTERNAL_IPS')

# Host redirect middleware
CANONICAL_HTTP_HOST = env('CANONICAL_HTTP_HOST')
VALID_HTTP_HOSTS = env('VALID_HTTP_HOSTS')

# AWS credientials
AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY')
