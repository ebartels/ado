"""
Default settings for ado
"""
import environ
import random
import string

DEBUG = False

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
MANAGERS = ADMINS

DEFAULT_FROM_EMAIL = 'Default Email <name@foo.com>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_SUBJECT_PREFIX = '[foo.com]'

# Project paths
root = environ.Path(__file__) - 2
BASE_DIR = root()

# Apps
INSTALLED_APPS = [
    'ado.customadmin',
    'django.contrib.admin',
    'ado.customadmin.apps.AuthConfig',  # 'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'polymorphic',
    'taggit',
    'ckeditor',
    'webpack_loader',
    'mptt',

    'ado.menus',
    'ado.utils',
    'ado.media',
    'ado.content',

    'ado.examples.pages',
    'ado.examples.portfolio',
    'ado.examples.artists',
    'ado.examples.exhibitions',
    'ado.examples.blog',
]

# Middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'ado.utils.threadlocal.middleware.ThreadLocalRequestMiddleware',
]

ROOT_URLCONF = 'ado.urls'

WSGI_APPLICATION = 'ado.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/
TIME_ZONE = 'America/Los_Angeles'
LANGUAGE_CODE = 'en-us'
USE_I18N = False
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
STATIC_URL = '/s/'
STATIC_ROOT = root('../../htdocs/static')
STATICFILES_DIRS = (
    root('../static'),
)

# Uploaded media & files
MEDIA_URL = '/m/'
MEDIA_ROOT = root('../../htdocs/media')
FILE_UPLOAD_PERMISSIONS = 0o664
DEFAULT_FILE_STORAGE = 'ado.utils.storage.overwrite.OverwriteFileSystemStorage'

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [root('templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'ado.menus.context_processors.menuitems',
            ],
        },
    },
]

# Headers used by http proxy middleware
PROXY_HOST_HEADER = 'HTTP_X_FORWARDED_FOR'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
    },
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{module} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
        'stderr': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stderr',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['stderr', 'mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'py.warnings': {
            'handlers': ['console'],
        },
    }
}

# Auth & Profiles
AUTHENTICATION_BACKENDS = (
    # uncomment to use Email as login
    'ado.utils.auth.backends.EmailAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# LOGIN_URL = '/account/login/'
# LOGIN_REDIRECT_URL = '/'

# Taggit settings
TAGGIT_TAGS_FROM_STRING = 'ado.utils.tags.comma_splitter'
TAGGIT_STRING_FROM_TAGS = 'ado.utils.tags.comma_joiner'

# This specifies the app & model order for admin dashboard and sidebar. To hide
# an app or model, do not specify in the config. If this is not set, or set to
# None, then all apps will be shown in alphabetical order (Django's default)
CUSTOMADMIN_DASHBOARD_APPS = (
    'media',
    'auth',
    'portfolio',
    'blog',
    'artists',
    'exhibitions',
)

CUSTOMADMIN_DASHBOARD_HIDDEN_APPS = (
    'taggit',
)


# CKEditor
def editor_asset(path):
    cache_buster = ''.join(random.choices(string.ascii_lowercase, k=5))
    return '{}{}?{}'.format(STATIC_URL, path, cache_buster)


CKEDITOR_COMMON_CONFIG = {
    'width': '100%',
    'allowedContent': True,
    # 'allowedContent': 'i b em strong br p h1 h2 h3 h4 h5 h6 blockquote ul ol table pre [*](*); img[!src,*]; a[!href,*]; iframe[*]{*}(*)',
    # 'extraAllowedContent': 'iframe[*]{*}(*)',
    'removePlugins': 'stylesheetparser',
    'extraPlugins': ','.join([
        'autogrow',
        'horizontalrule',
    ]),
    'autoGrow_minHeight': '120',
    'autoGrow_onStartup': True,
    # 'contentsCss': editor_asset('bundle/editor_content.css'),
    # 'stylesCombo_stylesSet': 'admin_styles:' + editor_asset('assets/editor/editor_styles.js'),
    # 'customConfig': editor_asset('assets/editor/editor_extra_config.js'),
}


def make_ckeditor_config(config):
    default = CKEDITOR_COMMON_CONFIG.copy()
    default.update(config)
    return default


CKEDITOR_CONFIGS = {
    'default': make_ckeditor_config({
        'height': '300',
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Styles', '-', 'Italic', 'Bold', 'Blockquote'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['NumberedList', 'BulletedList', 'HorizontalRule', 'Table'],
            ['Link', 'Unlink'],
            ['RemoveFormat', '-', 'Source'],
            # ['Maximize'],
        ],
        'autoGrow_maxHeight': '600',
        # 'filebrowserBrowseUrl': '/media/link_browser',
    }),
    'simple': make_ckeditor_config({
        'height': 120,
        'toolbar': 'Simple',
        'toolbar_Simple': [
            ['Italic', 'Bold'],
            ['Link', 'Unlink'],
            ['RemoveFormat', '-', 'Source']
            # ['Maximize'],
        ],
        'autoGrow_minHeight': '80',
        'autoGrow_maxHeight': '300',
    }),
}
