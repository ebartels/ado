"""
Settings for use in production.
"""
from .defaults import *

DEBUG = False

MANAGERS = list(MANAGERS) + [
    # Add any extra email addresses
]

ALLOWED_HOSTS = (
    # Add any domains that will be served in production
    # '.foo.com',
)

# Database defaults
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '_name_',
    },
}

# Cache Setup
CACHE_TIMEOUT = 60 * 10
CACHE_PREFIX = 'ADO'
CACHES = {
    'default': {
        'BACKEND': "django_redis.cache.RedisCache",
        'LOCATION': 'redis://127.0.0.1:6379',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT,
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_KWARGS': {
                'max_connections': 50,
                'timeout': 10,
                'retry_on_timeout': True,
            },
        },
    }
}
DJANGO_REDIS_IGNORE_EXCEPTIONS = True
DJANGO_REDIS_LOG_IGNORED_EXCEPTIONS = True
CACHE_MIDDLEWARE_SECONDS = CACHE_TIMEOUT
CACHE_MIDDLEWARE_KEY_PREFIX = CACHE_PREFIX
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

# Temp folders
TEMP_DIR = '/tmp/'
FILE_UPLOAD_TEMP_DIR = TEMP_DIR

# File Storage
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

# AWS Storage Settings
AWS_STORAGE_BUCKET_NAME = 'artcodehost-test-media'
AWS_LOCATION = 'ado'
AWS_DEFAULT_ACL = None
AWS_QUERYSTRING_AUTH = False
AWS_S3_REGION_NAME = 'us-west-2'
AWS_S3_CUSTOM_DOMAIN = 'test-media.artcodehost.io'
AWS_S3_URL_PROTOCOL = 'https:'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=864000',
}
AWS_S3_MAX_MEMORY_SIZE = 16 * 1024 * 1024  # 16 Mi

# Media serving
MEDIA_ROOT = os.path.join(TEMP_DIR, 'media')
MEDIA_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/{AWS_LOCATION}/'

STATIC_ROOT = root('../../htdocs/static')
STATIC_URL = '/s/'

# Image resizer config - uses cloudfront/lambda to generate images
DEFAULT_IMAGE_RESIZER = 'ado.media.resizer.LambdaImageResizer'
IMAGE_RESIZER_USE_DUMMY = False
IMAGE_RESIZER_LAMBDA_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/resized/{AWS_LOCATION}/'
