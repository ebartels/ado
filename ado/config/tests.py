import os
from .defaults import *

SECRET_KEY = '__secret__'
DEBUG = False
TESTING = True

# Database Settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

# Customize test apps (to pull in test-specific models)
INSTALLED_APPS = list(INSTALLED_APPS) + [
    'test_without_migrations',
    'ado.media.tests',
    'ado.menus.tests',
]

# media storage
DEFAULT_FILE_STORAGE = 'inmemorystorage.InMemoryStorage'
INMEMORYSTORAGE_PERSIST = True

# image resizer
DEFAULT_IMAGE_RESIZER = 'ado.media.resizer.DummyImageResizer'

# Cache Setup (use locmem for tests)
CACHE_TIMEOUT = 60 * 5
CACHE_PREFIX = 'ADO'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'ado-tests',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT
    }
}

TEMP_DIR = root('../tmp')

# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(TEMP_DIR, 'emails')

# Speed up tests by setting faster hash algo
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
)
