from django.shortcuts import render, get_object_or_404
from ado.examples.artists.models import Artist


def index(request):
    """
    Index & search page for Artists
    """
    artists = Artist.objects.filter(published=True)

    year = None
    if 'year' in request.GET:
        year = request.GET['year']
        artists = artists.filter(date__year=year)

    return render(request, 'artists/artist/index.html', {
        'artists': artists,
        'year': year,
    })


def view(request, slug):
    artist = get_object_or_404(Artist, slug=slug, published=True)
    return render(request, 'artists/artist/view.html', {
        'artist': artist
    })
