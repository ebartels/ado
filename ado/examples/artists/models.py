from django.db import models
from django.urls import reverse

from positions import PositionField
from ado.media.fields.related import RelatedImagesField, RelatedFilesField
from autoslug import AutoSlugField


class Artist(models.Model):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='name',
        max_length=200,
        unique=True,
        editable=False,
        help_text='Unique text identifier used in urls.')
    published = models.BooleanField(
        default=True,
        help_text='Whether to publish on the site.')
    bio = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    sort = PositionField(
        default=0,
        help_text='Lower numbers will appear first on site.')

    images = RelatedImagesField()
    files = RelatedFilesField()

    class Meta:
        ordering = ('sort', 'id',)

    def __str__(self):
        return '%s' % self.name

    def has_media(self):
        if self.images.count() or self.files.count():
            return True
        return False

    def get_absolute_url(self):
        return reverse('artists-artist-view', args=[self.slug])
