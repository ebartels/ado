from django.contrib import admin

from ado.examples.artists.models import Artist
from ado.media.admin.inlines import RelatedImagesInline
from ado.customadmin.admin import BaseModelAdminMixin, SortableAdminListMixin


@admin.register(Artist)
class ArtistAdmin(SortableAdminListMixin,
                  BaseModelAdminMixin,
                  admin.ModelAdmin):
    icon = '<i class="icon material-icons">person_outline</i>'
    list_display = ('image_column', 'name', 'sort', 'published',)
    list_filter = ('published',)
    search_fields = ['name', 'slug']
    html_editor_fields = ('bio', )
    inlines = [RelatedImagesInline]
    sortable_position_field = 'sort'

    def sort(self, obj):
        return obj.sort

    def get_image(self, obj):
        try:
            return obj.images.all()[:1][0].filename
        except IndexError:
            return None
