from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'ado.examples.pages'
    verbose_name = 'Pages'
    icon = '<i class="icon material-icons">content_copy</i>'
