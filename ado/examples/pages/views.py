from django.shortcuts import render, get_object_or_404
from ado.examples.pages.models import Page


def home(request):
    return render(request, 'home.html', {})


def view(request, slug):
    page = get_object_or_404(Page, slug=slug, published=True)
    templates = [
        'pages/page/%s/view.html' % page.pk,
        'pages/page/%s/view.html' % page.slug,
        'pages/page/view.html'
    ]
    menuitem = page.get_menuitem()
    if menuitem:
        for m in menuitem.get_ancestors():
            templates.insert(2, 'pages/page/menuitem_%s/view.html' % m.pk)
            templates.insert(2, 'pages/page/menuitem_%s/view.html' % m.slug)

    return render(request, templates, {
        'page': page
    })
