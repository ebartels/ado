from django.contrib import admin

from ado.examples.pages.models import Page
from ado.customadmin.admin import BaseModelAdminMixin


class PageAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    icon = '<i class="icon material-icons">content_copy</i>'
    model = Page
    list_filter = ('published', )
    list_display = ('title', 'published', )
    html_editor_fields = ['text']


admin.site.register(Page, PageAdmin)
