from django.db import models
from django.urls import reverse

from autoslug import AutoSlugField
from ado import menus


class Page(models.Model):
    """
    A page holds text content to display for the ado.
    """
    title = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='title',
        max_length=200,
        unique=True,
        editable=False,
        help_text='Unique text identifier used in urls.')
    published = models.BooleanField(
        default=True,
        help_text='Whether to publish on the site.')
    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % self.title

    def get_absolute_url(self):
        return reverse('pages-page-view', args=[self.slug])


menus.register_model(Page)
