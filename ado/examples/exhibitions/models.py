import datetime

from django.db import models
from django.urls import reverse
from ado.media.fields.related import RelatedImagesField, RelatedFilesField
from autoslug import AutoSlugField


class ExhibitionQuerySet(models.QuerySet):
    def current(self):
        now = datetime.date.today()
        return (self.filter(start_date__lt=now)
                    .filter(end_date__gt=now))

    def past(self):
        now = datetime.date.today()
        return (self.filter(end_date__lt=now)
                    .order_by('-start_date'))

    def future(self):
        now = datetime.date.today()
        return (self.filter(start_date__gt=now)
                    .order_by('start_date'))


class Exhibition(models.Model):
    """
    An Exhibition is a collection of images/files with start and end times.
    """
    title = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='title',
        max_length=200,
        unique=True,
        editable=False,
        help_text='Unique text identifier used in urls.')
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    published = models.BooleanField(
        default=True,
        help_text='Whether to publish on the site.')

    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    artists = models.ManyToManyField(
        'artists.Artist',
        blank=True,
        related_name='exhibitions')

    images = RelatedImagesField()
    files = RelatedFilesField()

    objects = ExhibitionQuerySet.as_manager()

    class Meta:
        ordering = ('-start_date', )

    def __str__(self):
        return '%s' % self.title

    def timeline(self):
        today = datetime.date.today()
        if self.start_date > today:
            type = 'upcoming'
        elif self.end_date < today:
            type = 'past'
        else:
            type = 'current'
        return type

    def has_media(self):
        if self.images.count() or self.files.count():
            return True
        return False

    def get_absolute_url(self):
        return reverse('exhibitions-exhibition-view', args=[self.slug])
