from django import http
from django.shortcuts import render, get_object_or_404
from django.core.paginator import QuerySetPaginator, InvalidPage
from ado.examples.exhibitions.models import Exhibition


def index(request):
    exhibitions = Exhibition.objects.filter(published=True)

    year = None
    if 'year' in request.GET:
        year = request.GET['year']
        exhibitions = exhibitions.filter(start_date__year=year)

    return render(request, 'exhibitions/exhibition/index.html', {
        'exhibitions': exhibitions,
        'year': year,
    })


def current(request):
    exhibitions = Exhibition.objects.current().filter(published=True)
    if not exhibitions:
        exhibitions = Exhibition.objects.future().filter(published=True)

    page = 1
    if 'page' in request.GET:
        page = request.GET['page']
    paginator = QuerySetPaginator(exhibitions, 10, orphans=2)
    try:
        page = paginator.page(page)
        exhibitions_for_page = page.object_list
    except InvalidPage:
        raise http.Http404

    return render(request, 'exhibitions/exhibition/index.html', {
        'exhibitions': exhibitions_for_page,
        'paginator': paginator,
        'current_page': page
    })


def past(request):
    exhibitions = Exhibition.objects.past().filter(published=True)

    page = 1
    if 'page' in request.GET:
        page = request.GET['page']
    paginator = QuerySetPaginator(exhibitions, 10, orphans=2)
    try:
        page = paginator.page(page)
        exhibitions_for_page = page.object_list
    except InvalidPage:
        raise http.Http404

    return render(request, 'exhibitions/exhibition/index.html', {
        'exhibitions': exhibitions_for_page,
        'paginator': paginator,
        'current_page': page
    })


def future(request):
    exhibitions = Exhibition.objects.future().filter(published=True)

    page = 1
    if 'page' in request.GET:
        page = request.GET['page']
    paginator = QuerySetPaginator(exhibitions, 10, orphans=2)
    try:
        page = paginator.page(page)
        exhibitions_for_page = page.object_list
    except InvalidPage:
        raise http.Http404

    return render(request, 'exhibitions/exhibition/index.html', {
        'exhibitions': exhibitions_for_page,
        'paginator': paginator,
        'current_page': page
    })


def view(request, slug):
    exhibition = get_object_or_404(Exhibition, slug=slug, published=True)
    return render(request, 'exhibitions/exhibition/view.html', {
        'exhibition': exhibition
    })
