from django import template

from ado.examples.exhibitions.models import Exhibition

register = template.Library()


@register.inclusion_tag('exhibitions/includes/exhibition_dates_menu.html')
def exhibition_dates_menu():
    """
    An inclusion tag to show a list of exhibition dates.
    """
    dates = Exhibition.objects.dates('start_date', 'year', order='DESC')

    return {
        'dates': dates
    }
