from django.db.models import Q
from django.views.generic import (
        ListView, MonthArchiveView, DayArchiveView, DateDetailView)
from django.views.generic.detail import (
        BaseDetailView, SingleObjectTemplateResponseMixin)
from django.views.generic.dates import (
        DateMixin, YearMixin, MonthMixin, _date_from_string)
from ado.examples.blog.models import Entry


import logging
log = logging.getLogger(__name__)


def _month_bounds(date):
    """
    Helper: return the first and last days of the month for the given date.
    """
    first_day = date.replace(day=1)
    if first_day.month == 12:
        last_day = first_day.replace(year=first_day.year + 1, month=1)
    else:
        last_day = first_day.replace(month=first_day.month + 1)

    return first_day, last_day


class BlogIndexMixin(object):
    template_name = 'blog/index.html'
    date_field = 'pub_date'

    def get_queryset(self):
        return self.filter_queryset(
                Entry.objects.published()
                     .select_related()
                     .prefetch_related('categories', 'tags'))

    def filter_queryset(self, qs):
        search_query = self.request.GET.get('q')
        if search_query:
            qs = qs.filter(
                Q(content__text__icontains=search_query) |
                Q(title__icontains=search_query))
        tag = self.request.GET.get('tag')
        if tag:
            qs = qs.filter(tags__name=tag)

        category_slug = self.get_category_slug()
        if category_slug:
            qs = qs.filter(categories__slug=category_slug)

        author_id = self.get_author_id()
        if author_id:
            qs = qs.filter(author=author_id)

        return qs

    def get_author_id(self):
        author_id = self.request.GET.get('author', '')
        try:
            author_id = int(author_id)
        except ValueError:
            author_id = None
        return author_id

    def get_category_slug(self):
        return self.request.GET.get('cat')


class BlogIndexView(BlogIndexMixin, ListView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'author_id': self.get_author_id(),
            'category_slug': self.request.GET.get('cat'),
            'current_tag': self.request.GET.get('tag'),
        })
        return context


blog_index = BlogIndexView.as_view()


class BlogMonthView(BlogIndexMixin, MonthArchiveView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'author_id': self.get_author_id(),
            'category_slug': self.request.GET.get('cat'),
            'current_tag': self.request.GET.get('tag'),
        })
        return context


month_archive = BlogMonthView.as_view()
month_archive_alt = BlogMonthView.as_view(month_format='%m')


class BlogDayView(BlogIndexMixin, DayArchiveView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'author_id': self.get_author_id(),
            'category_slug': self.request.GET.get('cat'),
            'current_tag': self.request.GET.get('tag'),
        })
        return context


day_archive = BlogDayView.as_view()
day_archive_alt = BlogDayView.as_view(month_format='%m')


class BaseMonthDetailView(YearMixin, MonthMixin, DateMixin, BaseDetailView):
    def get_object(self, queryset=None):
        year = self.get_year()
        month = self.get_month()

        qs = self.get_queryset()

        date_field = self.get_date_field()
        date = _date_from_string(year, self.get_year_format(),
                                 month, self.get_month_format())

        # Construct a date-range lookup.
        first_day, last_day = _month_bounds(date)
        lookup_kwargs = {
            '%s__gte' % date_field: first_day,
            '%s__lt' % date_field: last_day,
        }

        qs = qs.filter(**lookup_kwargs)

        return super().get_object(queryset=qs)


class BlogMonthDetailView(SingleObjectTemplateResponseMixin,
                          BaseMonthDetailView):
    date_field = 'pub_date'

    def get_queryset(self):
        return Entry.objects.published().select_related()


entry_month_detail = BlogMonthDetailView.as_view()
entry_month_detail_alt = BlogMonthDetailView.as_view(month_format='%m')


class EntryDetailView(DateDetailView):
    date_field = 'pub_date'

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated and user.is_staff:
            qs = Entry.objects.all().select_related()
        else:
            qs = Entry.objects.published().select_related()
        return qs


entry_detail = EntryDetailView.as_view()
entry_detail_alt = EntryDetailView.as_view(month_format='%m')
