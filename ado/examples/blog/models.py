# -*- coding: utf-8 -*-
from django.db import models
from django.urls import reverse
from django.utils.encoding import smart_text
from django.utils.text import Truncator
from django.utils.html import strip_tags
from django.utils import timezone
from django.contrib.auth.models import User
from django.conf import settings

from taggit.managers import TaggableManager
from autoslug import AutoSlugField
from positions.fields import PositionField
from ado.content.models import ContentItem


class CategoryQuerySet(models.QuerySet):
    def in_use(self):
        return self.filter(entries__isnull=False).distinct()


class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='name',
        max_length=200,
        unique=True,
        editable=False,
        help_text='Unique text identifier used in urls.')

    objects = CategoryQuerySet.as_manager()

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class EntryQuerySet(models.QuerySet):
    def published(self):
        now = timezone.now()
        return self.filter(published=True, pub_date__lte=now)

    def recent(self, count=10):
        return self.published()[:count]


class Entry(models.Model):
    title = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='title',
        max_length=200,
        unique=True,
        help_text='Unique text identifier used in urls.')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField(
        'Date published',
        default=timezone.now,
        db_index=True,
        help_text='Date you would like this entry to go live on the site.')
    published = models.BooleanField(
        default=True,
        help_text='Select to publish on the site.')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    categories = models.ManyToManyField(
        Category,
        blank=True,
        related_name='entries')
    related_entries = models.ManyToManyField(
        'self',
        blank=True,
        symmetrical=False)
    tags = TaggableManager(blank=True)

    objects = EntryQuerySet.as_manager()

    class Meta:
        verbose_name = 'Blog Entry'
        verbose_name_plural = 'Blog Entries'
        ordering = ('-pub_date',)
        get_latest_by = 'pub_date'

    def __str__(self):
        return '%s' % self.title

    def get_absolute_url(self):
        if settings.USE_TZ:
            tz = timezone.get_default_timezone()
            pub_date = timezone.localtime(self.pub_date, tz)
        else:
            pub_date = self.pub_date
        args = pub_date.strftime("%Y %b %d").split() + [self.slug]
        return reverse('blog-entry-view', args=args)

    @property
    def author_name(self):
        author = self.author
        name = author.get_full_name()
        return name or author.username or author.email

    @property
    def excerpt(self, trunc=80):
        text = ' '.join([item.text for item in self.content.exclude(text='')])
        text = smart_text(text, encoding="utf-8")
        text = strip_tags(text)
        return Truncator(text).words(trunc, html=False, truncate=' ...')


class EntryContent(ContentItem):
    related_model = Entry


class Link(models.Model):
    url = models.URLField(max_length=255)
    title = models.CharField(max_length=255)
    position = PositionField()

    class Meta:
        ordering = ('position',)

    def __str__(self):
        return self.title
