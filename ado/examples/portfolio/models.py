from django.db import models
from django.urls import reverse

from positions import PositionField
from ado.media.fields.related import RelatedImagesField, RelatedFilesField
from autoslug import AutoSlugField


class CollectionCategory(models.Model):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='name',
        max_length=200,
        unique=True,
        editable=False,
        help_text='Unique text identifier used in urls.')
    published = models.BooleanField(
        default=True,
        help_text='Whether to publish on the site.')
    sort = PositionField(
        'Position',
        help_text='Lower numbers will appear first on site.')

    class Meta:
        ordering = ('sort', 'id')
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Collection(models.Model):
    """
    A Collection of images and/or files
    """
    title = models.CharField(max_length=200)
    slug = AutoSlugField(
        populate_from='title',
        max_length=200,
        unique=True,
        editable=False,
        help_text='Unique text identifier used in urls.')
    date = models.DateField(blank=True, null=True)
    published = models.BooleanField(
        default=True,
        help_text='Whether to publish on the site.')
    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    sort = PositionField(
        'Position',
        help_text='Lower numbers appear first on site.')
    categories = models.ManyToManyField('CollectionCategory', blank=True)
    image = models.ForeignKey('media.Image', null=True, blank=True, on_delete=models.CASCADE)

    images = RelatedImagesField()
    files = RelatedFilesField()

    class Meta:
        ordering = ('-date', 'sort', 'id')

    def __str__(self):
        return '%s' % self.title

    def has_media(self):
        if self.images.count() or self.files.count():
            return True
        return False

    def get_absolute_url(self):
        return reverse('portfolio-collection-view', args=[self.slug])
