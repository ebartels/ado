from django.views.generic import ListView, DetailView
from ado.examples.portfolio.models import Collection, CollectionCategory


class CollectionListView(ListView):
    queryset = CollectionCategory.objects.filter(published=True)
    template_name = 'portfolio/collection/index.html'
    context_object_name = 'collection_categories'

    def get_queryset(self):
        qs = super().get_queryset()

        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
                qs = qs.filter(collections__date__year=year)
            except ValueError:
                pass

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = None
        if 'year' in self.request.GET:
            year = self.request.GET['year']
            context.update({
                'year': year,
            })
        return context


index = CollectionListView.as_view()


class CollectionDetailView(DetailView):
    queryset = Collection.objects.filter(published=True)
    template_name = 'portfolio/collection/view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        collection = context['object']
        images = collection.images.all()
        context.update({
            'collections': self.get_queryset(),
            'images': images,
        })
        return context


view = CollectionDetailView.as_view()
