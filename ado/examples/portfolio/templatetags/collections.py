from django import template

from ado.examples.portfolio.models import Collection, CollectionCategory

register = template.Library()


@register.inclusion_tag('portfolio/includes/collection_dates_menu.html')
def collection_dates_menu():
    """
    An inclusion tag to pull in a list of dates for Collection objects.
    """
    dates = Collection.objects.dates('date', 'year', order='DESC')

    return {
        'dates': dates
    }


@register.inclusion_tag('portfolio/includes/collection_menu.html')
def collection_menu():
    """
    An inclusion tag to show a list of Collections.
    """
    collections = Collection.objects.filter(published=True)
    categories = CollectionCategory.objects.filter(published=True)
    return {
        'collections': collections,
        'categories': categories
    }
