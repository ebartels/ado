from django.urls import path, include
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib import admin

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='index'),

    # App examples
    path('pages/', include('ado.examples.pages.urls')),
    path('portfolio/', include('ado.examples.portfolio.urls')),
    path('artists/', include('ado.examples.artists.urls')),
    path('exhibitions/', include('ado.examples.exhibitions.urls')),
    path('blog/', include('ado.examples.blog.urls')),

    # Admin
    path('customadmin/', include('ado.customadmin.urls')),
    path('media/', include('ado.media.urls')),
    path('admin/', admin.site.urls),
]

# MEDIA For Development Server
urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT,
                      show_indexes=True)
