from django.apps import AppConfig
from django.contrib.auth.apps import AuthConfig as _AuthConfig


class CustomAdminConfig(AppConfig):
    name = 'ado.customadmin'
    label = 'customadmin'


# Customize auth config
class AuthConfig(_AuthConfig):
    verbose_name = 'User Management'
    icon = '<i class="icon material-icons">groups</i>'
