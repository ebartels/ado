const path = require('path')
const autoprefixer = require('autoprefixer')
const BundleTracker = require('webpack-bundle-tracker')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackBar = require('webpackbar')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const paths = {
  root: path.resolve(__dirname),
  buildPath: path.resolve('./bundle'),
  appPath: path.resolve('./'),
  stylesPath: path.resolve('./styles'),
}

const config = (env = {}) => ({
  mode: env.production ? 'production' : 'development',
  context: paths.root,

  entry: {
    main: [
      'whatwg-fetch',
      './index.js',
    ],
    smartselect: './components/widgets/smartselect/index.js',
    sortablelist: './components/sortablelist/index.js',
    menus: './components/menus/index.js',
    media_changelist: './components/media/changelist/index.js',
    media_widgets: './components/media/widgets/index.js',
    media_inlines: './components/media/inlines/index.js',
    media_upload: './components/media/upload/index.js',
    content_inlines: './components/content/inlines/index.js',
  },

  output: {
    path: paths.buildPath,
    filename: `customadmin.[name]${env.production ? '.[contenthash:8]' : ''}.js`,
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    // Allow app imports from '~/some/module'
    alias: {
      '~': paths.appPath,
    },
  },

  module: {
    // Loaders rules for different file types
    rules: [
      {
        test: /react-virtualized/,
        use: {
          loader: 'null-loader',
        },
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
      },
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: !env.production,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: !env.production,
              plugins: [
                autoprefixer({ remove: false }),
              ],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !env.production,
              sassOptions: {
                includePaths: [paths.stylesPath],
              },
            },
          },
        ],
      },
      {
        test: /\.(png|gif|jpg|jpeg|svg)?$/,
        use: 'url-loader',
      },
      {
        test: /\.(ttf|eot|woff|woff2)(\?\S*)?$/,
        use: !env.production ? 'url-loader' : {
          loader: 'file-loader',
          options: {
            name: 'fonts/[hash].[ext]',
          },
        },
      },
    ],
  },

  stats: 'errors-warnings',

  plugins: [
    new WebpackBar(),

    // Outputs stats on generated files (used by django-webpack-loader)
    new BundleTracker({ filename: './bundle/webpack-stats.json' }),

    new MiniCssExtractPlugin({
      filename: `customadmin.[name]${env.production ? '.[contenthash:8]' : ''}.css`,
    }),

    // production only
    ...(env.production ? [
      // new BundleAnalyzerPlugin(),
    ] : []),

    // dev & production, but not for dev-server
    ...(!env.devServer ? [
      new CleanWebpackPlugin(),
    ] : []),
  ],

  optimization: {
    hashedModuleIds: env.production,
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        // commons chunk for shared code across entrypoints
        commons: {
          name: 'commons',
          chunks: 'initial',
          minChunks: 2,
          test: module => (
            // module.context.indexOf('/node_modules/') !== -1 &&
            !/\.s?css$/.test(module.nameForCondition && module.nameForCondition())
          ),
        },
      },
    },
  },

  performance: {
    hints: false,
  },

  ...(!env.production ? {
    devtool: 'inline-cheap-module-source-map',
  } : {}),
})

module.exports = config
