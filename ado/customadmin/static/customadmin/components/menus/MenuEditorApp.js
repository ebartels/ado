import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import SortableTree, {
  defaultGetNodeKey,
  getFlatDataFromTree,
  removeNodeAtPath,
} from 'react-sortable-tree'

import fetchUrl from '~/lib/fetchUrl'
import getCSRFToken from '~/lib/getCSRFToken'


import 'react-sortable-tree/style.css'
import './MenuEditorApp.scss'


/**
 * Form used in menu editor
 */
export class MenuForm extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    linkTitle: PropTypes.string.isRequired,
  }

  state = {
    isVisible: false,
  }

  handleOpenClick = () => {
    const isVisible = !this.state.isVisible
    this.setState({
      isVisible,
    })

    if (isVisible) {
      setTimeout(this.setFocus)
    }
  }

  setFocus = () => {
    const input = Array.from(this.el.querySelectorAll('input'))
      .filter(i => i.type !== 'hidden')[0]
    if (input) {
      input.focus()
    }
  }

  render() {
    return (
      <form action="" method="post" className="MenuForm" ref={el => { this.el = el }}>
        <input type="hidden" name="csrfmiddlewaretoken" value={getCSRFToken()} />
        <a className="addlink" onClick={this.handleOpenClick}>{this.props.linkTitle}</a>
        <fieldset className={`${this.state.isVisible ? 'visible' : 'hidden'}`}>
          {this.props.children}
        </fieldset>
      </form>
    )
  }
}

/**
 * Unused Objects List
 */
export class MenuUnusedObjects extends React.PureComponent {
  static propTypes = {
    unusedObjects: PropTypes.array,
    onAddObject: PropTypes.func,
  }

  addObject = (appLabel, modelName, pk) => {
    this.props.onAddObject(appLabel, modelName, pk)
  }

  render() {
    const { unusedObjects } = this.props
    return (
      <div
        className="MenuUnusedObjects"
        style={{ display: unusedObjects.length ? '' : 'none' }}>
        {unusedObjects.map(({ opts, objects }, i) => (
          <div key={i} className="MenuUnusedObjects__model module">
            <h2>
              <span>{opts.verbose_name_plural}</span>
              <a className="addlink" href={`/admin/${opts.app_label}/${opts.model_name}/add/`}>Add a {opts.verbose_name}</a>
            </h2>
            <div className="MenuUnusedObjects__objects">
              {objects.map((object, j) => (
                <div key={j} className="MenuUnusedObjects__objects__object">
                  <a href={object.admin_url}>{object.title}</a>
                  <a
                    className="addlink"
                    onClick={() => { this.addObject(opts.app_label, opts.model_name, object.pk) }}>
                    add to menu
                  </a>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    )
  }
}


/**
 * Unused Apps list
 */
export class MenuUnusedApps extends React.PureComponent {
  static propTypes = {
    unusedApps: PropTypes.array,
    onAddApp: PropTypes.func,
  }

  addApp = (app) => {
    this.props.onAddApp(app)
  }

  render() {
    const { unusedApps } = this.props
    return (
      <div
        className="MenuUnusedApps"
        style={{ display: unusedApps.length ? '' : 'none' }}>
        <div className="MenuUnusedApps__model module">
          <h2>
            <span>Apps</span>
          </h2>
          <div className="MenuUnusedApps__apps">
            {unusedApps.map((app, j) => (
              <div key={j} className="MenuUnusedApps__apps__app">
                <span>{app.title}</span>
                <a
                  className="addlink"
                  onClick={() => { this.addApp(app) }}>
                  add to menu
                </a>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}


/**
 * Tree component for menu
 */
export class MenuEditorTree extends React.PureComponent {
  static propTypes = {
    menuItems: PropTypes.array.isRequired,
    onTreeChange: PropTypes.func.isRequired,
    onMoveNode: PropTypes.func.isRequired,
    onDeleteNode: PropTypes.func.isRequired,
  }

  state = {
    treeData: [],
  }

  componentDidMount() {
    this.setState({
      treeData: this.computeTree(this.props),
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.menuItems !== prevProps.menuItems) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ treeData: this.computeTree(this.props) })
    }
  }

  computeTree = (props) => {
    const computeNode = node => {
      return {
        ...node,
        subtitle: this.subTitle,
        expanded: ('expanded' in node) ? node.expanded : node.level <= 1,
        children: (node.children || []).map(c => computeNode(c)),
      }
    }

    return props.menuItems.map(computeNode)
  }

  subTitle = ({ node, path, treeIndex }) => {
    return (
      <span className="nodeSubTitle">
        <em className="nodeLabel">{node.node_label}:</em>
        <span className="nodeTools">
          {node.url && (
            <a href={node.url} className="viewlink" target="_blank" rel="noopener noreferrer">view on site</a>
          )}
          <a href={`${node.id}/edit_object/`} className="changelink">edit</a>
          <a className="deletelink" onClick={() => { this.props.onDeleteNode(node.id) }}>remove</a>
        </span>
      </span>
    )
  }

  handleTreeChange = (treeData) => {
    this.props.onTreeChange(treeData)
  }

  handleMoveNode = ({ treeData, node, prevPath, prevTreeIndex, nextPath, nextTreeIndex }) => {
    const flatTree = getFlatDataFromTree({
      treeData,
      getNodeKey: defaultGetNodeKey,
      ignoreCollapsed: false,
    })

    // Find mptt lft & rght noe
    const findLftRght = () => {
      const currentNode = flatTree[nextTreeIndex]
      const siblings = flatTree.filter(i => i.parentNode === currentNode.parentNode)
      const currentIndex = siblings.indexOf(currentNode)
      return [
        siblings[currentIndex - 1],
        siblings[currentIndex + 1],
      ]
    }

    // Build data for api post
    const [lft, rght] = findLftRght()
    const data = {
      item: node.id,
    }

    if (nextPath.length > 1) {
      data.parent = flatTree[nextTreeIndex].parentNode.id
    }
    if (lft && lft.node.id !== data.parent) {
      data.lft = lft.node.id
    }
    if (rght && rght.node.id !== data.parent) {
      data.rght = rght.node.id
    }

    this.props.onMoveNode(data)
  }

  render() {
    return (
      <div className="MenuEditorTree">
        <SortableTree
          treeData={this.state.treeData}
          isVirtualized={false}
          rowHeight={68}
          onChange={this.handleTreeChange}
          onMoveNode={this.handleMoveNode}
        />
      </div>
    )
  }
}


/**
 * Main menu app component
 */
export class MenuEditorAppContainer extends React.PureComponent {
  static propTypes = {
    menuItems: PropTypes.array.isRequired,
    unusedObjects: PropTypes.array.isRequired,
    unusedApps: PropTypes.array.isRequired,
    moveUrl: PropTypes.string.isRequired,
    addObjectUrl: PropTypes.string.isRequired,
    addMenuitemUrl: PropTypes.string.isRequired,
    deleteMenuitemUrl: PropTypes.string.isRequired,
  }

  state = {
    menuItems: [],
    unusedObjects: [],
    unusedApps: [],
  }

  componentDidMount() {
    this.setState({
      menuItems: this.props.menuItems,
      unusedObjects: this.props.unusedObjects,
      unusedApps: this.props.unusedApps,
    })
  }

  handleTreeChange = (treeData) => {
    this.setState({ menuItems: treeData })
  }

  handleMoveNode = (data) => {
    const url = this.props.moveUrl
    fetchUrl(url, {
      method: 'POST',
      body: JSON.stringify(data),
    })
  }

  handleDeleteNode = (id) => {
    const url = this.props.deleteMenuitemUrl

    fetchUrl(url, {
      method: 'POST',
      body: JSON.stringify({ pk: id }),
    })
      .then(response => response.json())
      .then(json => {
        // Update menu tree data
        const flatTree = getFlatDataFromTree({
          treeData: this.state.menuItems,
          getNodeKey: defaultGetNodeKey,
          ignoreCollapsed: false,
        })
        const index = flatTree.findIndex(i => i.node.id === id)
        const path = flatTree[index].path
        const newTree = removeNodeAtPath({
          path,
          treeData: this.state.menuItems,
          getNodeKey: defaultGetNodeKey,
          ignoreCollapsed: false,
        })
        this.setState({
          menuItems: newTree,
        })


        // Update unused objects
        if (json.type === 'object') {
          const unusedObjects = this.state.unusedObjects
            .map(({ opts, objects }) => {
              if (opts.app_label === json.app_label &&
                  opts.model_name === json.model_name) {
                return {
                  opts,
                  objects: [
                    ...objects,
                    {
                      pk: json.object_id,
                      title: json.title,
                      admin_url: `/admin/${json.app_label}/${json.model_name}/${json.object_id}`,
                    },
                  ],
                }
              }
              return { opts, objects }
            })
          this.setState({
            unusedObjects,
          })
        }
        // Update unused apps
        else if (json.type === 'app') {
          const unusedApps = [
            ...this.state.unusedApps,
            { url: json.url, title: json.title },
          ]
          this.setState({
            unusedApps,
          })
        }
      })
  }

  handleAddObject = (appLabel, modelName, pk) => {
    const url = this.props.addObjectUrl
    fetchUrl(url, {
      method: 'POST',
      body: JSON.stringify({
        pk,
        model: `${appLabel}.${modelName}`,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // Update menuItems
        this.setState({
          menuItems: [
            ...this.state.menuItems,
            json,
          ],
        })

        // Update unused objects
        const unusedObjects = this.state.unusedObjects
          .map(({ opts, objects }) => {
            if (opts.app_label === json.app_label &&
              opts.model_name === json.model_name) {
              const index = objects.findIndex(o => o.pk === json.object_id)
              return {
                opts,
                objects: [
                  ...objects.slice(0, index),
                  ...objects.slice(index + 1),
                ],
              }
            }
            return { opts, objects }
          })
        this.setState({
          unusedObjects,
        })
      })
  }

  handleAddApp = (app) => {
    const url = this.props.addMenuitemUrl
    fetchUrl(url, {
      method: 'POST',
      body: JSON.stringify({
        url: app.url,
        title: app.title,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // Update menuItems
        this.setState({
          menuItems: [
            ...this.state.menuItems,
            json,
          ],
        })

        // Update unused apps
        const index = this.state.unusedApps.findIndex(a => a.url === app.url)
        const unusedApps = [
          ...this.state.unusedApps.slice(0, index),
          ...this.state.unusedApps.slice(index + 1),
        ]
        this.setState({
          unusedApps,
        })
      })
  }

  render() {
    return (
      <div className="MenuEditorAppContainer">
        <div className="MenuEditorAppContainer__tools">
          <MenuForm linkTitle="Add a Container">
            <div>
              <label htmlFor="id_container_title">Name:</label>
              <input type="text" name="container_title" id="id_container_title" />
              <button>Add</button>
            </div>
          </MenuForm>

          <MenuForm linkTitle="Add a Custom Liink">
            <div>
              <label htmlFor="id_url_title">Name:</label>
              <input type="text" name="url_title" id="id_url_title" />
            </div>
            <div>
              <label htmlFor="id_url_url">Url:</label>
              <input type="text" name="url_url" id="id_url_url" defaultValue="http://" />
              <button>Add</button>
            </div>
          </MenuForm>

          <MenuUnusedObjects
            unusedObjects={this.state.unusedObjects}
            onAddObject={this.handleAddObject}
          />

          <MenuUnusedApps
            unusedApps={this.state.unusedApps}
            onAddApp={this.handleAddApp}
          />
        </div>

        <MenuEditorTree
          menuItems={this.state.menuItems}
          moveUrl={this.props.moveUrl}
          onTreeChange={this.handleTreeChange}
          onMoveNode={this.handleMoveNode}
          onDeleteNode={this.handleDeleteNode}
        />
      </div>
    )
  }
}


/**
 * Mount component
 */
const init = (componentRoot) => {
  const scriptNode = document.getElementById('menu_app_json')
  const menuData = JSON.parse(scriptNode.innerHTML)
  const moveUrl = componentRoot.dataset.move_url
  const addObjectUrl = componentRoot.dataset.add_object_url
  const addMenuitemUrl = componentRoot.dataset.add_menuitem_url
  const deleteMenuitemUrl = componentRoot.dataset.delete_menuitem_url
  ReactDOM.render(
    <MenuEditorAppContainer
      moveUrl={moveUrl}
      addObjectUrl={addObjectUrl}
      addMenuitemUrl={addMenuitemUrl}
      deleteMenuitemUrl={deleteMenuitemUrl}
      menuItems={menuData.items}
      unusedObjects={menuData.unused_objects}
      unusedApps={menuData.unused_apps}
    />,
    componentRoot,
  )
}

/**
 * Export object
 *
 * Call: MenuEditorApp.init()
 */
export const MenuEditorApp = {
  init,
}

export default MenuEditorApp
