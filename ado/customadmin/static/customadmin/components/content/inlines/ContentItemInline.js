import domReady from '~/lib/domReady'
import { jQuery as $ } from '~/lib/jQuery'
import MultipleMediaSelector from '~/components/media/MultipleMediaSelector'
import { addMediaForeignKeyWidgets } from '~/components/media/widgets/MediaForeignKeyWidgets'

import './ContentItemInline.scss'

/**
 * Custom functionality for editing EssayPost contents
 *
 * Each EssayPost type has inlines for editing an essay section, which is
 * either an Image or a text section. This sets up functionality allowing to
 * add either an image or text and will hide the other fields in the inline
 * form depending on the type the user selected.
 */
export const ContentItemInline = (inlineGroup) => {
  inlineGroup.classList.add('content-item-inline')

  // Set up inline styles & labels
  const inlineRows = [...inlineGroup.querySelectorAll('.inline-related:not(.empty-form)')]
  inlineRows.forEach(inline => {
    const typeClass = inline.dataset.contentItemType

    // Add class type
    inline.classList.add(`inline-${typeClass}`)

    // Change inline label text
    const label = inline.querySelector('h3 .inline_label')
    if (label) label.innerText = typeClass

    // Change 'delete' label to remove'
    const deleteLabel = inline.querySelector('.delete .vCheckboxLabel')
    if (deleteLabel) deleteLabel.innerText = 'remove'
  })

  // Find add-row
  const addRow = inlineGroup.querySelector('.add-row')
  if (!addRow) return // no add-row link was found (likely means no permisisons)
  const addLink = addRow.querySelector('a')

  // Create links for adding new inline elements (image or text)
  const addTextLink = $(addLink).clone(false)[0]
  const addImageLink = $(addLink).clone(false)[0]
  const addVideoLink = $(addLink).clone(false)[0]

  // Hide the original add link
  addLink.style.display = 'none'

  // Set up text link
  addTextLink.innerText = 'Add Text Section'
  addRow.appendChild(addTextLink)

  // Set up image link
  addImageLink.innerText = 'Add Images'
  addRow.appendChild(addImageLink)

  // Set up video link
  addVideoLink.innerText = 'Add Videos'
  addRow.appendChild(addVideoLink)


  /**
   * Initialize new inline when added
   */
  const onInlineAdded = ({ fieldName, label }) => {
    $(addLink).trigger('click')
    const selector = '.inline-related.last-related:not(.empty-form)'
    const inline = Array.from(inlineGroup.querySelectorAll(selector)).slice(-1)[0]
    inline.classList.add(`inline-${fieldName}`)
    const inlineLabel = inline.querySelector('.inline_label')
    inlineLabel.innerText = label

    // prevent initializing ckeditor for non-text inlines
    if (fieldName !== 'text') {
      const textarea = inline.querySelector('.field-text textarea')
      textarea.dataset.processed = '1'
    }

    return inline
  }

  /**
   * Initialize multiple image & video inlines when added
   */
  const onMediaItemsAdded = (selectedIds, { fieldName, label }) => {
    selectedIds.forEach(async (id, i) => {
      const inline = onInlineAdded({ fieldName, label })
      addMediaForeignKeyWidgets()
      const input = $(inline).find(`.field-${fieldName} .vForeignKeyRawIdAdminField`)[0]
      $(input).val(id).trigger('change')
    })
  }

  // Handle add text
  const onAddTextClicked = (e) => {
    e.preventDefault()
    onInlineAdded({ fieldName: 'text', label: 'Text Section' })
  }

  // Handle add images
  const onAddImageClicked = async (e) => {
    e.preventDefault()
    e.stopPropagation()
    const selectedIds = await MultipleMediaSelector('media', 'image')
    onMediaItemsAdded(selectedIds, { fieldName: 'image', label: 'Image' })
  }

  // Handle add video
  const onAddVideoClicked = async (e) => {
    e.preventDefault()
    e.stopPropagation()
    const selectedIds = await MultipleMediaSelector('media', 'video')
    onMediaItemsAdded(selectedIds, { fieldName: 'video', label: 'Video' })
  }

  // Set up add click handlers
  $(addTextLink).on('click', onAddTextClicked)
  $(addImageLink).on('click', onAddImageClicked)
  $(addVideoLink).on('click', onAddVideoClicked)
}


/**
 * Init for content inline elements
 */
const init = () => {
  domReady(() => {
    setTimeout(() => {
      const inlineGroup = document.getElementById('content-group')
      if (inlineGroup) ContentItemInline(inlineGroup)
      else console.warn('Could not find content-group')  // eslint-disable-line
    })
  })
}

export default {
  init,
}
