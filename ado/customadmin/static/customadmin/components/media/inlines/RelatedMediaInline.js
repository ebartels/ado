/* eslint no-alert: off */
import domReady from '~/lib/domReady'
import { jQuery as $ } from '~/lib/jQuery'
import MultipleMediaSelector from '~/components/media/MultipleMediaSelector'
import { fetchMediaItemInfo } from '~/components/media/widgets/MediaForeignKeyWidgets'

import './RelatedMediaInline.scss'

export const RelatedMediaInline = (inlineModule) => {
  const inlineGroup = inlineModule.parentNode
  inlineGroup.classList.add('related-media-inline')

  // Add help text
  const inlineHeader = inlineModule.querySelector('h2')
  const headerText = inlineHeader.innerText.replace(/^All /, '')
  inlineHeader.innerHTML = `${headerText} <small>(drag to reorder)</small>`

  // Set up inline styles
  const inlineRows = [...inlineModule.querySelectorAll('.inline-related:not(.empty-form)')]
  inlineRows.forEach(inline => {
    const formRow = inline.querySelector('.form-row.field-item')

    const changeLink = formRow.querySelector('strong > a')
    if (changeLink) {
      changeLink.title = changeLink.innerText
      changeLink.innerText = 'edit'
      changeLink.classList.add('changelink')
    }

    const deletewidget = inline.querySelector('.delete')
    if (deletewidget) {
      $(deletewidget).insertAfter(changeLink)
      $(deletewidget).find('label').text('remove')
    }
  })

  // Handle MediaItemSelector for adding multiple media items

  // find out which type of media items the inline is for
  const formset = JSON.parse(inlineGroup.dataset.inlineFormset)
  const prefix = formset.options.prefix
  const [app, relatedModel] = prefix.split('-').slice(0, 2)

  // Find original add link for the inline
  const originalAddLink = inlineGroup.querySelector('.add-row a')
  if (!originalAddLink) return // no add-row link was found (likely means no permisisons)
  originalAddLink.style.display = 'none'

  // Create new add link for inline
  const addLink = document.createElement('a')
  addLink.href = '#'
  addLink.innerText = originalAddLink.innerText.replace(/Add another (.+)/, 'Add $1s')
  originalAddLink.parentNode.appendChild(addLink)

  // Click handler for add link
  addLink.addEventListener('click', async e => {
    e.preventDefault()

    // Show media selector
    const selectedIds = await MultipleMediaSelector(app, relatedModel)
    selectedIds.forEach(async id => {
      $(originalAddLink).trigger('click')
      const inline = $(inlineGroup).find('.inline-related.last-related:not(.empty-form)').last()[0]
      const input = $(inline).find('.vForeignKeyRawIdAdminField')[0]
      $(input).val(id).trigger('change')

      const data = await fetchMediaItemInfo({ model: 'media.mediaitem', pk: id })
      const mediaImage = inline.querySelector('.media-image')
      if (data.thumbnail || data.image) {
        mediaImage.innerHTML = `<img src="${data.thumbnail || data.image}" />`
      }
      const mediaTitle = inline.querySelector('.media-title')
      mediaTitle.innerText = data.str
    })
  })
}

const init = () => {
  domReady(() => {
    setTimeout(() => {
      const inlines = Array.from(document.querySelectorAll('.mediaitem-inline'))
      Array.from(inlines).forEach(RelatedMediaInline)
    })
  })
}

export default {
  init,
}
