import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import cookie from 'cookie'
import { default as cx } from 'classnames'
import Dropzone from 'react-dropzone'
import filesize from 'filesize'

import Icon from '~/components/Icon'
import './MediaUpload.scss'

export const FileList = ({ files, onRetry }) => (
  <div className="MediaUpload__FileList">
    <h2>Selected Files:</h2>
    <ul>
      {files.map((f, i) => (
        <li key={i} className={cx({ error: f.error })}>
          <span className="filename">{f.file.name}</span>
          <span className="filetype">{f.file.type}</span>
          <span className="filesize">{filesize(f.file.size)}</span>
          <span className="percent">{f.progress}%</span>
          {f.success && <Icon>check_circle</Icon>}
          {f.error && (
            <span className="error-msg">An error occurred uploading this file</span>
          )}
          {f.error && (
            <a className="error-retry" onClick={onRetry}>
              <span>Retry</span> <Icon>refresh</Icon>
            </a>
          )}
          <div
            className="progress-indicator"
            style={{
              width: `${f.progress}%`,
            }}
          />
        </li>
      ))}
    </ul>
  </div>
)

FileList.propTypes = {
  files: PropTypes.array,
  onRetry: PropTypes.func,
}

export class MediaUploadApp extends React.Component {
  static propTypes = {
    existingUploads: PropTypes.number,
  }

  static defaultProps = {
    existingUploads: 0,
  }

  state = {
    files: [],
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.files.length !== this.state.files.length) {
      this.startUpload()
    }
  }

  handleDrop = (files) => {
    const currentFiles = new Set(this.state.files.map(f => f.file.name))
    const newFiles = files
      .filter(f => !currentFiles.has(f.name))
      .map(f => ({ file: f, progress: 0, error: false }))
    this.setState(state => ({
      files: [
        ...newFiles,
        ...state.files,
      ],
    }))
  }

  startUpload = () => {
    // Get cxrf token
    const cookies = cookie.parse(document.cookie)
    const url = '/admin/media/mediaitem/upload_file/'
    const files = this.state.files.filter(f => f.progress === 0)

    // Loop through files
    files.forEach(f => {
      const formData = new FormData()
      formData.append('file', f.file)
      formData.append('filename', f.file.name)
      formData.append('filetype', f.file.type)

      // create request
      const xhr = new XMLHttpRequest()

      // handle load
      xhr.addEventListener('load', () => {
        if (xhr.status !== 200) {
          this.setError(f.file.name)
        }
        else {
          this.setSuccess(f.file.name)
        }
      })

      // handle error
      xhr.addEventListener('error', () => {
        this.setError(f.file.name)
      })

      // handle upload progress event
      xhr.upload.addEventListener('progress', (e) => {
        if (e.lengthComputable) {
          const percentage = Math.round((e.loaded * 100) / e.total)
          this.updateProgress(f.file.name, percentage)
        }
      })

      // handle upload load event
      xhr.upload.addEventListener('load', () => {
        this.updateProgress(f.file.name, 100)
      })

      // Start file upload
      xhr.open('POST', url)
      xhr.setRequestHeader('X-CSRFToken', cookies.csrftoken)
      xhr.send(formData)
    })
  }

  updateProgress = (filename, percentage) => {
    const files = this.state.files.map(f => {
      if (f.file.name === filename) {
        return {
          ...f,
          progress: percentage,
        }
      }
      return f
    })
    this.setState({ files })
  }

  setError = (filename) => {
    const files = this.state.files.map(f => {
      if (f.file.name === filename) {
        return {
          ...f,
          progress: 0,
          error: true,
        }
      }
      return f
    })
    this.setState({ files })
  }

  setSuccess = (filename) => {
    const files = this.state.files.map(f => {
      if (f.file.name === filename) {
        return {
          ...f,
          progress: 100,
          error: false,
          success: true,
        }
      }
      return f
    })
    this.setState({ files })
  }

  render() {
    const { existingUploads } = this.props
    const { files } = this.state

    const completedUploads = files.filter(f => f.success)
    const uploadCount = completedUploads.length + existingUploads

    return (
      <div className="MediaUpload">
        <Dropzone onDrop={this.handleDrop}>
          {({ getRootProps, getInputProps }) => (
            <div {...getRootProps()} className="MediaUpload__dropzone">
              <input {...getInputProps()} />
              <p>Drop files here<br /> or click to open finder</p>
              <Icon>file_upload</Icon>
            </div>
          )}
        </Dropzone>

        {uploadCount > 0 && <p><strong>You have {uploadCount} uploads ready to edit.</strong></p>}
        {uploadCount > 0 && (
          <p>
            <a href="./edit/" className="MediaUpload__continue-button">
              <span>Continue</span> <Icon>arrow_forward</Icon>
            </a>
          </p>
        )}

        {!files.length ? null : (
          <FileList files={files} onRetry={this.startUpload} />
        )}
      </div>
    )
  }
}


const init = () => {
  // find app element
  const el = document.getElementById('media-upload-app')
  if (!el) {
    return
  }

  const uploadCount = parseInt(el.dataset.uploadCount || 0, 10)

  // Render app component
  ReactDOM.render(
    <MediaUploadApp existingUploads={uploadCount} />,
    el,
  )
}

export default {
  init,
}
