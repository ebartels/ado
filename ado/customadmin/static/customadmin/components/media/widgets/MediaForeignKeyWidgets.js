/* eslint  no-param-reassign: off */
import fetchUrl from '~/lib/fetchUrl'
import './MediaForeignKeyWidgets.scss'

const BLANK_SRC = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='

/**
 * Watch input for changes
 */
const monitorInput = (input, fn) => {
  let lastValue = input.value

  const checkInput = () => {
    if (input.value !== lastValue) {
      fn(input.value)
    }
    lastValue = input.value
  }

  input.addEventListener('change input', checkInput)
  setInterval(checkInput, 500)
}

/**
 * Fetch url
 */
export const fetchMediaItemInfo = ({ model, pk }) => {
  const url = `/media/fetch_mediaitem_info/?model=${model}&pk=${pk}`
  return fetchUrl(url)
    .then(response => (response.ok ? response.json() : ''))
}

/**
 * Base component for media foreign keys
 */
export const MediaForeignKeyWidget = ({
  fieldContainer,
  model,
  showImage = true,
  searchLabel = 'Choose from media library',
}) => {
  // Find foreignkey field
  const input = fieldContainer.querySelector('.vForeignKeyRawIdAdminField')
  if (!input || fieldContainer.mediaFK) {
    return
  }

  // Tracks that the fk has already been applied (useful for cloned elements,
  // (like for Django inlines), so we don't apply this code twice.
  fieldContainer.mediaFK = true

  // Find the .foreignKeyImage container or create if it doesn't exist
  let imageContainer = null
  let image = null
  if (showImage) {
    imageContainer = fieldContainer.querySelector('.foreignKeyImage')
    if (!imageContainer) {
      imageContainer = document.createElement('div')
      imageContainer.className = 'foreignKeyImage empty'
      input.parentNode.appendChild(imageContainer)
      imageContainer.innerHTML = `
      <strong>
        <a>
            <div class="media-label">
              <img src="${BLANK_SRC}" />
              <span class="changelink"></span>
            </div>
        </a>
      </strong>
      `
      image = imageContainer.querySelector('img')
    }
    else {
      image = imageContainer.querySelector('img')
    }
  }

  // Find the search link for choosing a different media item
  const searchLink = input.parentNode.querySelector('.related-lookup')
  searchLink.innerHTML = searchLabel

  // Monitor fk input for changes
  monitorInput(input, value => {
    fetchMediaItemInfo({ model, pk: value })
      .then(data => {
        // remove empty class
        if (showImage) {
          imageContainer.classList.remove('empty')

          // Set image source
          image.src = data.thumbnail || data.image || ''
        }

        // Update admin changeLinks to point to the newly selected media item.
        const changeLinks = Array.from(fieldContainer.querySelectorAll('strong a'))
        changeLinks.forEach(link => {
          link.href = `${data.admin_url}?_popup=1`
          const changeLinkLabel = link.querySelector('.changelink')
          changeLinkLabel.innerHTML = data.str
        })
      })
  })
}

/**
 * FileForeignKeyWidget
 */
export const FileForeignKeyWidget = (fieldContainer) => (
  MediaForeignKeyWidget({ fieldContainer, model: 'media.File', searchLabel: '', showImage: false })
)

/**
 * ImageForeignKeyWidget
 */
export const ImageForeignKeyWidget = (fieldContainer) => (
  MediaForeignKeyWidget({ fieldContainer, model: 'media.Image' })
)

/**
 * VideoForeignKeyWidget
 */
export const VideoForeignKeyWidget = (fieldContainer) => (
  MediaForeignKeyWidget({ fieldContainer, model: 'media.Video' })
)

export const addMediaForeignKeyWidgets = () => {
  const widgets = {
    '.FileForeignKeyWidget': FileForeignKeyWidget,
    '.ImageForeignKeyWidget': ImageForeignKeyWidget,
    '.VideoForeignKeyWidget': VideoForeignKeyWidget,
  }
  const addFKWidgets = () => {
    Object.keys(widgets).forEach(className => {
      const widget = widgets[className]
      const els = Array.from(document.querySelectorAll(className))
      els.forEach(el => widget(el))
    })
  }
  addFKWidgets()
  setInterval(addFKWidgets, 250)
}


export const MediaForeignKeyWidgets = {
  init: () => addMediaForeignKeyWidgets(),
}

export default MediaForeignKeyWidgets
