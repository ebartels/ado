import domReady from '~/lib/domReady'
import MediaForeignKeyWidgets from './MediaForeignKeyWidgets'

// compoonents
domReady(() => {
  MediaForeignKeyWidgets.init()
})
