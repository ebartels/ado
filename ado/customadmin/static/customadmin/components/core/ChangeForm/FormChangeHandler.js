/* global CKEDITOR */
/* eslint no-param-reassign: off */
import debounce from 'lodash.debounce'
import { jQuery as $ } from '~/lib/jQuery'
import delay from '~/lib/delay'

/**
 * Track changes in form inputs
 */
const init = () => {
  // check if jquery loaded
  if (!$) {
    return
  }

  // Handle form changes
  let formChanged = false
  let formSubmitting = false
  const form = document.querySelectorAll('form')[0]
  const isAddForm = /add\/?$/.test(window.location.pathname)

  // Set class differently for add/change forms
  form.classList.add(isAddForm ? 'is-add-form' : 'is-change-form')

  // Serialize form data
  const serializeForm = () => {
    return $(form).serializeArray()
      .concat($(form).find('input[type="file"]').toArray()
        .map(input => ({ name: input.name, value: input.value })))
      .sort((a, b) => {
        if (a.name < b.name) return -1
        if (a.name > b.name) return 1
        return 0
      })
      .map(item => `${item.name}=${item.value}`)
      .join('&')
  }

  // Track changes to form data
  $(window).on('load', async () => {
    await delay(750)

    // Initial form state
    const initialSerialized = serializeForm()

    // Check for changes in form
    const _handleChange = () => {
      const serialized = serializeForm()
      formChanged = (serialized.length !== initialSerialized.length ||
                     serialized !== initialSerialized)
      if (formChanged) {
        form.classList.add('form--isChanged')
      }
      else {
        form.classList.remove('form--isChanged')
      }
    }
    const handleChange = debounce(_handleChange, 50, { leading: true, trailing: true })

    // handle form input changes
    $(form).on('change input', ':input', handleChange)

    // because hidden input changes are not detected
    setInterval(handleChange, 500)

    // Handle ckeditor changes
    if (window.CKEDITOR) {
      let editors = { ...CKEDITOR.instances }

      // finds all ckeditors
      const collectEditors = () => {
        editors = { ...editors, ...CKEDITOR.instances }

        // Handle changes in ckeditor instances
        Object.entries(editors).forEach(([id, editor]) => {
          editor.on('change', () => {
            const textarea = editor.element.$
            $(textarea).val(editor.getData().trim())
            handleChange()
          })
        })
      }

      setInterval(collectEditors, 500)
    }
  })

  // Track when submitting form
  $(form).on('submit', () => {
    formSubmitting = true
  })

  // Check if form changed when leaving the page
  $(window).on('beforeunload', (event) => {
    if (!formSubmitting && formChanged) {
      const msg = 'You may have unsaved changes.'
      event.returnValue = msg
      return msg
    }
    return undefined
  })
}

export default {
  init,
}
