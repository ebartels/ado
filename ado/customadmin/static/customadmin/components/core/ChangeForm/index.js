import Inline from './inlines/Inline'
import SubmitRow from './SubmitRow'
import FormChangeHandler from './FormChangeHandler'

export const isChangeForm = () => document.body.classList.contains('change-form')

const init = () => {
  if (isChangeForm()) {
    // Set up ChangeForm components
    Inline.init()
    SubmitRow.init()
    FormChangeHandler.init()

    // Make "View on Site" links open a new tab
    const viewSiteLink = document.querySelector('.viewsitelink')
    if (viewSiteLink) {
      viewSiteLink.setAttribute('target', '_blank')
    }
  }
}

export const ChangeForm = {
  init,
}

export default ChangeForm
