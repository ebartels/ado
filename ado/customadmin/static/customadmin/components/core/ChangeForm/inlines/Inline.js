import scrollToElement from 'scroll-to-element'
import domReady from '~/lib/domReady'
import { jQuery as $ } from '~/lib/jQuery'
import delay from '~/lib/delay'
import SortableInline from './SortableInline'

/**
 * Custom functionality for django's Inlines.
 * This code should run for all inlines.
 */
export const Inline = (inlineGroup) => {
  // Find inline's 'add' link and scroll to new inline when clicked
  const addLink = inlineGroup.querySelector('.add-row a')
  $(addLink).on('click', async () => {
    await delay(100)
    // Find the new inline
    const selector = '.inline-related:not(.empty-form)'
    const inline = Array.from(inlineGroup.querySelectorAll(selector)).slice(-1)[0]
    if (inline) {
      // Scroll the new inline into view
      scrollToElement(inline, {
        offset: 40,
        duration: 500,
      })
    }
  })

  // Add a class to inlines so we can style inlines to be deleted
  const inlines = inlineGroup.querySelectorAll('.inline-related')
  Array.from(inlines).forEach(inline => {
    const deleteInput = $(inline).find(':checkbox')[0]
    $(deleteInput).on('change', event => {
      if ($(deleteInput).is(':checked')) {
        inline.classList.add('will-delete')
      }
      else {
        inline.classList.remove('will-delete')
      }
    })
  })
}

/**
 * Init for all inline groups on the page
 */
const init = () => {
  domReady(() => {
    const inlineGroups = Array.from(document.querySelectorAll('.inline-group'))
    inlineGroups.forEach(Inline)
  })

  SortableInline.init()
}

export default {
  init,
}
