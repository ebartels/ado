/* eslint no-param-reassign: off */
/* global CKEDITOR */
import Sortable from 'sortablejs'
import { jQuery as $ } from '~/lib/jQuery'

import './SortableInline.scss'

/**
 * Adds functionality to allow drag & drop sorting of inlines using a specified
 * position field.
 *
 * To specify position field on Django InlineModelAdmin, set the 'classes' attribute:
 *
 * class MyInline(admin.StackedInline):
 *     classes = ['sortable', 'sortable-field__position']
 *
 * Change '__position' above to whatever the name of your position field is (must be number field)
 */
export const SortableInline = (inlineModule) => {
  // Get sortable position field name from module class
  const field = Array.from(inlineModule.classList)
    .filter(cls => {
      return cls.indexOf('sortable-field__') === 0
    })
    .map(cls => (cls.split('__').slice(-1)[0]))[0]

  // Find position rows for field ame
  const positionRows = Array.from(inlineModule.querySelectorAll(`.form-row.field-${field}`))

  // If we find readonly field, it likely means we don't have change permissions, so bail out now
  if (positionRows.find(el => el.querySelector('.readonly'))) return

  // Hide position fields for inline forms
  positionRows.forEach(el => {
    el.style.display = 'none'
  })

  // Add help title for dragging
  Array.from(inlineModule.querySelectorAll('.inline-related h3')).forEach(h3 => {
    h3.setAttribute('title', 'Drag to re-order')
    const dragHandle = document.createElement('span')
    dragHandle.classList.add('sortable-drag-handle')
    h3.insertBefore(dragHandle, h3.firstChild)
  })

  // Sortable events

  // Updates sort field for the inlines
  const updateSortOrder = (sortable) => {
    const items = sortable.toArray().filter(id => !/empty$/.test(id))
    items
      .map(id => (
        document.getElementById(id).querySelector(`.field-${field} input[name$="${field}"]`)
      ))
      .forEach((input, position) => {
        if (input) {
          $(input).val(position).trigger('change')
        }
        else {
          throw new Error(`Error: The field "${field}" was not found in the form.`)
        }
      })
  }

  // Fix for ckeditor state while sorting,
  // destroys the editor and saves data & config
  let selectedItem = null

  const saveEditorState = (item) => {
    if (typeof CKEDITOR !== 'undefined') {
      selectedItem = item

      // Set consistent height for item
      item.style.height = `${item.offsetHeight}px`

      const editorEls = item.querySelectorAll('.django-ckeditor-widget')
      Array.from(editorEls).forEach(editorEl => {
        editorEl.style.display = 'none'
        const textarea = editorEl.querySelector('textarea')
        const editorId = textarea.getAttribute('id')
        const editorInstance = CKEDITOR.instances[editorId]
        if (editorInstance) {
          const editorData = editorInstance.getData()
          const config = editorInstance.config
          editorEl.dataset.editorData = editorData
          editorEl.dataset.editorConfig = JSON.stringify(config)
          editorInstance.destroy()
        }
      })
    }
  }

  const restoreEditorState = (item) => {
    // Fix for ckeditor: restore editor with data & config
    // The editor is broken due to sort cloning the element.
    if (typeof CKEDITOR !== 'undefined' && selectedItem) {
      selectedItem = null
      const editorEls = item.querySelectorAll('.django-ckeditor-widget')
      Array.from(editorEls).forEach(editorEl => {
        editorEl.style.display = ''
        const textarea = editorEl.querySelector('textarea')
        const editorId = textarea.getAttribute('id')
        const editorData = editorEl.dataset.editorData
        const config = JSON.parse(editorEl.dataset.editorConfig || 'null')
        if (config) {
          $(textarea).val(editorData.trim())
          CKEDITOR.replace(editorId, config)
        }
      })

      // Reset item height
      item.style.height = ''
    }
  }

  // sortablejs is broken, checkboxes not restored properly
  // this saves checkbox state before sorting
  const saveCheckboxState = (item) => {
    const checkboxes = Array.from(item.querySelectorAll('input[type="checkbox"]'))
    checkboxes.forEach(checkbox => {
      $(checkbox).data('checked', checkbox.checked)
    })
  }

  // this restores checkbox state before sorting
  const restoreCheckboxState = (item) => {
    setTimeout(() => {
      const checkboxes = Array.from(item.querySelectorAll('input[type="checkbox"]'))
      checkboxes.forEach(checkbox => {
        checkbox.checked = $(checkbox).data('checked')
        delete checkbox.dataset.checked
      })
    }, 10)
  }

  // Fix: 'onChoose' firfing but not 'onEnd' when clicking on sortable element
  document.body.addEventListener('mouseup', () => {
    if (selectedItem) {
      restoreEditorState(selectedItem)
      selectedItem = null
    }
  })


  // Called when sortable item is first chosen (before dragging starts)
  const onChoose = (evt) => {
    // fix for checkboxes
    saveCheckboxState(evt.item)

    // fix for ckeditor
    saveEditorState(evt.item)
  }

  const onStart = (evt) => {
    inlineModule.classList.add('sortable--isSorting')

    // Hide submit row
    Array.from(document.querySelectorAll('.submit-row'))
      .forEach(el => {
        el.style.display = 'none'
      })
  }

  // Called when sortable is finished being sorted
  const onEnd = (evt) => {
    inlineModule.classList.remove('sortable--isSorting')

    // Show submit row
    Array.from(document.querySelectorAll('.submit-row'))
      .forEach(el => {
        el.style.display = ''
      })

    // fix for checkboxes
    restoreCheckboxState(evt.item)

    // fix for ckeditor
    restoreEditorState(evt.item)
  }

  // Configure Sortable instance
  Sortable.create(inlineModule, {
    animation: 150,
    draggable: '.inline-related',
    scroll: true,
    scrollSensitivity: 100,
    scrollSpeed: 10,
    handle: '.sortable-drag-handle',
    dataIdAttr: 'id',
    // forceFallback: true,
    onChoose,
    onStart,
    onEnd,
    store: {
      set: updateSortOrder,
      get: sortable => [], // noop
    },
  })

  // Ensure that form rows are in proper order (can get out of with a
  // combination of re-ordering and deletion of new form rows)
  const form = $(inlineModule).closest('form')[0]
  form.addEventListener('submit', () => {
    // Get form prefix
    const inlineGroup = inlineModule.parentElement
    const formset = JSON.parse(inlineGroup.dataset.inlineFormset)
    const prefix = formset.options.prefix

    // Find existing and new inline rows
    const existingRows = [...inlineModule.querySelectorAll('.inline-related.has_original')]
    const newRows = [...inlineModule.querySelectorAll('.inline-related.last-related')]

    // Update element attributes for given index
    const updateElementIndex = (el, index) => {
      const idRegex = new RegExp(`(${prefix}-(\\d+|__prefix__))`)
      const replacement = `${prefix}-${index}`
      if (el.getAttribute('for')) {
        el.setAttribute('for', el.getAttribute('for').replace(idRegex, replacement))
      }
      if (el.id) {
        el.id = el.id.replace(idRegex, replacement)
      }
      if (el.name) {
        el.name = el.name.replace(idRegex, replacement)
      }
    }

    // Update an inline row
    let rowIndex = 0
    const updateRow = (inline) => {
      updateElementIndex(inline, rowIndex)
      Array.from(inline.querySelectorAll('*')).forEach(el => updateElementIndex(el, rowIndex))
      rowIndex += 1
    }

    // Update existing and new rows
    existingRows.forEach(updateRow)
    newRows.forEach(updateRow)
  })
}


const init = () => {
  const modules = Array.from(document.querySelectorAll('.inline-group .sortable'))
  modules.forEach(SortableInline)
}

export default {
  init,
}
