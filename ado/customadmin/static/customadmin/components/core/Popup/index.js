/* eslint no-underscore-dangle: off */
const _windowOpen = window.open

/**
 * Opens a modified Django popup within an iframe by intercepting window.open
 *
 * NOTE:
 * Currently this doesn't quite work. Django hard-codes access to window.opener
 * for handling popups. The below code works for selecting from changelists,
 * but not after adding a new item due to the iframe reload.  Attempting to
 * link iframe.contentWindow.open to parent almost works except not after an
 * iframe reload.
 */
export const openIframePopup = (url, name, ...args) => {
  // Open an modal view with iframe in place of Django's window.open
  const modal = document.createElement('div')
  modal.className = 'modal-popup'

  const overlay = document.createElement('div')
  overlay.className = 'modal-overlay'
  modal.appendChild(overlay)

  const modalContent = document.createElement('div')
  modalContent.className = 'modal-popup-content'
  modal.appendChild(modalContent)

  const closeButton = document.createElement('a')
  closeButton.className = 'modal-close-button'
  modalContent.appendChild(closeButton)
  closeButton.innerHTML = '<i class="icon material-icons">close</i>'

  const iframe = document.createElement('iframe')
  iframe.id = 'popup-iframe'
  iframe.className = 'popup-iframe'
  modalContent.appendChild(iframe)

  document.body.appendChild(modal)

  let win = iframe.contentWindow
  iframe.onload = () => {
    // Set up window.opener for iframe to be compatible with Django js
    win = iframe.contentWindow
    win.opener = win.parent
    win.name = name

    // Close the popup
    win.close = () => {
      document.body.removeChild(modal)
    }

    win.focus = () => {}
  }

  iframe.src = url

  return win
}

/**
 * Modifies Django's popup window by intercepting window.open
 */
export const openWindowPopup = (url, name, specs, ...args) => {
  const width = Math.min(1400, parseInt(window.innerWidth * 0.8, 10))
  const height = parseInt(window.innerHeight * 0.80, 10)

  const requestedSpecs = specs.split(',')
    .reduce((obj, item, i) => {
      const [key, val] = item.split('=')
      return {
        [key]: val,
        ...obj,
      }
    }, {})

  const openSpecs = {
    ...requestedSpecs,
    width,
    height,
    centerscreen: 'yes',
  }

  const specStr = Object.entries(openSpecs)
    .map(entry => entry.join('='))
    .join(',')

  return _windowOpen(url, name, specStr, ...args)
}

/**
 * Initialize window.open intercept
 */
const init = () => {
  window.open = openWindowPopup
  // window.open = openIframePopup
}

export const Popup = {
  init,
}

export default Popup
