/**
 * A sidebar nav component for the Django admin
 * works in conjunction with the {% admin_app_list_json %} templatetag.
 */
/* eslint react/no-danger: 0 */
import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { default as cx } from 'classnames'
import Collapse, { Panel } from 'rc-collapse'

import getAdminData from '~/lib/getAdminData'
import Icon from '~/components/Icon'
import '~/styles/SidebarNav.scss'


/**
 * Sidebar nav UI
 */
export const SidebarNavUI = ({ apps, activeApp, activeModel, adminUrl = '/admin/' }) => (
  <div className="customadmin-SidebarNav">
    <a href={adminUrl}><Icon>home</Icon>Home</a>

    <Collapse
      defaultActiveKey={activeApp}
      className="customadmin-SidebarNav__collapse">
      {apps.map((app, i) => (
        <Panel
          key={app.app_label}
          showArrow={false}
          header={(
            <a className={cx({ active: activeApp === app.app_label })}>
              {app.icon
                ? <span dangerouslySetInnerHTML={{ __html: app.icon }} />
                : <Icon>apps</Icon>}
              <span className="app-name">{app.name}</span>
            </a>
          )}
        >
          {app.models.filter(model => !model.is_hidden).map((model, j) => (
            <a
              key={model.object_name}
              className={cx('app-link', {
                active: (model.object_name.toLowerCase() === activeModel
                         && app.app_label === activeApp),
              })}
              href={model.admin_url || false}>{model.name}
            </a>
          ))}
        </Panel>
      ))}
    </Collapse>
  </div>
)

SidebarNavUI.propTypes = {
  apps: PropTypes.array.isRequired,
  activeApp: PropTypes.string,
  activeModel: PropTypes.string,
  adminUrl: PropTypes.string,
}


/**
 * Container class for sidebar nav
 */
export class SidebarNavContainer extends React.Component {
  static propTypes = {
    apps: PropTypes.array,
    isAuthenticated: PropTypes.bool,
  }

  bodyActiveClass = 'SidebarNav--active'
  bodyInactiveClass = 'SidebarNav--inactive'

  constructor(props) {
    super(props)

    document.body.classList.add(this.props.isAuthenticated
      ? this.bodyActiveClass
      : this.bodyInactiveClass)
  }

  componentDidMount() {
    // todo open/close functionality
  }

  componentWillUnmount() {
    document.body.classList.remove(this.props.isAuthenticated
      ? this.bodyActiveClass
      : this.bodyInactiveClass)
  }

  getActiveAppInfo = () => {
    const path = window.location.pathname
      .split('/')
      .filter(segment => !!segment)

    const [adminUrl, activeApp, activeModel] = path
    return {
      activeApp,
      activeModel,
      adminUrl: `/${adminUrl}/`,
    }
  }

  render() {
    if (!this.props.isAuthenticated) {
      return null
    }

    const { adminUrl, activeApp, activeModel } = this.getActiveAppInfo()

    if (!this.props.apps) {
      return null
    }

    const apps = this.props.apps.filter(app => !app.is_hidden)

    return (
      <SidebarNavUI
        apps={apps}
        activeApp={activeApp}
        activeModel={activeModel}
        adminUrl={adminUrl}
      />
    )
  }
}


/**
 * Mount component into DOM. This relies on their being a script tag with JSON
 * data to pass to the component.
 *
 * <script id="customadmin-sidebarnav-json"
 *         type="application/json">{% admin_app_list_json %}</script>
 */
const init = () => {
  const renderComponent = () => {
    const adminData = getAdminData()

    // Add sidebar div
    const componentRoot = document.createElement('div')
    document.body.appendChild(componentRoot)

    // Render component
    ReactDOM.render(
      <SidebarNavContainer
        apps={adminData.available_apps}
        isAuthenticated={adminData.is_authenticated}
      />,
      componentRoot,
    )
  }

  renderComponent()
}

export const SidebarNav = {
  init,
}

export default SidebarNav
