import queryString from 'query-string'

/**
 * Enables multiple selections for custom MultiListFilter
 */
export const MultiSelectFilter = (filtersEl) => {
  // find filters with class .multi-filter
  const multiFilters = Array.from(filtersEl.querySelectorAll('.multi-filter'))

  multiFilters.forEach(listEl => {
    const param = listEl.dataset.parameterName

    const links = Array.from(listEl.querySelectorAll('a'))
      .filter((link, i) => i > 0)
    links.forEach(link => {
      link.addEventListener('click', (e) => {
        e.preventDefault()
        const query = queryString.parse(window.location.search)
        const linkQuery = queryString.parse(queryString.extract(link.href))

        if (link.parentNode.classList.contains('selected')) {
          query[param] = (query[param] || '')
            .split(',')
            .filter(val => val !== linkQuery[param])
            .filter(val => !!val)
            .join(',') || undefined
        }
        else {
          query[param] = [
            ...(query[param] || '').split(',').filter(val => !!val),
            linkQuery[param],
          ].join(',') || undefined
        }

        const qString = queryString.stringify(query, { encode: false })
        const url = `${window.location.pathname}?${qString}`
        window.location = url
      })
    })
  })
}

/**
 * Customizations for changelist filters
 */
export const ChangeListFilters = () => {
  // Find filter container
  const filtersEl = document.getElementById('changelist-filter')
  if (!filtersEl) {
    return
  }

  // Find filter headings
  const filterHeadings = Array.from(filtersEl.querySelectorAll('h3'))

  // Don't use collapsibles if only 1 filter and only 10 items or less
  if (filterHeadings.length < 2) {
    if (filterHeadings[0].nextElementSibling.querySelectorAll('li').length) {
      return
    }
  }

  filterHeadings.forEach(heading => {
    // Give collapsible class
    heading.classList.add('collapsible')

    // Each heading has a list of items (the filter links)
    const listEl = heading.nextElementSibling
    const listItems = Array.from(listEl.querySelectorAll('li'))

    // Find the seleted item
    const selectedItem = listItems.find(item => item.classList.contains('selected'))
    const selectedIndex = listItems.indexOf(selectedItem)

    // Add heading icon
    const collapseIcon = document.createElement('span')
    collapseIcon.classList.add('collapse-icon')
    heading.appendChild(collapseIcon)

    // If a filter is selected
    const openClass = 'collapsible-open'
    if (selectedIndex > 0) {
      heading.classList.add(openClass)
      listEl.classList.add(openClass)
    }

    // Toggle collapse
    heading.addEventListener('click', (e) => {
      if (heading.classList.contains(openClass)) {
        heading.classList.remove(openClass)
        listEl.classList.remove(openClass)
      }
      else {
        heading.classList.add(openClass)
        listEl.classList.add(openClass)
      }
    })
  })

  // Multi select filters
  MultiSelectFilter(filtersEl)
}

export default ChangeListFilters
