import ChangeListSearch from './ChangeListSearch'
import ChangeListActions from './ChangeListActions'
import ChangeListFilters from './ChangeListFilters'

export const isChangeList = () => document.body.classList.contains('change-list')

// ChangeList search bar
const init = () => {
  if (document.body.classList.contains('change-list')) {
    ChangeListSearch()
    ChangeListActions()
    ChangeListFilters()
  }
}

export const ChangeList = {
  init,
}

export default ChangeList
