import Sortable from 'sortablejs'
import fetchUrl from '~/lib/fetchUrl'

import './SortableAdminList.scss'

/**
 * Uses sortablejs to make sortable admin list
 */
export const SortableAdminList = () => {
  // Find container
  const containers = Array.from(document.querySelectorAll('.sortable-objects'))
  if (!containers.length) {
    return
  }

  // Update sortable numbers
  const updateNumbers = (container) => {
    const sortables = Array.from(container.querySelectorAll('.sortable-objects__object'))
    sortables.forEach((sortable, i) => {
      const el = sortable.querySelector('.sortable_objects__position')
      el.innerText = `${i + 1}.`
    })
  }

  // Update sortable positions with server
  const savePositions = ({ items, groupName, groupValue, extraData }) => {
    const url = `${window.location.pathname}update/`
    return fetchUrl(url, {
      method: 'POST',
      body: JSON.stringify({
        group_name: groupName,
        group_value: groupValue,
        positions: items,
        extraData,
      }),
    })
  }

  // Add sortable
  const sortables = containers.map(container => {
    updateNumbers(container)

    return Sortable.create(container, {
      group: 'sortable-admin-list',
      scroll: true,
      scrollSensitivity: 100,
      scrollSpeed: 10,
      animation: 150,
      onEnd: event => {
        sortables.forEach(sortable => {
          const listEl = sortable.el
          const items = sortable.toArray()
          const groupName = listEl.dataset.group_name
          const groupValue = listEl.dataset.group_value
          const extraData = JSON.parse(listEl.dataset.extraData || '{}')
          savePositions({ items, groupName, groupValue, extraData })
          updateNumbers(listEl)
        })
      },
    })
  })
}

const init = () => {
  SortableAdminList()
}

export default {
  init,
}
