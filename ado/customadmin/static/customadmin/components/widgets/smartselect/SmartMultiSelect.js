/* eslint no-param-reassign: off, react/no-find-dom-node: off */
import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import Select from 'react-select'
import { jQuery as $ } from '~/lib/jQuery'

import './SmartMultiSelect.scss'


/**
 * Uses react-select to render a select component
 */
export const propTypes = {
  options: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
}

export const SmartMultiSelectWidget = (props) => (
  <span>
    <Select
      multi
      autoBlur
      simpleValue
      placeholder={props.placeholder || 'Select...'}
      value={props.value}
      name={props.name}
      options={props.options}
      inputProps={{
        autoComplete: 'off',
        autoCorrect: 'off',
        autoCapitalize: 'off',
        spellCheck: 'false',
      }}
      onChange={props.onChange}
    />
  </span>
)

SmartMultiSelectWidget.propTypes = propTypes


/**
 * Container that renders a SmartMultiSelectWidget based on a given select form
 * element in the Django admin.
 */
export class SmartMultiSelectWidgetContainer extends React.Component {
  static propTypes = {
    select: PropTypes.object.isRequired,
  }

  state = {
    options: [],
    name: '',
    value: '',
  }

  componentDidMount() {
    this.el = ReactDOM.findDOMNode(this)

    const { select } = this.props
    this.setOptionsFromSelect()

    // Set state from select element
    const name = select.name
    this.setState({
      name,
    })

    // Hide select and remove name
    select.setAttribute('name', '')
    select.style.position = 'absolute'
    select.style.zIndex = '-1'
    select.style.visibility = 'hidden'
    select.style.top = '-9999px'
    select.style.left = '-9999px'
    $(select).on('change', this.onSelectChange)
    // select.parentNode.removeChild(select)
  }

  componentWillUnmount() {
    const { select } = this.props
    $(select).off('change', this.onSelectChange)
  }

  setOptionsFromSelect = () => {
    // Get select options and convert to objects
    const { select } = this.props
    const opts = Array.from(select.querySelectorAll('option'))
    const options = opts
      .filter(opt => !!opt.value)
      .map(opt => ({ value: opt.value, label: opt.text }))

    // Get selected options
    const values = opts
      .filter(o => o.selected)
      .map(o => o.value || o.text)

    this.setState({
      options,
      value: values.join(','),
    })

    // Clear selected options
    opts.forEach(opt => { opt.removeAttribute('selected') })
  }

  onSelectChange = () => {
    const { select } = this.props

    // Sync options
    const opts = Array.from(select.querySelectorAll('option'))
    const options = opts
      .filter(opt => !!opt.value)
      .map(opt => ({ value: opt.value, label: opt.text }))

    // Sync option values
    const values = this.state.value.split(',').filter(p => !!p)
    const selectValues = opts
      .filter(o => o.selected)
      .map(o => o.value || o.text)
    const newValues = values.concat(selectValues)

    this.setState({
      options,
      value: newValues.join(','),
    })
  }

  handleChange = (value) => {
    // Update value
    this.setState({
      value,
    })

    // Trigger input change event
    setTimeout(() => {
      $(this.el).find(':input').trigger('change')
    }, 50)
  }

  render() {
    return (
      <SmartMultiSelectWidget
        name={this.state.name}
        options={this.state.options || []}
        value={this.state.value}
        placeholder={this.props.select.dataset.placeholder || null}
        onChange={this.handleChange}
      />
    )
  }
}


/**
 * Add SmartMultiSelectWidget to DOM, replacing normal select element
 */
export const addSmartMultiSelect = (select) => {
  // Create component container element
  const container = document.createElement('div')
  container.className = 'SmartMultiSelectContainer'
  select.parentNode.insertBefore(container, select)

  // Remove help text message added by Django for select fields
  const helpDiv = select.parentNode.parentNode.querySelector('.help')
  if (helpDiv) {
    helpDiv.innerText = helpDiv.innerText.replace(/Hold down "Control".+$/, '')
  }

  // Render component
  ReactDOM.render(
    <SmartMultiSelectWidgetContainer
      select={select}
    />,
    container,
  )
}

export const addSmartMultiSelects = () => {
  // Find .SmartMultiSelect components and replace
  const selects = Array.from(document.querySelectorAll('select.SmartMultiSelect'))
  selects.forEach(addSmartMultiSelect)
}

const init = () => {
  addSmartMultiSelects()
}

export const SmartMultiSelect = {
  init,
}

export default SmartMultiSelect
