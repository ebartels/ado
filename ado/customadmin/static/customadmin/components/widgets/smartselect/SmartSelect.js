/* eslint no-param-reassign: off, react/no-find-dom-node: off */
import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import Select from 'react-select'
import { jQuery as $ } from '~/lib/jQuery'

import './SmartSelect.scss'

/**
 * Uses react-select to render a select component
 */
const propTypes = {
  options: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
}

export const SmartSelectWidget = (props) => (
  <Select
    autoBlur
    simpleValue
    placeholder={props.placeholder || 'Select...'}
    value={props.value}
    name={props.name}
    options={props.options}
    inputProps={{
      autoComplete: 'off',
      autoCorrect: 'off',
      autoCapitalize: 'off',
      spellCheck: 'false',
    }}
    onChange={props.onChange}
  />
)

SmartSelectWidget.propTypes = propTypes


/**
 * Container that renders a SmartSelectWidget based on a given select form
 * element in the Django admin.
 */
export class SmartSelectWidgetContainer extends React.Component {
  static propTypes = {
    select: PropTypes.object.isRequired,
    relatedLink: PropTypes.object,
  }

  state = {
    options: [],
    name: '',
    value: null,
  }

  componentDidMount(props) {
    this.el = ReactDOM.findDOMNode(this)

    const { select } = props
    this.setOptionsFromSelect()

    // Set state from select element
    const name = select.name
    this.setState({
      name,
    })

    // Hide select and remove name
    select.setAttribute('name', '')
    select.style.position = 'absolute'
    select.style.zIndex = '-1'
    select.style.top = '-9999px'
    select.style.left = '-9999px'
    select.style.visibility = 'hidden'
    $(select).on('change', this.onSelectChange)
    // select.parentNode.removeChild(select)
  }

  componentWillUnmount() {
    const { select } = this.props
    $(select).off('change', this.onSelectChange)
  }

  setOptionsFromSelect = () => {
    const { select } = this.props
    const options = Array.from(select.querySelectorAll('option'))
      .filter(opt => !!opt.value)
      .map(opt => ({ value: opt.value, label: opt.text }))

    this.setState({
      options,
      value: select.value,
    })
  }

  onSelectChange = (e) => {
    this.setOptionsFromSelect()
  }

  handleChange = (value) => {
    // Update value
    this.setState({
      value,
    })

    // Update the related link
    this.updateRelatedLink(value)

    $(this.el).trigger('SmartSelect.updated', value)

    // Trigger input change event
    setTimeout(() => {
      $(this.el).find(':input').trigger('change')
    }, 50)
  }

  updateRelatedLink = (value) => {
    // Update django's related link url
    const link = this.props.relatedLink
    if (link) {
      if (value) {
        const hrefTemplate = link.getAttribute('data-href-template')
        const href = hrefTemplate.replace('__fk__', value)
        link.href = href
      }
      else {
        link.removeAttribute('href')
      }
    }
  }

  render() {
    return (
      <SmartSelectWidget
        name={this.state.name}
        options={this.state.options || []}
        value={this.state.value}
        placeholder={this.props.select.dataset.placeholder || null}
        onChange={this.handleChange}
      />
    )
  }
}


/**
 * Add SmartSelectWidget to DOM, replacing normal select element
 */
export const addSmartSelect = (select) => {
  // Create component container element
  const container = document.createElement('div')
  container.className = 'SmartSelectContainer'
  select.parentNode.insertBefore(container, select)

  // Related edit link
  const relatedLink = select.parentNode.querySelector('a.related-widget-wrapper-link')

  // Render component
  ReactDOM.render(
    <SmartSelectWidgetContainer
      select={select}
      relatedLink={relatedLink}
    />,
    container,
  )
}

export const addSmartSelects = () => {
  // Find .SmartSelect components and replace
  const selects = Array.from(document.querySelectorAll('select.SmartSelect'))
  selects.forEach(addSmartSelect)
}

const init = () => {
  addSmartSelects()
}

export const SmartSelect = {
  init,
}

export default SmartSelect
