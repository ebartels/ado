import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { Creatable } from 'react-select'
import {
  SmartMultiSelectWidgetContainer,
  propTypes as defaultPropTypes,
} from './SmartMultiSelect'


export const propTypes = {
  ...defaultPropTypes,
  isOptionUnique: PropTypes.func,
}

/**
 *{ option: Object, options: Array, labelKey: string, valueKey: string }
 */
const isTagUnique = ({ option, options, labelKey, valueKey }) => (
  options
    .filter((existingOption) => (
      existingOption[labelKey].toLowerCase() === option[labelKey].toLowerCase() ||
      existingOption[valueKey].toLowerCase() === option[valueKey].toLowerCase()
    ))
    .length === 0
)

/**
 * SmartTagSelectWidget widget using react-select Creatable
 */
export const SmartTagSelectWidget = (props) => (
  <span>
    <Creatable
      multi
      autoBlur
      simpleValue
      placeholder={props.placeholder || 'Select...'}
      value={props.value}
      name={props.name}
      options={props.options}
      inputProps={{
        autoComplete: 'off',
        autoCorrect: 'off',
        autoCapitalize: 'off',
        spellCheck: 'false',
      }}
      onChange={props.onChange}
      isOptionUnique={isTagUnique}
      promptTextCreator={() => ('Add new tag')}
    />
  </span>
)

SmartTagSelectWidget.propTypes = propTypes


/**
 * SmartMultiSelectWidget for tags
 */
export class SmartTagSelect extends SmartMultiSelectWidgetContainer {
  render() {
    return (
      <SmartTagSelectWidget
        name={this.state.name}
        options={this.state.options || []}
        value={this.state.value}
        placeholder={this.props.select.dataset.placeholder || null}
        onChange={this.handleChange}
      />
    )
  }
}


/**
 * Add SmartMultiSelectWidget to DOM, replacing normal select element
 */
export const addSmartMultiSelect = (select) => {
  // Create component container element
  const container = document.createElement('div')
  container.className = 'SmartMultiSelectContainer'
  select.parentNode.insertBefore(container, select)

  // Render component
  ReactDOM.render(
    <SmartTagSelect
      select={select}
    />,
    container,
  )
}

export const addSmartMultiSelects = () => {
  // Find .SmartTagSelect components and replace
  const selects = Array.from(document.querySelectorAll('select.SmartTagSelect'))
  selects.forEach(addSmartMultiSelect)
}

const init = () => {
  addSmartMultiSelects()
}

export const SmartMultiSelect = {
  init,
}

export default SmartMultiSelect
