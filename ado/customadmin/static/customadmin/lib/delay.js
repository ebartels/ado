/**
 * setTimeout but promise based
 *
 * Example:
 *
 * delay(100).then(() => { ..do stuff })
 *
 * OR
 *
 * await delay(100)
 * ... do stuff
 */
export const delay = (duration) => (
  new Promise(resolve => setTimeout(resolve, duration))
)

export default delay
