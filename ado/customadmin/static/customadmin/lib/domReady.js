import 'core-js/es6/promise'
import ready from 'document-ready-promise'

/**
 * Similar to jquery .ready
 */
export function domReady(fn = () => {}) {
  return ready().then(fn)
}

export default domReady
