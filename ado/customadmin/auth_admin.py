from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.sites import NotRegistered
from django.contrib.auth import admin as auth_admin
from django.contrib.auth.models import User, Group
from django.contrib.auth import forms as auth_forms

from ado.customadmin.admin import BaseModelAdminMixin

try:
    admin.site.unregister(User)
except NotRegistered:
    pass


def _filter_permissions(qs):
    # Filter out certain content type permissions
    # These don't need admin permissions
    return (qs.exclude(content_type__app_label='admin')
              .exclude(content_type__app_label='contenttypes')
              .exclude(content_type__app_label='sessions')
              .exclude(content_type__app_label='thumbnail')
          )


class UserCreationForm(auth_forms.UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # We want users created in the admin to be staff by default
        self.fields['is_staff'].initial = True


@admin.register(User)
class UserAdmin(BaseModelAdminMixin, auth_admin.UserAdmin):
    icon = '<i class="icon material-icons">person</i>'
    dashboard_order = 0
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'groups_col')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Important dates'), {'fields': (('last_login', 'date_joined'),)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2',),
        }),
        (_('Permissions'), {
            'classes': ('wide',),
            'fields': (('is_staff', 'is_superuser'),),
        }),
    )
    readonly_fields = ['last_login', 'date_joined']
    filter_horizontal = ['user_permissions']
    smartselect_fields = ['groups']
    add_form = UserCreationForm

    def groups_col(self, obj):
        return ', '.join([str(g) for g in obj.groups.all()])

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'user_permissions':
            qs = kwargs.get('queryset', db_field.remote_field.model.objects)

            qs = _filter_permissions(qs)

            # Avoid a major performance hit resolving permission names which
            # triggers a content_type load:
            kwargs['queryset'] = qs.select_related('content_type')
        return super().formfield_for_manytomany(db_field, request=request, **kwargs)


try:
    admin.site.unregister(Group)
except NotRegistered:
    pass


@admin.register(Group)
class GroupAdmin(BaseModelAdminMixin, auth_admin.GroupAdmin):
    icon = '<i class="icon material-icons">groups</i>'
    dashboard_order = 1

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'permissions':
            qs = kwargs.get('queryset', db_field.remote_field.model.objects)

            qs = _filter_permissions(qs)

            # Avoid a major performance hit resolving permission names which
            # triggers a content_type load:
            kwargs['queryset'] = qs.select_related('content_type')
        return super().formfield_for_manytomany(db_field, request=request, **kwargs)
