import json
import sys

from django.apps import apps
from django.template import Library
from django.contrib.admin import site as admin_site
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings

register = Library()


@register.simple_tag(takes_context=True)
def get_app_list(context, app_list=None):
    # Get app_list from context, or from admin_site if not available
    if app_list is None:
        app_list = context.get('available_apps')
        if app_list is None:
            request = context.get('request')
            if request:
                app_list = admin_site.get_app_list(request)

    # Could not find an app_list
    if app_list is None:
        return []

    # App config specifies which appes/models to show and in which order. If
    # None, it will use Django's default app/model ordering.
    dashboard_apps = getattr(settings, 'CUSTOMADMIN_DASHBOARD_APPS', None)
    dashboard_hidden_apps = getattr(settings, 'CUSTOMADMIN_DASHBOARD_HIDDEN_APPS', None)

    if dashboard_hidden_apps:
        for app in app_list:
            app['is_hidden'] = app['app_label'] in dashboard_hidden_apps

    # Filter and sort app_list by admin config setting
    if dashboard_apps:

        def app_sort_key(a):
            return (dashboard_apps.index(a['app_label'])
                    if a['app_label'] in dashboard_apps
                    else float('inf'))

        app_list.sort(key=app_sort_key)

    # Get all registered ModelAdmins
    model_admins = dict([
        ((model._meta.app_label, model._meta.model_name), admin)
        for (model, admin) in list(admin_site._registry.items())])

    # Get icons for apps & models
    for app in app_list:
        app_label = app['app_label']

        # Get any configured app icon
        app_config = apps.get_app_config(app_label)
        app['icon'] = mark_safe(getattr(app_config, 'icon', ''))

        # Get model admins
        app_models = app['models']
        for model in app_models:
            model_name = model['object_name'].lower()
            model_admin = model_admins.get((app_label, model_name), None)
            if model_admin:
                model['icon'] = mark_safe(getattr(model_admin, 'icon', ''))
                model['order'] = getattr(model_admin, 'dashboard_order', sys.maxsize)
                model['is_hidden'] = getattr(model_admin, 'hidden', False)

        # Sort models by order
        app['models'].sort(key=lambda m: m['order'])

    return app_list


@register.simple_tag(takes_context=True)
def admin_app_json(context):
    request = context.get('request', None)
    if not request:
        return ''

    user = request.user

    available_apps = get_app_list(context)
    has_permission = context.get('has_permission')
    admin_index_url = reverse('admin:index')

    opts = context.get('opts')
    if opts:
        opt_data = {
            'model_name': opts.model_name,
            'app_label': opts.app_label,
            'verbose_name': opts.verbose_name,
            'verbose_name_plural': opts.verbose_name_plural,
        }
    else:
        opt_data = None

    json_data = {
        'admin_index_url': admin_index_url,
        'available_apps': available_apps,
        'has_permission': has_permission,
        'is_authenticated': user.is_authenticated,
        'opts': opt_data,
    }

    json_string = json.dumps(json_data, cls=DjangoJSONEncoder)
    return mark_safe(json_string)
