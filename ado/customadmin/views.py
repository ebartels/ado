from django.apps import apps
from django.shortcuts import get_object_or_404
from django.utils.datastructures import MultiValueDictKeyError
from django import http


def fetch_url(request):
    try:
        model_string = request.GET['model']
        pk = request.GET['pk']
    except MultiValueDictKeyError:
        raise http.Http404

    try:
        (app_label, model_name) = model_string.split('.')
    except ValueError:
        raise http.Http404

    model = apps.get_model(app_label, model_name)
    if model is None:
        raise http.Http404

    obj = get_object_or_404(model, pk=pk)

    try:
        url = obj.get_absolute_url()
    except AttributeError:
        raise http.Http404

    return http.HttpResponse(url)
