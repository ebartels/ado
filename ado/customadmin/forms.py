from django import forms
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.contrib.admin.widgets import ForeignKeyRawIdWidget, AdminFileWidget
from django.template import Template, Context
from django.conf import settings
from django.utils.datastructures import MultiValueDict

from taggit.models import Tag
from taggit.forms import TagField
from webpack_loader import utils

from ado.media import resizer


class SmartSelectMediaMixin(object):
    @property
    def media(self):
        # add webpack assets
        webpack_js = utils.get_files('smartselect', 'js', config='CUSTOMADMIN')
        webpack_css = utils.get_files('smartselect', 'css', config='CUSTOMADMIN')

        super_media = super().media
        dependencies = ['admin/js/jquery.init.js']
        media = forms.Media(
            js=dependencies + [f['url'] for f in webpack_js],
            css={**super_media._css, **{'all': [f['url'] for f in webpack_css]}},
        )

        return super_media + media


class SmartSelect(SmartSelectMediaMixin, forms.Select):
    """
    A select widget that uses react-select, a better searchable select field.
    """
    def __init__(self, attrs=None, choices=()):
        default_attrs = {
            'class': 'SmartSelect',
        }
        default_attrs.update(attrs or {})
        super().__init__(default_attrs, choices)


class SmartMultiSelect(SmartSelectMediaMixin, forms.SelectMultiple):
    """
    A multiple select widget that uses react-select, a better searchable
    select widget.
    """
    def __init__(self, attrs=None, choices=()):
        default_attrs = {
            'class': 'SmartMultiSelect',
        }
        default_attrs.update(attrs or {})
        super().__init__(default_attrs, choices)


class TagSelect(forms.SelectMultiple):
    def __init__(self, attrs=None, tag_model=Tag):
        super().__init__(attrs)
        self.tag_model = tag_model

    @property
    def choices(self):
        return [(tag.name, tag.name) for tag in self.tag_model.objects.all()]

    @choices.setter
    def choices(self, val):
        pass

    def value_from_datadict(self, data, files, name):
        if isinstance(data, MultiValueDict):
            items = data.getlist(name)
        else:
            items = data.get(name)
        return ','.join(items)

    def render(self, name, value, attrs=None, renderer=None):
        if value is None:
            value = []
        if isinstance(value, str):
            value = [s.strip() for s in value.split(',')]
        else:
            value = [t.name for t in value]
        return super().render(name, value, attrs)


class SmartTagSelect(SmartSelectMediaMixin, TagSelect):
    def __init__(self, attrs=None):
        default_attrs = {
            'class': 'SmartTagSelect',
        }
        default_attrs.update(attrs or {})
        super().__init__(default_attrs)


class TagEditForm(forms.Form):
    """
    A form that can be used to edit tags.
    This is currently used in admin actions to tag multiple items.
    """
    tags = TagField(
        widget=SmartTagSelect,
        help_text="Select one or more tags")
    action = forms.ChoiceField(choices=(
        ('add', 'Add tags'),
        ('remove', 'Remove tags'),
    ), initial='add', help_text="Select an action")

    @property
    def media(self):
        parent_media = super().media
        extra = '' if settings.DEBUG else '.min'
        return forms.Media(
            js=[
                'admin/js/vendor/jquery/jquery{0}.js'.format(extra),
                'admin/js/jquery.init.js',
                'admin/js/core.js',
            ],
            css={'all': ['admin/css/forms.css']},
        ) + parent_media


class ImageInputWidget(AdminFileWidget):
    """
    A file input for images showing thumbnail of current image.
    """
    def __init__(self, *args, **kwargs):
        self.display_size = kwargs.pop('display_size', (150, 150))
        self.thumbnail_format = kwargs.pop('thumbnail_format', 'JPEG')
        super().__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, renderer=None):
        """ Render parent fileinput widget along with a thumbnail. """
        if type(value) == list:
            value = value[0]
        output = []
        output.append(super().render(name, value, attrs))
        if value:
            source = """{% load thumbnailer %}
            <a href="{{media_url}}{{value}}" class="thumb" target="_blank">
            {% thumbnail value size format=format as thumb %}
            <img src="{{thumb.url}}" width="{{thumb.width}}" />
            {% endthumbnail %}
            </a><br/>"""
            t = Template(source)
            o = t.render(Context({
                'media_url': settings.MEDIA_URL,
                'value': value,
                'size': 'x'.join(str(s) for s in self.display_size),
                'format': self.thumbnail_format,
            }))
            output.append(o)
        return mark_safe(''.join(output))


class MediaItemTypeForeignKeyWidget(ForeignKeyRawIdWidget):
    template_name = 'admin/media/widgets/mediaitem_foreign_key_widget.html'

    @property
    def model_name(self):
        raise NotImplementedError('Must add model_name property')

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)

        context['widget_class'] = self.__class__.__name__

        related_url = reverse('admin:media_{0}_changelist'.format(self.model_name))
        params = self.url_parameters()
        if params:
            related_url += '?' + '&amp;'.join('{}={}'.format(k, v) for k, v in params.items())
        context['related_url'] = mark_safe(related_url)

        return context

    @property
    def media(self):
        # add webpack assets
        webpack_js = utils.get_files('media_widgets', 'js', config='CUSTOMADMIN')
        webpack_css = utils.get_files('media_widgets', 'css', config='CUSTOMADMIN')

        super_media = super().media
        dependencies = ['admin/js/jquery.init.js']
        media = forms.Media(
            js=dependencies + [f['url'] for f in webpack_js],
            css={**super_media._css, **{'all': [f['url'] for f in webpack_css]}},
        )

        return super_media + media


class ImageForeignKeyWidget(MediaItemTypeForeignKeyWidget):
    """
    Special ForeignKey widget for media.Image modmel
    """
    model_name = 'image'

    def label_and_url_for_value(self, value):
        key = self.rel.get_related_field().name
        try:
            obj = self.rel.model._default_manager.using(self.db).get(**{key: value})
        except (ValueError, self.rel.model.DoesNotExist):
            return '', ''

        url = reverse('admin:media_image_change', args=(obj.pk,))

        thumb = resizer.get_resized(obj.filename, '360x300')
        label = f"""
        <div class="media-label">
            <div class="foreignKeyImage">
                <img src="{thumb.url}" width="{thumb.width}" />
            </div>
            <span class="changelink">{obj}</span>
        </div>
        """

        return mark_safe(label), url


class FileForeignKeyWidget(MediaItemTypeForeignKeyWidget):
    """
    Raw ForeignKey Widget for File model.
    Displays link to file along with widget.
    """
    model_name = 'file'

    def label_and_url_for_value(self, value):
        key = self.rel.get_related_field().name
        try:
            obj = self.rel.model._default_manager.using(self.db).get(**{key: value})
        except (ValueError, self.rel.model.DoesNotExist):
            return '', ''

        url = reverse('admin:media_file_change', args=(obj.pk,))

        t = Template("""
        <div class="media-label">
            <span class="changelink">{{obj}}</span>
        </div>
        """)
        return t.render(Context({'obj': obj})), url


class VideoForeignKeyWidget(MediaItemTypeForeignKeyWidget):
    """
    Raw ForeignKey Widget for Video model.
    Displays link to video along with widget.
    """
    model_name = 'video'

    def label_and_url_for_value(self, value):
        key = self.rel.get_related_field().name
        try:
            obj = self.rel.model._default_manager.using(self.db).get(**{key: value})
        except self.rel.model.DoesNotExist:
            return '', ''

        url = reverse('admin:media_video_change', args=(obj.pk,))

        thumb = resizer.get_resized(obj.video_image, '360x300')
        label = f"""
        <div class="foreignKeyImage">
            <div class="foreignKeyImage">
                <img src="{thumb.url}" width="{thumb.width}" />
            </div>
            <span class="changelink">{obj}</span>
        </div>
        """

        return mark_safe(label), url
