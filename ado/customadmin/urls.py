from django.urls import path
from . import views

urlpatterns = [
    path('fetch_url', views.fetch_url),
]
