import graphene


class ContentItemInterface(graphene.Interface):
    """
    Interface for ContentType model sublcasses
    """
    type = graphene.String()

    def resolve_type(self, info, **kwargs):
        return self.type
