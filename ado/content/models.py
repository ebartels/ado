from django.db import models
from django.db.models.base import ModelBase
from django.core.exceptions import ImproperlyConfigured
from django.core.exceptions import ValidationError
from django.urls import reverse

from positions import PositionField


class ContentItemMeta(ModelBase):
    """
    Metaclass ContentItem
    """
    def __new__(meta, name, bases, attrs):
        super_new = super().__new__

        # Return super if not ContentItem is not parent class
        parent = [b for b in bases if isinstance(b, ModelBase)][0]
        if parent.__name__ != 'ContentItem':
            return super_new(meta, name, bases, attrs)

        # Get related model properties
        try:
            related_model = attrs.pop('related_model')
        except KeyError:
            raise ImproperlyConfigured(
                'A "related_model" property must be added to the "{0}" model'.format(name))

        if not isinstance(related_model, type) or not issubclass(related_model, models.Model):
            raise ImproperlyConfigured(
                'The "related_model" property on "{0}" must be a model subclass'.format(
                    name,
                ))

        related_property = attrs.get('related_property', related_model._meta.model_name)

        # Define meta class
        if 'Meta' in attrs:
            attr_meta = attrs.pop('Meta')
            meta_bases = (ContentItem.Meta, attr_meta)
        else:
            meta_bases = (ContentItem.Meta,)

        Meta = type(str('Meta'), meta_bases, {})

        if not hasattr(Meta, 'ordering'):
            Meta.ordering = (related_property, 'position')

        # Class attributes
        new_attrs = {
            'Meta': Meta,

            # bookkeeping attributes
            'related_model': related_model,
            'related_property': related_property,

            # model fields
            related_property: models.ForeignKey(
                related_model,
                on_delete=models.CASCADE,
                related_name='content',
            ),
            'position': PositionField(collection=related_property),
        }

        new_attrs.update(attrs)

        # Create new model class
        new_class = super_new(meta, name, bases, new_attrs)
        return new_class


class ContentItem(models.Model, metaclass=ContentItemMeta):
    """
    Base class for content items.

    This class must be subclassed and a "related_model" property added pointing
    to the django model content should be associated with.
    """
    # Only one of these should be used for each instance.
    text = models.TextField(blank=True)
    image = models.ForeignKey(
        'media.Image',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_related",
        related_query_name="%(app_label)s_%(class)ss",
    )
    video = models.ForeignKey(
        'media.Video',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_related",
        related_query_name="%(app_label)s_%(class)ss",
    )

    class Meta(object):
        verbose_name = 'Content Item'
        abstract = True

    def __str__(self):
        return '{0}: {1}'.format(
            self.label,
            getattr(self, self.related_property),
        )

    def clean(self):
        # Make sure one type of content is used
        content_fields = ['text', 'image', 'video']
        content_values = [getattr(self, field) for field in content_fields]
        has_content_value = any(content_values)
        if not has_content_value:
            msg = 'No content was provided for this item'
            raise ValidationError({
                'text': msg,
                'image': msg,
                'video': msg,
            })

    @property
    def type(self):
        if self.text:
            return 'text'
        elif self.image:
            return 'image'
        elif self.video:
            return 'video'
        return 'text'

    @property
    def label(self):
        if self.image:
            return 'Image'
        elif self.video:
            return 'Video'
        elif self.text:
            return 'Text Section'
        return ''

    @property
    def instance(self):
        return getattr(self, self.related_property)

    def parent_admin_url(self):
        url = 'admin:{app}_{model}_change'.format(
            app=self.related_model._meta.app_label,
            model=self.related_model._meta.model_name,
        )
        return reverse(url, args=[self.instance.pk])
