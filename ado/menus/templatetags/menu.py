import json

from django import template
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse

from ado.menus.models import MenuItem
from ado.menus import registered_models, registered_urls


register = template.Library()


class MainMenuNode(template.Node):
    def render(self, context):
        menuitems = MenuItem.objects.root_nodes()
        return render_to_string([
            'includes/main_menu.html',
            'menus/includes/main_menu.html',
        ], {
            'menuitems': menuitems,
        })


@register.tag('main_menu')
def main_menu(parser, token):
    return MainMenuNode()


@register.simple_tag(takes_context=True)
def menu_app_json(context):

    # Recursive function for building menu items
    def get_menu_item(item):
        children = [get_menu_item(i)
                    for i in item.get_children()]

        url = item.get_absolute_url()
        if not url and len(children):
            url = children[0]['url']

        ctype = item.content_type
        app_label, model = None, None
        if ctype:
            app_label = ctype.app_label
            model = ctype.model

        return {
            'id': item.id,
            'pk': item.id,
            'title': item.item_title,
            'slug': item.slug,
            'url': url,
            'type': item.type,
            'node_label': item.node_label,
            'app_label': app_label,
            'model_name': model,
            'lft': item.lft,
            'rght': item.rght,
            'level': item.level,
            'tree_id': item.tree_id,
            'parent': item.parent and item.parent.slug,
            'children': children,
        }

    # Get item tree
    items = [get_menu_item(item) for item in MenuItem.objects.root_nodes()]

    # Get unused apps
    used_app_urls = [i.url for i in MenuItem.objects.apps()]
    unused_apps = [app for app in registered_urls
                   if app['url'] not in used_app_urls]

    # Get unused models
    unused_objects = []
    for model in registered_models:
        ctype = ContentType.objects.get_for_model(model)
        ids = [i.object.pk for i in MenuItem.objects.filter(
                                                    content_type=ctype)]
        queryset = model.objects.exclude(id__in=ids)
        opts = model._meta
        unused_objects.append({
            'opts': {
                'verbose_name': opts.verbose_name.title(),
                'verbose_name_plural': opts.verbose_name_plural.title(),
                'app_label': opts.app_label,
                'model_name': opts.model_name,
                'object_name': opts.object_name,
            },
            'objects': [
                {
                    'pk': o.pk,
                    'id': o.pk,
                    'title': str(o),
                    'admin_url': reverse('admin:{0}_{1}_change'.format(
                        opts.app_label,
                        opts.model_name), args=[o.pk]),
                } for o in queryset
            ]
        })

    menu_data = {
        'items': items,
        'unused_apps': unused_apps,
        'unused_objects': unused_objects,
    }

    json_string = json.dumps(menu_data, cls=DjangoJSONEncoder)
    return mark_safe(json_string)
