import factory

from django import http
from django.test import TestCase, override_settings
from django.template.defaultfilters import slugify
from django.urls import path

from ado import menus
from ado.menus.models import MenuItem
from ado.menus.tests import models as test_models


class PageFactory(factory.DjangoModelFactory):

    class Meta:
        model = test_models.Page

    title = factory.Sequence(lambda n: 'Test Page {0}'.format(n))
    slug = factory.LazyAttribute(lambda a: slugify(a.title))


class MenuItemObjectTest(TestCase):
    def test_create_for_object(self):
        page = PageFactory.create()
        item = MenuItem.objects.create_for_object(page)
        self.assertTrue(item.slug)
        self.assertTrue(item.is_object)
        self.assertTrue(item in MenuItem.objects.objects())

    def test_create_for_object_publishes_object(self):
        page = PageFactory.create(published=False)
        self.assertFalse(page.published)
        MenuItem.objects.create_for_object(page)
        self.assertTrue(page.published)

    def test_unpublish_object_delete_menuitem(self):
        page = PageFactory.create(published=True)
        item = MenuItem.objects.create_for_object(page)
        page.published = False
        page.save()
        self.assertEqual(MenuItem.objects.filter(pk=item.pk).count(), 0)
        item = page.get_menuitem()
        self.assertEqual(item, None)

    def test_deleting_object_deletes_menuitem(self):
        page = PageFactory.create()
        item = MenuItem.objects.create_for_object(page)
        page.delete()
        self.assertEqual(MenuItem.objects.filter(pk=item.pk).count(), 0)

    def test_change_object_title_changes_menuitem_title(self):
        page = PageFactory.create()
        item = MenuItem.objects.create_for_object(page)
        self.assertEqual(page.title, item.title)
        page.title = 'Some other title'
        page.save()
        item = page.get_menuitem()
        self.assertEqual(page.title, item.title)

    def test_get_menuitem_for_object(self):
        page = PageFactory.create()
        item = MenuItem.objects.create_for_object(page)
        self.assertEqual(item, MenuItem.objects.get_menuitem_for_object(page))
        self.assertEqual(item, page.get_menuitem())

    def test_get_root_menuitem_for_object(self):
        page = PageFactory.create()
        item = MenuItem.objects.create_for_object(page)
        root_item = MenuItem.objects.create(item_title='Root Item')
        item.move_to(root_item)

        self.assertEqual(item.parent, root_item)
        self.assertEqual(root_item,
                         MenuItem.objects.get_root_menuitem_for_object(page))
        self.assertEqual(root_item,
                         page.get_root_menuitem())

    def test_get_parent_for_object(self):
        page = PageFactory.create()
        item = MenuItem.objects.create_for_object(page)
        parent = MenuItem.objects.create(item_title='Root Item')
        item.move_to(parent)

        self.assertEqual(item.parent, parent)
        self.assertEqual(parent,
                         MenuItem.objects.get_parent_for_object(page))
        self.assertEqual(parent,
                         page.get_parent())


@override_settings(ROOT_URLCONF=[
    path('test_url1',
         lambda r: http.HttpResponse('test'),
         name='test_url1'),
])
class MenuItemURLTest(TestCase):
    def setUp(self):
        # reset urls
        menus.registered_urls = []

    def test_register_by_url(self):
        url = '/test_url1'
        title = 'Test 1'
        menus.register_url(title, url)
        self.assertTrue({'title': title, 'url': url} in menus.registered_urls)

    def test_register_by_name(self):
        url = '/test_url1'
        title = 'Test 1'
        menus.register_url(title, name='test_url1')

        matching_urls = [i for i in menus.registered_urls if str(i['url']) == url]
        self.assertEqual(len(matching_urls), 1)

    def test_is_app(self):
        item = MenuItem.objects.create(url='/test_url1', item_title='test')
        self.assertTrue(item.is_app)
        self.assertTrue(item in MenuItem.objects.apps())

    def test_external_link(self):
        url = 'http://google.com/'
        item = MenuItem.objects.create(url=url, item_title='test')
        self.assertTrue(item.is_external_link)

        url = 'ftp://somesite.com/'
        item = MenuItem.objects.create(url=url, item_title='test')
        self.assertTrue(item.is_external_link)

    def test_container_item(self):
        item = MenuItem.objects.create(item_title='Container Item')
        self.assertTrue(item.is_container)
