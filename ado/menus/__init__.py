from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.utils.functional import lazy
from django.db.models import signals
from django.conf import settings


default_app_config = 'ado.menus.apps.MenusConfig'

registered_models = []
registered_urls = []


class AlreadyRegistered(Exception):
    """
    An attempt was made to register a model more than once.
    """
    pass


def register_model(model):
    """
    Registers the model so instances can be used in the menu system.
    """
    from ado.menus.models import MenuItem
    from ado.menus.models import (menuitem_object_post_save,
                                  menuitem_object_pre_save,
                                  menuitem_object_pre_delete)
    if model in registered_models:
        raise AlreadyRegistered(
            'The model {0} has already been registered for menu items.'.format(
                model.__name__,
            ))

    # Set up shortcut methods for object
    setattr(model, 'get_menuitem',
            lambda self: MenuItem.objects.get_menuitem_for_object(self))
    setattr(model, 'get_root_menuitem',
            lambda self: MenuItem.objects.get_root_menuitem_for_object(self))
    setattr(model, 'get_parent',
            lambda self: MenuItem.objects.get_parent_for_object(self))

    # Set up signals
    signals.post_save.connect(menuitem_object_post_save, sender=model)
    signals.pre_save.connect(menuitem_object_pre_save, sender=model)
    signals.pre_delete.connect(menuitem_object_pre_delete, sender=model)

    registered_models.append(model)


def register_url(title, url=None, name=None, **kwargs):
    """
    Registers a URL to be used in the menu navigation system.  You can pass
    either a url directly, as in:

        register_url("Portfolio", "/collections/"),

    Or the name of a view either directly or from the name in the urlconf,
    as in:

        register_url("Blog", name="artcode.apps.blog.views.index")

    Or:

        register_url("Blog", name="blog-index")
    """
    if url is None and name is None:
        raise TypeError(
                "register_url() takes either 'url' or 'name' as arguments.")

    if title in registered_urls:
        raise AlreadyRegistered(
            'The title %s has already been registered for menu items.' % title)

    def reverse_named_url(name):
        try:
            return reverse(name)
        except NoReverseMatch:
            if settings.DEBUG:
                raise
            return ''

    lazy_reverse = lazy(reverse_named_url, str)
    if not url:
        url = lazy_reverse(name, **kwargs)
    registered_urls.append({
        'title': title,
        'url': url,
    })
