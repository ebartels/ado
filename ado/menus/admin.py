import json

from django.contrib import admin
from django import http
from django.apps import apps
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.urls import reverse

from ado.menus.models import MenuItem
from ado.utils.jsonutils import json_encode


csrf_protect_m = method_decorator(csrf_protect)


class MenuItemAdmin(admin.ModelAdmin):
    model = MenuItem
    icon = '<i class="icon material-icons">menu</i>'
    fields = ['item_title', 'url']

    def get_queryset(self, request):
        qs = self.model.objects.get_queryset()
        ordering = self.ordering or ()  # Otherwise we might try to do *None
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def get_form(self, request, obj=None):
        if obj and obj.is_container:
            self.fields = ['item_title']
        else:
            self.fields = ['item_title', 'url']
        return super().get_form(request, obj)

    def get_urls(self):
        from django.urls import path
        urls = super().get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name

        myurls = [
            path('move_item/',
                 self.admin_site.admin_view(self.move_menuitem_view),
                 name='%s_%s_move' % info),
            path('add_item/',
                 self.admin_site.admin_view(self.add_menuitem_view),
                 name='%s_%s_add_item' % info),
            path('delete_item/',
                 self.admin_site.admin_view(self.delete_menuitem_view),
                 name='%s_%s_delete_item' % info),
            path('add_object/',
                 self.admin_site.admin_view(self.add_object_view),
                 name='%s_%s_add_object' % info),
            path('<int:id>/edit_object/',
                 self.admin_site.admin_view(self.edit_object),
                 name="admin_menus_menuitem_edit_object"),
        ]
        return myurls + urls

    @csrf_protect_m
    def changelist_view(self, request, extra_context=None):
        has_change_permission = self.has_change_permission(request, None)
        has_view_permission = self.has_view_permission(request, None)

        if not has_view_permission:
            return super().changelist_view(self, request, extra_context=extra_context)

        # Add a new menu category
        if has_change_permission and request.POST:
            if 'container_title' in request.POST:
                title = request.POST['container_title']
                menuitem = MenuItem(item_title=title)
                MenuItem.objects.insert_node(menuitem,
                                             None,
                                             position='last-child',
                                             save=True)
                self.message_user(request,
                                  "Your menu item has been added.")
                return http.HttpResponseRedirect('./')

            if 'url_title' in request.POST and 'url_url' in request.POST:
                title = request.POST['url_title']
                url = request.POST['url_url']
                menuitem = MenuItem(item_title=title, url=url)
                MenuItem.objects.insert_node(menuitem,
                                             None,
                                             position='last-child',
                                             save=True)
                self.message_user(request, "Your menu item has been added.")
                return http.HttpResponseRedirect('./')

        return render(request, 'admin/menus/change_list.html', {
            'title': 'Configure Menu',
            'has_change_permission': has_change_permission,
        })

    @method_decorator(permission_required('menus.change_menuitem'))
    def move_menuitem_view(self, request):
        """
        Ajax view to reorder the placement of a MenuItem in the tree structure.
        """

        request_data = json.loads(request.body.decode('UTF-8'))
        if request_data:
            item_id = request_data['item']
            item = get_object_or_404(MenuItem, pk=item_id)
            parent, rght, lft = None, None, None
            if 'parent' in request_data:
                try:
                    parent_id = int(request_data['parent'])
                    parent = get_object_or_404(MenuItem, pk=parent_id)
                except ValueError:
                    pass
            if 'rght' in request_data:
                try:
                    rght_id = int(request_data['rght'])
                    if rght_id != item_id:
                        rght = get_object_or_404(MenuItem, pk=rght_id)
                except ValueError:
                    pass
            if 'lft' in request_data:
                try:
                    lft_id = int(request_data['lft'])
                    if lft_id != item_id:
                        lft = get_object_or_404(MenuItem, pk=lft_id)
                except ValueError:
                    pass

            # Move node based on parent
            if rght:
                MenuItem.objects.move_node(item, rght, 'left')
            elif lft:
                MenuItem.objects.move_node(item, lft, 'right')
            else:
                MenuItem.objects.move_node(item, parent)

            return http.HttpResponse('ok')

        return http.HttpResponseBadRequest('no data provided')

    def _json_encode_menuitem(self, item):
        ctype = item.content_type
        app_label, model_name = None, None
        if ctype:
            app_label = ctype.app_label
            model_name = ctype.model

        data = {
            'pk': item.id,
            'id': item.id,
            'title': item.item_title,
            'slug': item.slug,
            'url': item.get_absolute_url(),
            'type': item.type,
            'node_label': item.node_label,
            'app_label': app_label,
            'model_name': model_name,
            'object_id': item.object_id,
            'lft': item.lft,
            'rght': item.rght,
            'level': item.level,
            'tree_id': item.tree_id,
            'parent': item.parent and item.parent.slug,
        }
        return json_encode(data)

    @method_decorator(permission_required('menus.add_menuitem'))
    def add_menuitem_view(self, request):
        if request.method == 'POST':
            request_data = json.loads(request.body.decode('UTF-8'))
            if 'title' in request_data and 'url' in request_data:
                title = request_data['title']
                url = request_data['url']
                menuitem = MenuItem.objects.create(item_title=title, url=url)
                return http.HttpResponse(self._json_encode_menuitem(menuitem))
            else:
                return http.HttpResponseBadRequest('wrong data')
        else:
            return http.HttpResponseBadRequest('no data provided')

    @method_decorator(permission_required('menus.delete_menuitem'))
    def delete_menuitem_view(self, request):
        if request.method == 'POST':
            request_data = json.loads(request.body.decode('UTF-8'))
            if 'pk' in request_data:
                pk = request_data['pk']
                menuitem = get_object_or_404(MenuItem, pk=pk)
                menuitem.delete()
                return http.HttpResponse(self._json_encode_menuitem(menuitem))
            else:
                return http.HttpResponseBadRequest('wrong data')
        else:
            return http.HttpResponseBadRequest('no data provided')

    @method_decorator(permission_required('menus.add_menuitem'))
    def add_object_view(self, request):
        if request.method == 'POST':
            request_data = json.loads(request.body.decode('UTF-8'))
            if 'model' in request_data and 'pk' in request_data:
                app_name, model_name = request_data['model'].split('.')
                pk = request_data['pk']
                model = apps.get_model(app_name, model_name)
                instance = get_object_or_404(model, pk=pk)
                menuitem = MenuItem.objects.create_for_object(instance)
                return http.HttpResponse(self._json_encode_menuitem(menuitem))
            else:
                return http.HttpResponseBadRequest('wrong data')
        else:
            return http.HttpResponseBadRequest('no data provided')

    def edit_object(self, request, id):
        menuitem = get_object_or_404(MenuItem, pk=id)
        if menuitem.object:
            url = reverse('admin:%s_%s_change' %
                          (menuitem.object._meta.app_label,
                           menuitem.object._meta.model_name),
                          args=(menuitem.object.pk,))
        else:
            url = reverse('admin:menus_menuitem_change', args=(menuitem.pk,))
        return http.HttpResponseRedirect(url)


admin.site.register(MenuItem, MenuItemAdmin)
