from ado.menus.models import MenuItem


def menuitems(request):
    return {
        'main_menuitems': MenuItem.objects.all(),
        'root_menuitems': MenuItem.objects.root_nodes(),
    }


def root_menuitems(request):
    menuitems = MenuItem.objects.root_nodes()
    return {
        'main_menuitems': menuitems,
    }


def current_menuitem(request):
    path = request.path
    try:
        menuitem = MenuItem.objects.select_related('parent').filter(url=path)[0]
    except IndexError:
        menuitem = None

    if not menuitem:
        items = [(m, m.get_absolute_url()) for m in MenuItem.objects.all()]
        urls = [i[1] for i in items]
        if path in urls:
            index = urls.index(path)
            menuitem = items[index][0]
    return {
        'current_menuitem': menuitem,
    }
