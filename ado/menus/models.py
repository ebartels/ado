import re
from mptt.models import MPTTModel
from mptt.managers import TreeManager

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse, NoReverseMatch

try:
    from django.contrib.contenttypes.fields import GenericForeignKey
except ImportError:  # django < 1.7
    from django.contrib.contenttypes.generic import GenericForeignKey

from autoslug import AutoSlugField


MENUITEM_TYPE_CHOICES = (
    ('object', 'object'),
    ('app', 'app'),
    ('container', 'container'),
)


class MenuItemManager(TreeManager):
    def create_for_object(self, instance):
        """
        Given an object instance, creates a MenuItem object for
        the instance.
        """
        if hasattr(instance, 'title'):
            title = instance.title
        elif hasattr(instance, 'name'):
            title = instance.name
        ctype = ContentType.objects.get_for_model(type(instance))
        menu_item = MenuItem(content_type=ctype, object_id=instance.pk,
                             item_title=title)
        self.insert_node(menu_item, None, position='last-child', save=True)
        if hasattr(instance, 'published') and not instance.published:
            instance.published = True
            instance.save()
        return menu_item

    def apps(self):
        return self.get_queryset().filter(type='app')

    def containers(self):
        return self.get_queryset().filter(type='container')

    def objects(self):
        return self.get_queryset().filter(type='object')

    def get_menuitem_for_object(self, object):
        """
        Returns the menuitem for an object
        """
        try:
            ctype = ContentType.objects.get_for_model(object)
            menuitem = self.filter(content_type=ctype, object_id=object.pk)[0]
            return menuitem
        except Exception:
            return None

    def get_root_menuitem_for_object(self, object):
        """
        Returns a root top-level menuitem for an object
        """
        try:
            ctype = ContentType.objects.get_for_model(object)
            menuitem = self.filter(content_type=ctype, object_id=object.pk)[0]
            return menuitem.get_root()
        except Exception:
            return None

    def get_parent_for_object(self, object):
        """
        Returns the parent container MenuItem for an object.
        """
        try:
            ctype = ContentType.objects.get_for_model(object)
            menuitem = self.filter(content_type=ctype, object_id=object.pk)[0]
            return menuitem.parent
        except Exception:
            return None

    def get_parent_for_url(self, url):
        """
        Takes a URL and returns the closest parent MenuItem instance that will
        either be the URL itself or be a parent directory of the URL.
        For example, if you have a MenuItem with url='/collections/' then
        MenuItem.objects.get_parent_for_url('/collections/about/view') would
        return this item since it is a parent URL.
        """
        url_parts = [u for u in url.split('/') if u]
        search_urls = []
        while len(url_parts):
            search_urls.append("/%s/" % "/".join(url_parts))
            url_parts.pop()
        try:
            return MenuItem.objects.filter(url__in=search_urls)[0]
        except IndexError:
            pass
        return None


class MenuItem(MPTTModel):
    item_title = models.CharField('Title', max_length=512)
    slug = AutoSlugField(
        populate_from='item_title',
        max_length=512,
        unique=True,
        editable=False,
    )
    url = models.CharField(max_length=512, blank=True)
    parent = models.ForeignKey('self',
                               null=True,
                               blank=True,
                               on_delete=models.CASCADE,
                               related_name='children')
    type = models.CharField(max_length=20, db_index=True, editable=False,
                            choices=MENUITEM_TYPE_CHOICES)
    content_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.CASCADE)
    object = GenericForeignKey('content_type', 'object_id')
    object_id = models.PositiveIntegerField(null=True, blank=True)

    objects = MenuItemManager()

    class Meta:
        verbose_name = 'Menu Item'

    def get_absolute_url(self):
        if self.url:
            return self.url
        elif self.object and self.object.get_absolute_url:
            return self.object.get_absolute_url()
        try:
            return reverse('menus-menuitem-view', kwargs={'slug': self.slug})
        except NoReverseMatch:
            return ''

    @property
    def is_container(self):
        return self.type == 'container'

    @property
    def is_page(self):
        return self.type == 'object'

    @property
    def is_object(self):
        return self.type == 'object'

    @property
    def is_app(self):
        return self.type == 'app'

    @property
    def is_external_link(self):
        if self.is_app:
            return bool(re.match(r'^\w+://', self.url))
        return False

    @property
    def node_label(self):
        if self.is_external_link:
            return 'External Link'
        if self.type == 'object':
            return str(self.content_type).title()
        if self.type == 'App':
            return 'URL'
        return self.type.title()

    @property
    def title(self):
        if self.object_id:
            if hasattr(self.object, 'title'):
                return self.object.title
            elif hasattr(self.object, 'name'):
                return self.object.name
        return self.item_title

    def __str__(self):
        return '{0}'.format(self.title)

    def save(self, *args, **kwargs):
        if not self.url and not self.object_id:
            self.type = 'container'
        elif self.url and not self.object_id:
            self.type = 'app'
        else:
            self.type = 'object'
        return super().save(*args, **kwargs)


# Post-save signal for MenuItem related objects
def menuitem_object_post_save(sender, instance, created, raw, *args, **kwargs):
    # Create new MenuItem instance object
    published = getattr(instance, 'published', True)
    if not published:
        ctype = ContentType.objects.get_for_model(sender)
        try:
            menuitem = MenuItem.objects.get(content_type=ctype,
                                            object_id=instance.pk)
            menuitem.delete()
        except MenuItem.DoesNotExist:
            pass


def menuitem_object_pre_save(sender, instance, *args, **kwargs):
    # sync MenuItem._item_title with instance.title
    title = None
    if hasattr(instance, 'title'):
        title = instance.title
    elif hasattr(instance, 'name'):
        title = instance.name

    if title:
        ctype = ContentType.objects.get_for_model(sender)
        try:
            menuitem = MenuItem.objects.get(content_type=ctype,
                                            object_id=instance.pk)
            menuitem.item_title = title
            menuitem.save()
        except MenuItem.DoesNotExist:
            pass


def menuitem_object_pre_delete(sender, instance, *args, **kwargs):
    # Make sure to delete menuitems for objects before they are deleted.
    ctype = ContentType.objects.get_for_model(sender)
    MenuItem.objects.filter(content_type=ctype, object_id=instance.pk).delete()
