from django import forms
from django.contrib.auth.forms import AuthenticationForm


class EmailAuthForm(AuthenticationForm):
    """
    Auth form for using email (or username).
    """
    username = forms.CharField(label="Email", max_length=75)
