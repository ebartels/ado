from django.db import models
from django.db.models import signals as model_signals
from django.dispatch.dispatcher import Signal

PROPAGATE_SIGNALS = [
    getattr(model_signals, s) for s in dir(model_signals)
    if isinstance(getattr(model_signals, s), Signal)
]


_signal_receivers = []


class SignalForwarder(object):
    """
    A signal receiver who forwards its signal to another sender.
    """
    __name__ = 'SignalForwarder'  # keeps debug_toolbar happy

    def __init__(self, signal, forward_to):
        self.signal = signal
        self.forward_to = forward_to
        _signal_receivers.append(self)

    def __call__(self, sender, *args, **kwargs):
        kwargs.pop('signal')
        self.signal.send(sender=self.forward_to, *args, **kwargs)


def _get_parents(model):
    parents = [p for p in model.__bases__
               if (issubclass(p, models.Model)
                   and p is not models.Model
                   and not p._meta.abstract)]
    for p in parents:
        if issubclass(p, models.Model) and p is not models.Model:
            parents += _get_parents(p)
    return parents


def propagate_signals_to_parents(model, signals=PROPAGATE_SIGNALS):
    """
    Due to Django bug #9318, signal handlers set up for models are
    not dispatched for child models.  This also effects proxy models.
    Here, we propagate model signals up the chain to any signal handlers
    listening for parent classes.
    see: http://code.djangoproject.com/ticket/9318
    """
    for signal in signals:
        parents = set(_get_parents(model))
        for parent in parents:
            receiver = SignalForwarder(signal, parent)
            signal.connect(receiver, sender=model)
