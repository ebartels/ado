"""
Some useful decorators to make mock.patch apply condtionally.
"""
from unittest.mock import patch


def patchif(condtion, *args, **kwargs):
    """A decorator to apply mock.patch if condition is True"""
    def dec(obj):
        if condtion:
            return patch(*args, **kwargs)(obj)
        return obj
    return dec


def _patchif_object(condtion, *args, **kwargs):
    """A decorataor to apply mock.path.object if condition is True"""
    def dec(obj):
        if condtion:
            return patch.object(*args, **kwargs)(obj)
        return obj
    return dec


patchif.object = _patchif_object
