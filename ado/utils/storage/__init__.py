from django.apps import apps
from django.db import models

import logging
log = logging.getLogger(__name__)


def storage_copy(from_storage, to_storage=None, force=False, dry_run=False):
    """
    Copies files from one storage to another.  Pass in the front/to storage
    instances and all FileField instances on all models will have their files
    copied to 'to-storage'.
    """
    file_fields = set()
    for model in apps.get_models():
        for field in model._meta.fields:
            if isinstance(field, (models.FileField, models.ImageField)):
                file_fields.add((model, field))

    log.debug('Copying files{0}'.format(' - dry run' if dry_run else ''))
    log.debug('=======================')
    for model, field in file_fields:
        for instance in model.objects.all():
            f = getattr(instance, field.name)
            if not f:
                continue

            # Destination defaults to the fields default storage.
            if not to_storage:
                dest_storage = f.storage
            else:
                dest_storage = to_storage

            if from_storage and from_storage.exists(f.name):
                if force or not dest_storage.exists(f.name):
                    log.debug("copying: {0} to {1}".format(
                        f.name,
                        str(dest_storage)))
                    if not dry_run:
                        dest_storage.save(f.name, from_storage.open(f.name))
                else:
                    log.debug("skipping: {0}, already exists".format(f.name))
            else:
                log.debug("missing: {0}, missing from source storage".format(
                    f.name))
