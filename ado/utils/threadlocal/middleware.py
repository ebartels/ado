from . import set_thread_variable


def threadlocal_middlware(get_response):

    def middleware(request):
        set_thread_variable('request', request)
        return get_response(request)

    return middleware
