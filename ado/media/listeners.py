"""
Signals listeners for ado.media
"""
from PIL import Image as PILImage, ExifTags
import xml.etree.ElementTree as ET

from django.db.models import signals
from django.dispatch import receiver
from django.conf import settings
from django_cleanup.signals import cleanup_pre_delete

from ado.media.models import Image, Video
from ado.media import resizer


DEFAULT_IMAGE_SIZES = (
    ('thumb', '200x200'),
    ('largethumb', '360x300'),
)

IMAGE_SIZES = getattr(settings, 'SCALED_IMAGE_SIZES', DEFAULT_IMAGE_SIZES)

EXIF_ORIENTATION = next(t[0] for t in ExifTags.TAGS.items() if t[1] == 'Orientation')


@receiver(signals.pre_save, sender=Image)
def add_image_dimensions(instance, **kwargs):
    """
    A custom version of width/height calculation for images.

    * Handle svg images (Django does not)
    * Take into account orientation in EXIF metadata
    """
    if not kwargs.get('raw') and instance.filename:
        if not instance.width or not instance.height:
            # Special handling for svg files
            if instance.filename.name.endswith('.svg'):
                try:
                    instance.filename.open()
                    file_pos = instance.filename.tell()
                    instance.filename.seek(0)

                    # svg_contents = instance.filename.read()
                    tree = ET.parse(instance.filename)
                    root = tree.getroot()
                    width = int(float(root.get('width') or 0))
                    height = int(float(root.get('height') or 0))
                    instance.filename.seek(file_pos)
                except (ValueError, ET.ParseError):
                    width = 0
                    height = 0

                instance.width = width
                instance.height = height

            else:
                # Get image dimensions from the image field.
                # This can fail with lzw compressed tiffs.
                try:
                    width = instance.filename.width
                    height = instance.filename.height
                except OSError:
                    width = None
                    height = None

                instance.filename.open()
                file_pos = instance.filename.tell()
                instance.filename.seek(0)
                try:
                    image = PILImage.open(instance.filename)

                    # Get width/height if not there already
                    if not width or not height:
                        width, height = image.size

                    # Check exif data to see if orientation needs to be corrected
                    # see: http://sylvana.net/jpegcrop/exif_orientation.html
                    exif = image._getexif()
                    if exif:
                        orientation = exif.get(EXIF_ORIENTATION)
                        if orientation and orientation in (5, 6, 7, 8):
                            [width, height] = [height, width]
                except Exception:
                    pass

                instance.filename.seek(file_pos)
                instance.width = width
                instance.height = height


@receiver(signals.post_save, sender=Image)
def generate_image_thumbnails(instance, created, **kwargs):
    """
    Pre-generate resized images on save
    """
    if not kwargs.get('raw'):
        for key, size in IMAGE_SIZES:
            resizer.get_resized(instance.filename, size)


@receiver(signals.post_save, sender=Video)
def generate_video_thumbnails(instance, **kwargs):
    """
    Pre-generate resized images for Video.video_image on save
    """
    if not kwargs.get('raw'):
        if instance.video_image:
            for key, size in IMAGE_SIZES:
                resizer.get_resized(instance.video_image, size)


@receiver(cleanup_pre_delete)
def cleanup_thumbnails(**kwargs):
    """
    Clean up thumbnail files when ImageField model is deleted or changed.
    """
    file_ = kwargs.get('file')
    resizer.delete_resized(file_)
