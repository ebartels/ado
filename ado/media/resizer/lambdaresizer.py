import re
import os
from urllib.parse import urlparse
from django.conf import settings
from django.core.checks import register, Error, Warning

from .base import ResizedImage, BaseImageResizer
from .dummyresizer import DummyResizedImage
from .config import IMAGE_RESIZER_USE_DUMMY, DEFAULT_IMAGE_RESIZER


# Map source file extension to resized file extension
# Not all output formats are supported so we decide which file resized image
# extension to use based on the input file extension.
# TODO: Thik about using .webp once all browsers support it (I'm looking at your Safari!)
VALID_EXTENSIONS = {
    '': '.jpg',
    '.': '.jpg',
    '.jpg': '.jpg',
    '.jpeg': '.jpg',
    '.tif': '.jpg',
    '.tiff': '.jpg',
    '.webp': '.jpg',
    '.png': '.png',
    '.gif': '.png',
}


@register()
def check_for_settings(app_configs, **kwargs):
    errors = []
    if app_configs is None or 'ado.media' in [c.name for c in app_configs]:
        if DEFAULT_IMAGE_RESIZER.rsplit('.', 1).pop() == 'LambdaImageResizer':
            if not hasattr(settings, 'IMAGE_RESIZER_LAMBDA_URL'):
                errors.append(Error(
                    '"IMAGE_RESIZER_LAMBDA_URL" setting is required to serve resized images.',
                    obj='LambdaImageResizer',
                    hint=('Add to settings.py, e.g. "IMAGE_RESIZER_LAMBDA_URL '
                          '= https://d1234.cloudfront.net/resized/..."'),
                ))

            storage = getattr(settings, 'DEFAULT_FILE_STORAGE', '')
            if 's3boto' not in storage:
                errors.append(Warning(
                    f'You are not using s3boto or s3boto3 for default file storage.',
                    obj='LambdaImageResizer',
                    hint='set DEFAULT_FILE_STORAGE to s3boto or s3boto3 from django-storages',
                ))
                pass
    return errors


class LambdaImageResizer(BaseImageResizer):
    def _get_resized(self, source_file, geometry, **kwargs):
        IMAGE_RESIZER_LAMBDA_URL = getattr(settings, 'IMAGE_RESIZER_LAMBDA_URL')

        # parse width and height
        width, height = [int(n) for n in geometry.split('x')]

        # Backup to dummy images if the file source is not found
        # Avoid using in production for remote storage, like s3boto
        if IMAGE_RESIZER_USE_DUMMY:
            if not source_file.storage.exists(source_file.name):
                return DummyResizedImage(width, height)

        crop = 'cover' if kwargs.get('crop', False) else 'inside'
        basename, sourceExt = os.path.splitext(os.path.basename(source_file.name))

        # Choose the resized image file extension, defaulting to .jpg
        ext = VALID_EXTENSIONS.get(sourceExt.lower(), None)
        if not ext:
            return source_file.storage.url(source_file.name)

        # Create the url
        url = urlparse(IMAGE_RESIZER_LAMBDA_URL)
        path = re.sub(r'/+', '/', '/'.join([
            url.path,
            f'/{source_file.name}/{width}x{height}.{crop}.{basename}{ext}',
        ]))
        url = url._replace(path=path).geturl()

        return ResizedImage(
            url,
            width,
            height,
        )

    def _delete_resized(self, source_file):
        # Instead, use S3 lifecycle rule to expire images
        pass
