from importlib import import_module
from django.utils.functional import LazyObject

from .config import DEFAULT_IMAGE_RESIZER
from .base import ResizedImage  # noqa
from .dummyresizer import DummyImageResizer # noqa
from .sorlresizer import SorlImageResizer # noqa
from .lambdaresizer import LambdaImageResizer # noqa


def get_resizer_class(mod_str=DEFAULT_IMAGE_RESIZER):
    mod_name, cls_name = mod_str.rsplit('.', 1)
    mod = import_module(mod_name)
    return getattr(mod, cls_name)


# Using LazyObject here prevents the server process from crashing in the event
# of misconfigured settings.DEFAULT_IMAGE_RESIZER.
class Resizer(LazyObject):
    def _setup(self):
        self._wrapped = get_resizer_class(DEFAULT_IMAGE_RESIZER)()


resizer = Resizer()


def get_resized(source_file, geometry, **kwargs):
    """
    Returns a resized version of the given image file.
    """
    return resizer.get_resized(source_file, geometry, **kwargs)


def delete_resized(source_file):
    """
    Cleans up any resized images associated with the given file.
    """
    return resizer.delete_resized(source_file)
