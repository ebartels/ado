from .base import ResizedImage, BaseImageResizer


class DummyResizedImage(ResizedImage):
    def __init__(self, width, height):
        url = f'/media/placeholder/{width}/{height}/'
        super().__init__(url, width, height)


class DummyImageResizer(BaseImageResizer):

    def _get_resized(self, source_file, geometry, **kwargs):
        width, height = [int(n) for n in geometry.split('x')]
        return DummyResizedImage(width, height)

    def _delete_resized(self, source_file):
        pass
