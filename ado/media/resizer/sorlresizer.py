from django.core.checks import Error, register
from django.conf import settings

from .base import ResizedImage, BaseImageResizer
from .config import DEFAULT_IMAGE_RESIZER


ERR_MSG = """You have 'SorlImageResizer' configured for image resizing. This
requires the 'sorl.thumbnail' module be installed. Please add 'sorl.thumbnail'
to your INSTALLED_APPS setting.""".replace('\n', ' ')


@register()
def check_for_sorl_thumbnail(app_configs, **kwargs):
    errors = []
    if app_configs is None or 'ado.media' in [c.name for c in app_configs]:
        if DEFAULT_IMAGE_RESIZER.rsplit('.', 1).pop() == 'SorlImageResizer':
            if 'sorl.thumbnail' not in settings.INSTALLED_APPS:
                errors.append(Error(ERR_MSG))
    return errors


class SorlResizedImage(ResizedImage):
    def __init__(self, thumb):
        super().__init__(thumb.url, thumb.width, thumb.height)


class SorlImageResizer(BaseImageResizer):
    """
    A resizer that uses sorl.thumbnail
    """
    def __init__(self, *args, **kwargs):
        from sorl import thumbnail
        self.thumbnail = thumbnail
        super().__init__(*args, **kwargs)

    def _get_resized(self, source_file, geometry, **kwargs):
        crop = kwargs.get('crop', None)
        if crop and not isinstance(crop, str):
            crop = 'center'
        thumb = self.thumbnail.get_thumbnail(source_file, geometry, crop=crop)
        return SorlResizedImage(thumb)

    def _delete_resized(self, source_file):
        self.thumbnail.delete(source_file, delete_file=False)
