import re
from django.core.files import File


class ResizedImage:
    """
    Object returned for resized version of an image
    """
    def __init__(self, url, width, height):
        self.url = url
        self.width = width
        self.height = height

    def __str__(self):
        return '{} @ {}x{}'.format(
            self.url,
            self.width,
            self.height,
        )


RE_GEOMETRY = re.compile(r'\d+x\d+$|x\d+$|\d+$')


class BaseImageResizer:
    """
    Base class for image resizers.

    It should return a ResizedImage instance
    """
    @classmethod
    def _check_file_arg(cls, file_arg):
        # Check correct type of source_file
        if not isinstance(file_arg, File):
            raise ValueError(
                f'"source_file" must be of type {File}, but got {type(file_arg)}'
            )

    @classmethod
    def _check_geometry_arg(cls, geometry):
        if not RE_GEOMETRY.match(geometry):
            raise SyntaxError(
                f'Geometry  does not have the correct syntax: "{geometry}"'
            )

    def get_resized(self, source_file, geometry, **kwargs):
        """
        Returns a resized version of the given image file.
        """
        self._check_file_arg(source_file)
        self._check_geometry_arg(geometry)

        # SVG files don't need resizing
        if source_file.name.lower().endswith('.svg'):
            width, height = [int(n) for n in geometry.strip().split('x')]
            return ResizedImage(
                source_file.storage.url(source_file.name),
                width=width,
                height=height,
            )

        return self._get_resized(source_file, geometry, **kwargs)

    def delete_resized(self, source_file):
        self._check_file_arg(source_file)
        return self._delete_resized(source_file)

    def _get_resized(self, source_file, geometry, **kwargs):
        raise NotImplementedError(f'{type(self).__name__} needs to implement "_get_resized"')

    def _delete_resized(self, source_file):
        """
        Deletes any resized image files from storage.
        """
        raise NotImplementedError(f'{type(self).__name__} needs to implement "_delete_resized"')
