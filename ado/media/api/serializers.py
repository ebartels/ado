from rest_framework import serializers
from django.conf import settings

from ado.media.models import Image, Video, File
from ado.media import resizer


IMAGE_SIZES = settings.SCALED_IMAGE_SIZES


class MediaItemSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()

    def get_type(self, obj):
        return obj.subtype._meta.model_name


class ImageSerializer(MediaItemSerializer):
    url = serializers.SerializerMethodField()
    filename = serializers.SerializerMethodField()
    resized = serializers.SerializerMethodField()
    ratio = serializers.SerializerMethodField()
    is_portrait = serializers.SerializerMethodField()

    class Meta:
        model = Image
        fields = ('id',
                  'type',
                  'uid',
                  'url',
                  'filename',
                  'title',
                  'caption',
                  'alt_text',
                  'width',
                  'height',
                  'ratio',
                  'is_portrait',
                  'mimetype',
                  'resized',
                  )

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super().__init__(self, *args, **kwargs)

    def get_url(self, obj):
        return obj.filename.url

    def get_filename(self, obj):
        return obj.filename.name

    def get_ratio(self, obj):
        return float(obj.width) / float(obj.height)

    def get_is_portrait(self, obj):
        return self.get_ratio(obj) < 1

    def get_resized(self, obj):
        images = {}
        # for key, size in (('medium', '420x9000'),):
        for key, size in self.sizes:
            thumb = resizer.get_resized(obj.filename, size)
            images[key] = {
                'url': thumb.url,
                'width': thumb.width,
                'height': thumb.height,
            }
        return images


class VideoSerializer(MediaItemSerializer):
    info = serializers.SerializerMethodField()
    resized = serializers.SerializerMethodField()
    ratio = serializers.SerializerMethodField()
    is_portrait = serializers.SerializerMethodField()

    class Meta:
        model = Video
        fields = ('id',
                  'type',
                  'uid',
                  'url',
                  'title',
                  'caption',
                  'video_image',
                  'info',
                  'ratio',
                  'is_portrait',
                  'resized',
                 )

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super().__init__(self, *args, **kwargs)

    def get_info(self, obj):
        return obj.info

    def get_ratio(self, obj):
        resized = self.get_resized(obj)
        item = list(resized.values())[0]
        return float(item['width']) / float(item['height'])

    def get_is_portrait(self, obj):
        return self.get_ratio(obj) < 1

    def get_resized(self, obj):
        images = {}
        # for key, size in (('medium', '420x9000'),):
        for key, size in self.sizes:
            thumb = resizer.get_resized(obj.video_image, size)
            images[key] = {
                'url': thumb.url,
                'width': thumb.width,
                'height': thumb.height,
            }
        return images


class FileSerializer(MediaItemSerializer):
    url = serializers.SerializerMethodField()
    filename = serializers.SerializerMethodField()
    filetype = serializers.SerializerMethodField()
    filesize = serializers.SerializerMethodField()

    class Meta:
        model = File
        fields = ('id',
                  'type',
                  'uid',
                  'title',
                  'caption',
                  'filename',
                  'url',
                  'mimetype',
                  'filetype',
                  'filesize',
                  )

    def get_url(self, obj):
        return obj.filename.url

    def get_filename(self, obj):
        return obj.filename.name

    def get_filetype(self, obj):
        return obj.mimetype.split('/')[-1].upper()

    def get_filesize(self, obj):
        return obj.filename.size


class MediaSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super().__init__(*args, **kwargs)

    def to_representation(self, instance):
        subtype = instance.subtype
        if isinstance(subtype, Image):
            return (ImageSerializer(sizes=self.sizes, context=self.context)
                    .to_representation(instance))
        elif isinstance(subtype, Video):
            return (VideoSerializer(sizes=self.sizes, context=self.context)
                    .to_representation(instance))
        elif isinstance(subtype, File):
            return (FileSerializer(context=self.context)
                    .to_representation(instance))
