import json
import requests
from urllib.parse import urlparse, urlencode
from hashlib import md5

from django.core.cache import cache


OEMBED_URLS = {
    'vimeo.com': ('http://vimeo.com/api/oembed.json', {
            'portrait': '0',
            'byline': '0',
            'title': '0', }),
    'youtube.com': ('http://www.youtube.com/oembed', {}),
}


class OembedError(Exception):
    pass


def fetch(url, params=None):
    """
    Fetch video information using oembed.
    url -- the url for the video
    params -- extra querystring data params passed to oembed
    """
    # handle params
    if params is None:
        params = {}

    params.update({
        'url': url,
    })

    # Check what video source we've got and build oembed url
    URL = urlparse(url)
    oembed_url = None
    oembed_params = {}
    for source, (api_url, api_params) in list(OEMBED_URLS.items()):
        if URL.netloc.endswith(source):
            oembed_url = api_url
            oembed_params.update(api_params)
            break
    if not oembed_url:
        raise OembedError

    oembed_params.update(params)

    # check for cached response
    url_hash = md5('{0}?{1}'.format(
        url, urlencode(oembed_params)
    ).encode('utf-8')).hexdigest()
    cache_key = 'video_oembed_{0}'.format(url_hash)
    data = cache.get(cache_key)
    if data:
        return data

    data = send_request(oembed_url, oembed_params)
    cache.set(cache_key, data, 30)
    return data


def send_request(url, params=None):
    # make the oembed request
    headers = {'User-Agent': 'Django'}
    try:
        response = requests.get(url, params=params, headers=headers)
        response.raise_for_status()
    except requests.RequestException:
        raise OembedError
    data = json.loads(response.text)
    return data
