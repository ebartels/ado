import re
import os
import tempfile
from urllib.parse import urlparse

import requests

from django import forms
from django.core.files.base import File as DJFile
from django.contrib.admin import widgets as admin_widgets
from django.db import transaction
from django.conf import settings

from ckeditor.widgets import CKEditorWidget
from ado.customadmin.forms import SmartTagSelect
from ado.media import resizer

from ..models import (
    Image,
    File,
    Video,
    title_from_filename,
)
from .. import oembed


class MediaItemAdminForm(forms.ModelForm):
    """
    Base admin form for media items
    """
    class Meta:
        fields = "__all__"
        widgets = {
            'tags': SmartTagSelect,
        }


class FileAdminForm(MediaItemAdminForm):
    """
    Custom form that allows grabbing a remote file and saving it
    """
    file_url = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'vTextField'}))

    class Meta(MediaItemAdminForm.Meta):
        model = File

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not self.instance.pk:
            self.fields['filename'].required = False
            self.fields['title'].required = False

        if 'filename' in self.fields:
            self.fields['filename'].label = 'Upload a file'
        if 'file_url' in self.fields:
            self.fields['file_url'].label = 'or Enter a URL'

    def clean_file_url(self):
        url = self.cleaned_data['file_url']
        if url:
            if not re.match(r'^\w+://', url):
                url = 'http://{0}'.format(url)
            try:
                headers = {'User-Agent': 'Django'}
                response = requests.get(url, headers=headers)
                response.raise_for_status()
                o = urlparse(url)
                name, ext = os.path.splitext(os.path.basename(o.path))
                file_ = tempfile.NamedTemporaryFile(suffix='{0}{1}'.format(name, ext))
                file_.write(response.content)
                self.downloaded_file = file_
            except (requests.RequestException):
                raise forms.ValidationError("The url you entered could not be "
                                            "reached: {0}".format(url))
        return url

    def clean(self):
        cleaned_data = self.cleaned_data
        if (cleaned_data.get('file_url') and
                cleaned_data.get('filename')):
            raise forms.ValidationError("Please enter either an upload or "
                                        "a url, not both")
        if (not cleaned_data.get('file_url') and
                not cleaned_data.get('filename')):
            raise forms.ValidationError("Please upload a file or enter a URL")

        if hasattr(self, 'downloaded_file'):
            file_ = self.downloaded_file
            file_.seek(0)
            field = self.fields['filename']
            cleaned_data['filename'] = field.clean(DJFile(file_))

        return cleaned_data


class CustomImageField(forms.ImageField):
    widget = admin_widgets.AdminFileWidget

    def to_python(self, data):
        # Allow svg files without Django's image check
        if data and getattr(data, 'name', '').endswith('.svg'):
            f = super(forms.ImageField, self).to_python(data)
            return f
        else:
            return super().to_python(data)


class ImageAdminForm(FileAdminForm):
    filename = CustomImageField()

    class Meta(FileAdminForm.Meta):
        model = Image

    def clean(self, *args, **kwargs):
        # check if we have a new file
        if self.instance.pk and 'filename' in self.files:
            # reset width/height
            self.instance.width = None
            self.instance.height = None

            # delete thumbnails
            resizer.delete_resized(self.instance.filename)
        return super().clean(*args, **kwargs)


class VideoAdminForm(MediaItemAdminForm):
    video_image = CustomImageField(required=False)

    class Meta(MediaItemAdminForm.Meta):
        model = Video

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not self.instance.pk:
            self.fields['title'].required = False

    def clean_url(self):
        url = self.cleaned_data['url']
        try:
            data = oembed.fetch(url)
            for key in ('title', 'thumbnail_url', 'html'):
                if key not in list(data.keys()):
                    raise forms.ValidationError((
                        'Could not retrieve data on this video. '
                        'It is possible that the video is not public, or '
                        'needs permissions to be set with to allow embedding.'))
        except oembed.OembedError:
            raise forms.ValidationError(
                    'This URL was not recognized. '
                    'Please enter the full URL for the video.')

        return url

    def clean(self, *args, **kwargs):
        # delete thumbnails if we got a new file
        if self.instance.pk and 'video_image' in self.files:
            if self.instance.video_image:
                resizer.delete_resized(self.instance.video_image)
        return super().clean(*args, **kwargs)


# Upload forms
class MediaUploadForm(forms.ModelForm):
    upload_id = forms.IntegerField(widget=forms.HiddenInput)
    DELETE = forms.BooleanField(required=False)

    class Meta:
        widgets = {
            'title': admin_widgets.AdminTextInputWidget,
            'caption': CKEditorWidget(config_name='simple'),
            'tags': SmartTagSelect,
        }

    @property
    def media(self):
        parent_media = super().media
        extra = '' if settings.DEBUG else '.min'
        return forms.Media(
            js=[
                'admin/js/vendor/jquery/jquery{0}.js'.format(extra),
                'admin/js/jquery.init.js',
                'admin/js/core.js',
            ]
        ) + parent_media

    def __init__(self, *args, **kwargs):
        self.upload = kwargs.pop('upload')
        initial = kwargs.pop('initial', {})
        initial.update({
            'upload_id': self.upload.id,
            'title': title_from_filename(self.upload.original_filename),
        })
        kwargs.update({'initial': initial})

        super().__init__(*args, **kwargs)

        self.fields['title'].required = False

    def create_mediaitem(self):
        upload = self.upload
        media_class = Image if upload.media_type == 'image' else File
        data = self.cleaned_data
        mediaitem = media_class(
            uid=upload.uid,
            title=upload.original_filename,
            filename=upload.filename.name,
            filesize=upload.metadata['filesize'],
            original_filename=upload.original_filename,
            caption=data['caption'],
        )
        mediaitem = self.save_mediaitem(mediaitem)
        mediaitem.tags.add(*data['tags'])
        return mediaitem

    def save_mediaitem(self, mediaitem):
        raise NotImplementedError(
            "Method 'save_mediaitem' must be implemented on child class"
        )

    def clean(self):
        if self.cleaned_data.get('DELETE'):
            return self.cleaned_data
        if not self.cleaned_data.get('title'):
            raise forms.ValidationError({'title': 'This field is required'})
        return super().clean()

    def save(self):
        if self.cleaned_data.get('DELETE'):
            self.upload.delete()
            return None

        mediaitem = self.create_mediaitem()
        self.upload.delete(no_cleanup=True)
        return mediaitem


class MediaUploadImageForm(MediaUploadForm):
    class Meta(MediaUploadForm.Meta):
        model = Image
        fields = [
            'upload_id',
            'title',
            'alt_text',
            'caption',
            'tags',
        ]
        widgets = MediaUploadForm.Meta.widgets.copy()
        widgets.update({
            'alt_text': admin_widgets.AdminTextInputWidget,
        })

    def save_mediaitem(self, image):
        data = self.cleaned_data
        image.title = data['title']
        image.alt_text = data['alt_text']
        image.save()
        return image


class MediaUploadFileForm(MediaUploadForm):
    class Meta(MediaUploadForm.Meta):
        model = File
        fields = [
            'upload_id',
            'title',
            'caption',
            'tags',
        ]

    def save_mediaitem(self, mediaitem):
        mediaitem.save()
        return mediaitem


class MediaUploadFormset(list):

    def is_valid(self):
        return all([form.is_valid() for form in self])

    @transaction.atomic
    def save(self):
        instances = []
        # We save forms in reversed order to preserve upload ordering so most
        # recent media item is first
        for form in reversed(self):
            instances.append(form.save())
        return instances


def create_media_upload_formset(uploads, data=None):
    forms = []
    form_classes = {
        'image': MediaUploadImageForm,
        'file': MediaUploadFileForm,
    }

    for upload in uploads:
        form_class = form_classes[upload.media_type]
        form = form_class(
            upload=upload,
            prefix='upload-{0}'.format(upload.id),
            data=data,
        )
        forms.append(form)

    return MediaUploadFormset(forms)
