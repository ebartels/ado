from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.contrib import admin
from django import forms
from django.conf import settings
from django.contrib.admin.widgets import ForeignKeyRawIdWidget
from django.contrib.contenttypes.admin import GenericStackedInline
from django.contrib.contenttypes.forms import BaseGenericInlineFormSet
from webpack_loader import utils

from ..models import (
    MediaRelation,
    ImageRelation,
    FileRelation,
    VideoRelation)


class RelatedMediaForeignKeyWidget(ForeignKeyRawIdWidget):
    # Simplify the generated label to avoid extra database queries that aren't
    # used in our custom inline template.
    def label_and_url_for_value(self, value):
        pk = value
        try:
            url = reverse(
                '%s:%s_%s_change' % (
                    self.admin_site.name,
                    self.rel.model._meta.app_label,
                    self.rel.model._meta.object_name.lower(),
                ),
                args=(pk,)
            )
        except NoReverseMatch:
            url = ''  # Admin not registered for target model.

        return 'edit', url


class RelatedMediaFormset(BaseGenericInlineFormSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queryset = self.queryset.prefetch_related('item', 'object')


class RelatedMediaInlineMixin(object):
    template = 'admin/edit_inline/media_sort.html'
    classes = ['sortable', 'sortable-field__sort', 'mediaitem-inline']
    extra = 0
    raw_id_fields = ['item']

    @property
    def media(self):
        # add webpack assets
        webpack_js = utils.get_files('media_inlines', 'js', config='CUSTOMADMIN')
        webpack_css = utils.get_files('media_inlines', 'css', config='CUSTOMADMIN')

        extra = '' if settings.DEBUG else '.min'
        dependencies = ['admin/js/jquery.init.js', 'admin/js/inlines%s.js' % extra]

        super_media = super().media
        media = forms.Media(
            js=dependencies + [f['url'] for f in webpack_js],
            css={**super_media._css, **{'all': [f['url'] for f in webpack_css]}},
        )

        return super_media + media


class RelatedMediaInline(RelatedMediaInlineMixin, GenericStackedInline):
    model = MediaRelation
    formset = RelatedMediaFormset

    fieldsets = (
        ('', {
            'fields': ('item', 'sort',),
        }),
    )

    def __init__(self, *args, **kwargs):
        self.media_model = self.model.get_media_model()
        if not self.verbose_name:
            self.verbose_name = self.media_model._meta.verbose_name
        if not self.verbose_name_plural:
            self.verbose_name_plural = (self.media_model
                                            ._meta.verbose_name_plural)
        super().__init__(*args, **kwargs)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'item':
            kwargs['required'] = False
            kwargs['label'] = 'Add from media library'
        if db_field.name in self.raw_id_fields:
            kwargs['widget'] = RelatedMediaForeignKeyWidget(db_field.remote_field,
                                                            self.admin_site)
            return db_field.formfield(**kwargs)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class RelatedImagesInline(RelatedMediaInline):
    model = ImageRelation


class RelatedFilesInline(RelatedMediaInline):
    model = FileRelation


class RelatedVideosInline(RelatedMediaInline):
    model = VideoRelation


class BaseImagesInline(RelatedMediaInlineMixin, admin.StackedInline):
    """
    An admin inline for custom ImageRelation subclasses.
    """
    fieldsets = (
        ('', {
            'fields': ('item', 'sort',),
        }),
    )
