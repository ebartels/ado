import re
from functools import update_wrapper
from PIL import Image as PILImage

from django.contrib import admin
from django import forms
from django.contrib.admin import helpers
from django.contrib.admin.utils import unquote
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.conf import settings
from django.template.response import TemplateResponse
from django.utils.dateformat import format as date_format
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.decorators import method_decorator
from django.contrib.admin.widgets import AdminFileWidget
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django import http

from webpack_loader import utils
from polymorphic.admin import (
    PolymorphicParentModelAdmin,
    PolymorphicChildModelAdmin,
    PolymorphicChildModelFilter)
from taggit.models import Tag
from ado.customadmin.admin import BaseModelAdminMixin, MultiListFilter
from ado.customadmin.forms import TagEditForm
from ado.media import resizer

from ..models import (
    MediaItem,
    Image,
    File,
    Video,
    MediaRelation,
    MediaUpload,
)
from .forms import (
    FileAdminForm,
    ImageAdminForm,
    VideoAdminForm,
    create_media_upload_formset,
)
from ..filetypes import get_allowed_image_mime_types


csrf_protect_m = method_decorator(csrf_protect)
require_POST_m = method_decorator(require_POST)


class MediaTypeFilter(PolymorphicChildModelFilter):
    """
    Filter type for polymorphic media models
    """
    title = 'Type'
    parameter_name = 'media_type'

    def lookups(self, request, model_admin):
        result = super().lookups(request, model_admin)
        return [(item[0], item[1].title()) for item in result]


class TagListFilter(MultiListFilter):
    """
    Filter items by tag
    """
    title = 'Tag'
    parameter_name = 'tags'

    def get_tags(self, request, model_admin):
        # Get ctype for admin model
        if model_admin.model is not MediaItem:
            ctypes = [ContentType.objects.get_for_model(model_admin.model)]
        # For MediaItem admin, get ctypes for all media items
        else:
            ctypes = list(ContentType.objects.filter(app_label='media'))

        # Return tags that have been used by mediaitems
        tags = (Tag.objects
                .filter(taggit_taggeditem_items__content_type__in=ctypes)
                .order_by('name', 'id')
                .distinct())
        return tags

    def lookups(self, request, model_admin):
        tags = self.get_tags(request, model_admin)
        return [('untagged', 'Untagged')] + [('{}'.format(t.pk), t.name) for t in tags]

    def queryset(self, request, queryset):
        vals = self.value()
        untagged = 'untagged' in vals
        tag_ids = []
        for val in vals:
            try:
                tag_ids.append(int(val))
            except ValueError:
                pass

        if untagged:
            queryset = queryset.exclude(tags__isnull=False)
        elif tag_ids:
            for tag_id in tag_ids:
                queryset = queryset.filter(tags=tag_id)

        return queryset


class MediaItemAdminMixin(BaseModelAdminMixin):
    """
    Base admin mixin for polymorphic media models
    """
    ordering = ['-created']
    list_display = ['image_column', 'title', 'created_column']
    list_display_links = ['image_column', 'title']
    list_per_page = 50
    search_fields = ['id', 'title', 'caption', 'uid', 'tags__name']
    actions = ['tag_selected']
    html_editor_fields = [('caption', 'simple')]

    # Changelist columns
    @mark_safe
    def image_column(self, obj):
        if obj.source_image:
            resized = resizer.get_resized(obj.source_image, '200x200')
            return f'<img src="{resized.url}" alt="{obj}" width="120" />'
        if hasattr(obj, 'thumbnail_url'):
            return f'<img src="{obj.thumbnail_url}" width="120" alt="{obj.url}" />'

        return f'<i class="icon mime-icon material-icons" data-mimetype="{obj.mimetype}"></i>'
    image_column.short_description = 'Image'
    image_column.admin_order_field = 'source'

    def date_column(self, date):
        date = timezone.localtime(date, timezone.get_default_timezone())
        return '<span style="white-space:nowrap">%s</span><br/>%s' % (
            date_format(date, "M j, Y"),
            date_format(date, settings.TIME_FORMAT))

    @mark_safe
    def created_column(self, obj):
        return self.date_column(obj.created)
    created_column.short_description = 'Created'
    created_column.admin_order_field = 'created'

    def type_column(self, instance):
        return instance.subtype._meta.verbose_name.title()
    type_column.short_description = 'Type'
    type_column.admin_order_field = 'polymorphic_ctype'

    # Changelist actions
    def tag_selected(self, request, queryset):
        """
        Admin action to tag multiple media items
        """

        if request.POST.get('tags'):
            form = TagEditForm(request.POST)
            if form.is_valid():
                tags = form.cleaned_data['tags']
                action = form.cleaned_data['action']
                for item in queryset:
                    if action == 'remove':
                        item.subtype.tags.remove(*tags)
                    else:
                        item.subtype.tags.add(*tags)
                self.message_user(
                    request,
                    'Successfully updated tags for {0} items'.format(queryset.count())
                )
                return None
        else:
            form = TagEditForm()

        return TemplateResponse(
            request,
            'admin/media/tag_selected_action.html',
            {
                'form': form,
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
        )
    tag_selected.short_description = 'Tag selected items'

    @property
    def media(self):
        # add webpack assets
        webpack_js = utils.get_files('media_changelist', 'js', config='CUSTOMADMIN')
        webpack_css = utils.get_files('media_changelist', 'css', config='CUSTOMADMIN')

        super_media = super().media
        dependencies = ['admin/js/jquery.init.js']
        media = forms.Media(
            js=dependencies + [f['url'] for f in webpack_js],
            css={**super_media._css, **{'all': [f['url'] for f in webpack_css]}},
        )

        return super_media + media

    def get_urls(self):
        from django.urls import path
        urls = super().get_urls()

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        custom_urls = [
            path('upload/',
                 wrap(self.media_upload_view),
                 name='%s_%s_upload' % info),
            path('upload/edit/',
                 wrap(self.media_upload_edit_view),
                 name='%s_%s_upload_edit' % info),
            path('upload_file/',
                 wrap(self.media_upload_file_view),
                 name='%s_%s_upload_file' % info),
        ]

        return custom_urls + urls

    def media_upload_view(self, request, extra_context=None):
        opts = self.model._meta
        app_label = opts.app_label
        title = 'Bulk Upload'
        uploads = MediaUpload.objects.all()

        context = {
            'opts': opts,
            'title': title,
            'app_label': app_label,
            'modeladmin': self,
            'has_add_permission': self.has_add_permission(request),
            'has_change_permission': self.has_change_permission(request),
            'uploads': uploads,
        }
        context.update(extra_context or {})

        template_names = [
            'admin/%s/%s/media_upload.html' % (app_label, opts.model_name),
            'admin/%s/media_upload.html' % app_label,
        ]
        return render(request, template_names, context)

    @csrf_protect_m
    def media_upload_edit_view(self, request, extra_context=None):
        opts = self.model._meta
        app_label = opts.app_label
        title = 'Add uploaded files to Media Library'

        uploads = list(MediaUpload.objects.all())

        # Sort alpha-numerically
        # This is done so the ordering of mediaitems saved can be controlled by
        # setting filesnames with leading numbers.
        def sort_key(item):
            parts = re.split(r'\s+|_|-|\.', item.original_filename)
            if not parts:
                return (float('inf'), item.original_filename)
            return (
                (int(parts[0]), item.original_filename)
                if parts[0].isdigit() else
                (float('inf'), item.original_filename))

        uploads.sort(key=sort_key)

        if not uploads:
            return http.HttpResponseRedirect('..')

        if request.method == 'POST':
            formset = create_media_upload_formset(uploads, data=request.POST)
            if formset.is_valid():
                formset.save()
                url = reverse('admin:media_mediaitem_changelist')
                return http.HttpResponseRedirect(url)
        else:
            formset = create_media_upload_formset(uploads)
        if formset:
            media = formset[0].media
        else:
            media = None

        context = {
            'opts': opts,
            'title': title,
            'app_label': app_label,
            'modeladmin': self,
            'has_add_permission': self.has_add_permission(request),
            'has_change_permission': self.has_change_permission(request),
            'uploads': uploads,
            'formset': formset,
            'media': media,
        }
        context.update(extra_context or {})

        template_names = [
            'admin/%s/%s/media_upload_edit.html' % (app_label, opts.model_name),
            'admin/%s/media_upload_edit.html' % app_label,
        ]
        return render(request, template_names, context)

    @require_POST_m
    @csrf_protect_m
    def media_upload_file_view(self, request, extra_context=None):
        # Get form data
        try:
            filetype = request.POST['filetype']
            filename = request.POST['filename']
            upload_file = request.FILES['file']
        except KeyError as e:
            return http.HttpResponseBadRequest('bad form data: {0}'.format(e))

        # Check for recognized image type
        if filetype == 'image/svg+xml':
            mediatype = 'image'
        else:
            try:
                image = PILImage.open(upload_file)
                image.verify()
                filetype = PILImage.MIME.get(image.format)
                if filetype in get_allowed_image_mime_types():
                    mediatype = 'image'
                else:
                    mediatype = 'file'
            except IOError:
                mediatype = 'file'

        # Save media upload
        upload_file.seek(0)
        upload = MediaUpload(
            media_type=mediatype,
            original_filename=filename,
            metadata={
                'original_filename': filename,
                'filesize': upload_file.size,
                'filetype': filetype,
            },
        )
        upload.filename.save(filename, upload_file)

        return http.HttpResponse('ok')


@admin.register(MediaItem)
class MediaItemAdmin(MediaItemAdminMixin, PolymorphicParentModelAdmin):
    """
    Parent ModelAdmin for MediaItem class
    """
    icon = '<i class="icon material-icons">perm_media</i>'
    base_model = MediaItem
    child_models = [File, Image, Video]
    list_display = MediaItemAdminMixin.list_display + ['type_column']
    list_filter = ['created', 'modified', MediaTypeFilter, TagListFilter]

    def get_actions(self, request):
        # Override delete action so it doesn't say "All Media"
        actions = super().get_actions(request)
        delete_tuple = actions.get('delete_selected')
        if delete_tuple:
            actions['delete_selected'] = (
                delete_tuple[0],
                delete_tuple[1],
                'Delete selected Media',
            )
        return actions


class MediaItemChildAdminMixin(object):
    """
    Admin class mixin for child media items
    """
    show_in_index = True
    list_filter = ['created', 'modified', TagListFilter]
    fieldsets = [
        (None, {
            'fields': ('filename', 'title', 'caption',),
        }),
        ('Tags', {
            'fields': ('tags',),
        }),
    ]

    def get_fieldsets(self, request, obj=None):
        if hasattr(self, 'add_fieldsets') and not obj:
            return self.add_fieldsets
        return super().get_fieldsets(request, obj)

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        if db_field.name == 'filename':
            kwargs['widget'] = AdminFileWidget
            return db_field.formfield(**kwargs)

        return super().formfield_for_dbfield(db_field, request, **kwargs)

    def change_view(self, request, object_id, form_url='', extra_context=None):

        # Get related objects
        obj = self.get_object(request, unquote(object_id))
        if obj:
            related_accessors = [rel.get_accessor_name()
                                 for rel in obj._meta.related_objects]
            related_fields = [getattr(obj, accessor)
                              for accessor in related_accessors
                              if '+' not in accessor]
            related_objects = []
            for field in related_fields:
                if hasattr(field, 'all'):
                    for related_object in field.all():
                        if isinstance(related_object, MediaRelation):
                            related_object = related_object.object
                        opts = related_object._meta
                        try:
                            if hasattr(related_object, 'parent_admin_url'):
                                admin_url = related_object.parent_admin_url
                            else:
                                admin_url = reverse('admin:{0}_{1}_change'.format(
                                                        opts.app_label,
                                                        opts.model_name),
                                                    args=[related_object.pk])
                        except NoReverseMatch:
                            admin_url = ''

                        related_objects.append({
                            'object': related_object,
                            'opts': opts,
                            'admin_url': admin_url,
                        })

            if extra_context is None:
                extra_context = {}

            extra_context.update({
                'related_objects': related_objects,
            })

        return super().change_view(request, object_id, form_url, extra_context)


@admin.register(File)
class FileAdmin(MediaItemAdminMixin, MediaItemChildAdminMixin, PolymorphicChildModelAdmin):
    base_model = File
    icon = '<i class="icon material-icons">folder_open</i>'
    form = FileAdminForm
    search_fields = MediaItemAdminMixin.search_fields + ['filename']
    fieldsets = [
        (None, {
            'fields': ('filename', 'title', 'caption',),
        }),
        ('Tags', {
            'fields': ('tags',),
        }),
    ]
    add_fieldsets = [
        (None, {
            'fields': ('filename', 'file_url', 'title', 'caption'),
        }),
        ('Tags', {
            'fields': ('tags',),
        }),
    ]


@admin.register(Image)
class ImageAdmin(MediaItemChildAdminMixin, MediaItemAdminMixin, PolymorphicChildModelAdmin):
    base_model = Image
    icon = '<i class="icon material-icons">photo</i>'
    search_fields = MediaItemAdminMixin.search_fields + ['filename']
    form = ImageAdminForm
    fieldsets = [
        (None, {
            'fields': ('filename', 'title', 'alt_text', 'caption',),
        }),
        ('Tags', {
            'fields': ('tags',),
        }),
    ]
    add_fieldsets = [
        (None, {
            'fields': ('filename', 'file_url', 'title', 'caption'),
        }),
        ('Tags', {
            'fields': ('tags',),
        }),
    ]


@admin.register(Video)
class VideoAdmin(MediaItemChildAdminMixin, MediaItemAdminMixin, PolymorphicChildModelAdmin):
    base_model = Video
    icon = '<i class="icon material-icons">local_movies</i>'
    search_fields = MediaItemAdminMixin.search_fields + ['url']
    form = VideoAdminForm
    fieldsets = [
        (None, {
            'fields': ('url', 'title', 'video_image', 'caption', ),
        }),
        ('Tags', {
            'fields': ('tags',),
        }),
    ]
    add_fieldsets = [
        (None, {
            'fields': ('url', 'title', 'caption'),
        }),
        ('Tags', {
            'fields': ('tags',),
        }),
    ]
