import re
from django import http
from django.apps import apps
from django.utils.cache import patch_cache_control
from django.shortcuts import render, get_object_or_404
from django.contrib.admin.views.decorators import staff_member_required
from django.urls import reverse
from django.utils.datastructures import MultiValueDictKeyError
from ado.media import resizer

RE_WS = re.compile(r'\s*\n\s*')


def placeholder(request, width, height, **kwargs):
    """
    Returns a placeholder svg image.
    """
    bg = kwargs.get('bg', 'fff')
    fg = kwargs.get('fg', 'aaa')

    SVG = f"""
<svg width="{width}" height="{height}" xmlns="http://www.w3.org/2000/svg">
    <rect x="2" y="2" width="{width - 4}" height="{height - 4}" style="fill:#{bg};stroke:#{fg};stroke-width:2"/>
    <text x="50%" y="50%" font-size="18" text-anchor="middle" alignment-baseline="middle" font-family="sans-serif" fill="#{fg}">
    {width}&#215;{height}
    </text>
</svg>
    """ # noqa

    content = RE_WS.sub('', SVG.strip())
    resp = http.HttpResponse(content, content_type='image/svg+xml')
    patch_cache_control(resp, max_age=864000)
    return resp


@staff_member_required
def fetch_mediaitem_info(request):
    """
    Returns the url to a MediaItem instance
    """
    try:
        model_name = request.GET.get('model', 'media.mediaitem')
        pk = int(request.GET['pk'])
    except (MultiValueDictKeyError, ValueError, TypeError):
        return http.HttpResponseBadRequest('The "pk" param is required and was not provided')

    try:
        (app_label, model_name) = model_name.split('.')
    except ValueError:
        raise http.Http404

    model = apps.get_model(app_label, model_name)
    mediaitem = get_object_or_404(model, pk=pk)
    image = mediaitem.source_image
    thumb = None
    if image:
        thumb = resizer.get_resized(image, '360x300')

    opts = model._meta

    data = {
        'pk': pk,
        'title': mediaitem.title,
        'str': str(mediaitem),
        'type': opts.model_name,
        'mimetype': mediaitem.mimetype,
        'url': mediaitem.media_url,
        'image': image and image.url or None,
        'thumbnail': thumb and thumb.url or None,
        'admin_url': reverse('admin:media_{0}_change'.format(opts.model_name), args=[mediaitem.pk]),
    }
    return http.JsonResponse(data)


# TODO: fix these or remove
# Link & image browser for CKEDITOR
@staff_member_required
def link_browser(request):
    return render(request, 'admin/media/link_browser.html', {})


@staff_member_required
def image_browser(request):
    return render(request, 'admin/media/image_browser.html', {})
