from PIL import Image, ImageChops, ImageFilter
from sorl.thumbnail.conf import settings
from sorl.thumbnail.base import ThumbnailBackend as _ThumbnailBackend
from sorl.thumbnail.engines import pil_engine


ALLOWED_FORMATS = (
    'JPEG',
    'PNG',
    'GIF',
)


class ThumbnailBackend(_ThumbnailBackend):
    """
    A custom sorl thumbnail backend.
    """
    def _get_format(self, source):
        # restrict formats to ones with wide browser support.
        fmt = super()._get_format(source)
        return fmt if fmt in ALLOWED_FORMATS else 'JPEG'


# TODO: should we remove this?
class PILEngine(pil_engine.Engine):
    """
    A custom sorl thumbnail PIL engine that can handle autotrimming images.
    """
    def create(self, image, geometry, options):
        """
        Processing conductor, returns the thumbnail as an image engine instance
        """
        image = self.cropbox(image, geometry, options)
        image = self.orientation(image, geometry, options)
        image = self.autotrim(image, geometry, options)
        image = self.colorspace(image, geometry, options)
        image = self.remove_border(image, options)
        image = self.scale(image, geometry, options)
        image = self.crop(image, geometry, options)
        image = self.rounded(image, geometry, options)
        image = self.blur(image, geometry, options)
        image = self.padding(image, geometry, options)
        image = self.detail(image, geometry, options)
        return image

    def autotrim(self, image, geometry, options):
        """Automatically trims any white space from around an image"""
        if 'autotrim' not in options:
            return image

        # Get the color option
        # Allows numeric values of 0-255, & "transparent"
        color = options['autotrim']
        has_alpha = image.mode[-1] == 'A'
        try:
            color = int(color)
            if has_alpha and image.mode in ('RGB', 'RGBA'):
                color = (color, color, color)
        except ValueError:
            pass

        # Prepare image for comparison
        if has_alpha:
            im = Image.new('RGB', image.size, color)
            im.paste(image, image)
        else:
            im = image.convert('L')

        # Background
        if has_alpha:
            bg = Image.new('RGB', image.size, color)
        else:
            bg = Image.new('L', image.size, color)
        diff = ImageChops.difference(im, bg)
        bbox = diff.getbbox()
        if bbox:
            return image.crop(bbox)
        return image

    def detail(self, image, geometry, options):
        """detail & sharpen filters"""
        try:
            if 'detail' in options:
                return image.filter(ImageFilter.DETAIL)
            if 'sharpen' in options:
                return image.filter(ImageFilter.SHARPEN)
        except ValueError:
            if settings.THUMBNAIL_DEBUG:
                raise
        return image
