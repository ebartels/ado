from django.apps import AppConfig
from django_cleanup import cache, handlers
from ado.signals import _monkey_patch_queryset_with_bulk_signals
from ado.media.filetypes import _monkey_patch_allowed_image_file_extensions


class MediaConfig(AppConfig):
    name = 'ado.media'
    icon = '<i class="icon material-icons">perm_media</i>'
    verbose_name = 'Media Library'

    def ready(self):
        # monkeypatching
        _monkey_patch_queryset_with_bulk_signals()
        _monkey_patch_taggit_join_restriction()
        _monkey_patch_allowed_image_file_extensions()

        # media listeners
        from ado.media import listeners  # noqa

        # setup for django_cleanup (copied from django_cleanup.apps)
        cache.prepare()
        handlers.connect()


def _monkey_patch_taggit_join_restriction():
    """
    Fix taggit.managers ExtraJoinRestriction
    """
    # This fixes problem with using django-cachalot along with taggit. Some
    # queries use ExtraJoinRestriction, which does not have an 'rhs' property,
    # which cachalot attempts to access
    #
    # TODO: can remove this if the below bugs are fixed.
    # see: https://github.com/noripyt/django-cachalot/issues/121
    # see: https://github.com/noripyt/django-cachalot/pull/132
    from taggit.managers import ExtraJoinRestriction
    ExtraJoinRestriction.rhs = None
