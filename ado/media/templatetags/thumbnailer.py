import re
from urllib.parse import urlparse
from django.utils.encoding import smart_str
from django.template import Library, Node, NodeList, TemplateSyntaxError
from django.conf import settings

from ado.media.resizer import get_resized


register = Library()


RE_IMAG_PATH = re.compile(r'^' + urlparse(settings.MEDIA_URL).path)
KW_PAT = re.compile(r'^(?P<key>[\w]+)=(?P<value>.+)$')


class ThumbnailerTag(Node):
    error_msg = ('Syntax error. Expected: ``thumbnailer source geometry '
                 '[key1=val1 key2=val2...]``')

    def __init__(self, parser, token):
        bits = token.split_contents()
        if len(bits) < 2:
            raise TemplateSyntaxError(self.error_msg)

        self.geometry = parser.compile_filter(bits[1])

        self.options = []
        for bit in bits[2:]:
            m = KW_PAT.match(bit)
            if not m:
                raise TemplateSyntaxError(self.error_msg)
            key = smart_str(m.group('key'))
            expr = parser.compile_filter(m.group('value'))
            self.options.append((key, expr))

        self.nodelist = parser.parse(['endthumbnailer'])
        parser.delete_first_token()

    def render(self, context):
        try:
            return self._render(context)
        except Exception:
            if settings.DEBUG:
                raise
            return self.nodelist.render(context)

    def _render(self, context):
        from bs4 import BeautifulSoup
        geometry = self.geometry.resolve(context)
        options = {}
        for key, expr in self.options:
            noresolve = {'True': True, 'False': False, 'None': None}
            value = noresolve.get(str(expr), expr.resolve(context))
            if key == 'options':
                options.update(value)
            else:
                options[key] = value

        output = self.nodelist.render(context)

        soup = BeautifulSoup(output, 'html.parser')
        imgs = soup.findAll('img')
        for img in imgs:
            src = img.get('src').strip()
            if not src:
                continue
            src_url = urlparse(src)
            media_url = urlparse(settings.MEDIA_URL)
            if src.startswith('/') or (src_url.hostname == media_url.hostname):
                file_ = RE_IMAG_PATH.sub('', src_url.path)
                thumbnail = get_resized(file_, geometry, **options)
                img['src'] = thumbnail.url
                img['width'] = thumbnail.width
                img['height'] = thumbnail.height

        output = str(soup)
        return output

    def __repr__(self):
        return "<ThumbnailerNode>"


@register.tag
def thumbnailer(parser, token):
    """
    Generates thumbnails for all "local" images found within the tag.
    "Local" images are any images which are stored under settings.MEDIA_ROOT.
    This is useful when you want to scale any images within a block of content.

    Usage:
    {% thumbnailer "500x700" %}
     [html potentially containing images...]
    {% endthumbnailer %}
    """
    return ThumbnailerTag(parser, token)


class ThumbnailNode(Node):
    nodelist_empty = NodeList()
    child_nodelists = ('nodelist_file', 'nodelist_empty')
    error_msg = ('Syntax error. Expected: ``thumbnail source geometry '
                 '[key1=val1 key2=val2...] as var``')

    def __init__(self, parser, token):
        bits = token.split_contents()
        self.file_ = parser.compile_filter(bits[1])
        self.geometry = parser.compile_filter(bits[2])
        self.options = []
        self.as_var = None
        self.nodelist_file = None

        if bits[-2] == 'as':
            options_bits = bits[3:-2]
        else:
            options_bits = bits[3:]

        for bit in options_bits:
            m = KW_PAT.match(bit)
            if not m:
                raise TemplateSyntaxError(self.error_msg)
            key = smart_str(m.group('key'))
            expr = parser.compile_filter(m.group('value'))
            self.options.append((key, expr))

        if bits[-2] == 'as':
            self.as_var = bits[-1]
            self.nodelist_file = parser.parse(('empty', 'endthumbnail',))
            if parser.next_token().contents == 'empty':
                self.nodelist_empty = parser.parse(('endthumbnail',))
                parser.delete_first_token()

    def render(self, context):
        try:
            return self._render(context)
        except Exception:
            if settings.DEBUG:
                raise
            return self.nodelist.render(context)

    def _render(self, context):
        file_ = self.file_.resolve(context)
        geometry = self.geometry.resolve(context)
        options = {}
        for key, expr in self.options:
            noresolve = {'True': True, 'False': False, 'None': None}
            value = noresolve.get(str(expr), expr.resolve(context))
            if key == 'options':
                options.update(value)
            else:
                options[key] = value

        thumbnail = None
        if file_:
            thumbnail = get_resized(file_, geometry, **options)

        if not thumbnail:
            if self.nodelist_empty:
                return self.nodelist_empty.render(context)
            else:
                return ''

        if self.as_var:
            context.push()
            context[self.as_var] = thumbnail
            output = self.nodelist_file.render(context)
            context.pop()
        else:
            output = thumbnail.url

        return output

    def __repr__(self):
        return "<ThumbnailNode>"

    def __iter__(self):
        for node in self.nodelist_file:
            yield node
        for node in self.nodelist_empty:
            yield node


@register.tag
def thumbnail(parser, token):
    """
    Usage:
    {% thumbnail image.filename "500x700" [crop=1] as thumb %}
    <img src="{{thumb.url}}" width="{{thumb.width}}" height="{{thumb.height}}" />
    {% empty %}
    <p>no image</p>
    {% endthumbnail %}
    """
    return ThumbnailNode(parser, token)
