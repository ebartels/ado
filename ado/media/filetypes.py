import mimetypes

# Limit to extensions that can be input into sharp
ALLOWED_IMAGE_FILE_EXTENSIONS = [
    'jpg',
    'jpeg',
    'png',
    'webp',
    'tif',
    'tiff',
    'gif',
    'svg',
]

ALLOWED_IMAGE_MIME_TYPES = [
    'image/jpeg',
    'image/png',
    'image/webp',
    'image/tiff',
    'image/gif',
    'image/svg+xml',
]


def get_allowed_image_file_extensions():
    """
    Image file extensions that are allowed for media upload
    """
    return ALLOWED_IMAGE_FILE_EXTENSIONS


def get_allowed_image_mime_types():
    """
    Image mime types that are allowed for media upload
    """
    return ALLOWED_IMAGE_MIME_TYPES


def guess_mimetype(filename):
    """
    Gues the mimetype from the filename using mimetypes module
    """
    mtype = mimetypes.guess_type(filename)[0] or 'unknown'
    if mtype == 'unknown' and filename.endswith('.webp'):
        return 'image/webp'
    return mtype


def _monkey_patch_allowed_image_file_extensions():
    """
    monkeypatch django.core.validators.get_available_image_extensions

    We need to limit accepted image extensions further than what Pillow
    supports. Django's ImageField uses this to validate uploaded files, and
    monkeypatching seems like the easiest way to get this happening.
    """
    from django.core import validators

    # _get_available_image_extensions = validators.get_available_image_extensions

    def patch_get_available_image_extensions():
        return ALLOWED_IMAGE_FILE_EXTENSIONS

    validators.get_available_image_extensions = patch_get_available_image_extensions
