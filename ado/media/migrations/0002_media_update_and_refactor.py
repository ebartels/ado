# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-01 20:47
from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='file',
            options={'verbose_name': 'File', 'verbose_name_plural': 'Files'},
        ),
        migrations.AlterModelOptions(
            name='image',
            options={'verbose_name': 'Image', 'verbose_name_plural': 'Images'},
        ),
        migrations.AlterModelOptions(
            name='mediaitem',
            options={'verbose_name': 'Media Item', 'verbose_name_plural': 'All Media'},
        ),
        migrations.AlterModelOptions(
            name='video',
            options={'verbose_name': 'Video', 'verbose_name_plural': 'Videos'},
        ),
        migrations.RenameField(
            model_name='video',
            old_name='image',
            new_name='video_image',
        ),
        migrations.RemoveField(
            model_name='mediaitem',
            name='published',
        ),
        migrations.AddField(
            model_name='file',
            name='filesize',
            field=models.PositiveIntegerField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='image',
            name='filesize',
            field=models.PositiveIntegerField(editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='image',
            name='alt_text',
            field=models.CharField(blank=True, help_text='A short description of the image, useful for those with visual impairment', max_length=255),
        ),
        migrations.AlterField(
            model_name='mediaitem',
            name='caption',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='mediaitem',
            name='title',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='video',
            name='data',
            field=jsonfield.fields.JSONField(blank=True),
        ),
    ]
