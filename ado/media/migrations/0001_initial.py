# -*- coding: utf-8 -*-
from django.db import models, migrations
import ado.media.fields
import ado.media.models
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0001_initial'),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MediaItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, blank=True)),
                ('uuid', models.CharField(max_length=22, unique=True)),
                ('published', models.BooleanField(default=True, help_text='Publish on site.')),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('modified', models.DateTimeField(auto_now=True, db_index=True)),
                ('caption', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('mediaitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='media.MediaItem', on_delete=models.CASCADE)),
                ('filename', models.ImageField(help_text='Select an image file to upload.', height_field='height', width_field='width', upload_to=ado.media.models.image_upload_to)),
                ('width', models.PositiveIntegerField(editable=False)),
                ('height', models.PositiveIntegerField(editable=False)),
                ('alt_text', models.CharField(help_text='A short description for image alt tag', max_length=255, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('media.mediaitem',),
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('mediaitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='media.MediaItem', on_delete=models.CASCADE)),
                ('filename', models.FileField(help_text='Select a file to upload.', upload_to=ado.media.models.file_upload_to)),
            ],
            options={
                'abstract': False,
            },
            bases=('media.mediaitem',),
        ),
        migrations.CreateModel(
            name='MediaRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort', models.PositiveIntegerField(default=0, null=True, verbose_name='sort order', blank=True)),
                ('object_id', models.PositiveIntegerField(verbose_name='object id', db_index=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ('content_type', 'sort'),
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('mediaitem_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='media.MediaItem', on_delete=models.CASCADE)),
                ('url', models.URLField(help_text='Enter the URL for a video on vimeo or youtube (e.g. http://vimeo.com/VIDEO_ID/ or http://youtube.com/watch?v=VIDEO_ID)', max_length=255)),
                ('thumbnail_url', models.URLField(max_length=255, editable=False)),
                ('image', models.ImageField(help_text='Upload a custom image for this video.', null=True, upload_to=ado.media.models.image_upload_to, blank=True)),
                ('data', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('media.mediaitem',),
        ),
        migrations.AddField(
            model_name='mediarelation',
            name='item',
            field=models.ForeignKey(related_name='mediarelation_set', to='media.MediaItem', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='mediarelation',
            unique_together=set([('item', 'object_id', 'content_type')]),
        ),
        migrations.AddField(
            model_name='mediaitem',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_media.mediaitem_set+', editable=False, to='contenttypes.ContentType', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mediaitem',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='MediaSubRelation',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('media.mediarelation',),
        ),
        migrations.CreateModel(
            name='ImageRelation',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('media.mediasubrelation',),
        ),
        migrations.CreateModel(
            name='FileRelation',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('media.mediasubrelation',),
        ),
        migrations.CreateModel(
            name='VideoRelation',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('media.mediasubrelation',),
        ),
    ]
