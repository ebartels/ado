from operator import attrgetter

from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import connections
from django.db import models, router, DEFAULT_DB_ALIAS
from django.db.models.fields.related import (
    ManyToManyRel,
    RelatedField,
    lazy_related_operation,
)


class MediaGenericRelation(GenericRelation):
    def bulk_related_objects(self, objs, using=DEFAULT_DB_ALIAS):
        """
        Return all objects related to ``objs`` via this ``GenericRelation``.
        """
        bulk_related = super().bulk_related_objects(objs, using)

        media_model = self.remote_field.model.get_media_model()
        ctype = ContentType.objects.get_for_model(media_model)
        return bulk_related.filter(**{
            'item__polymorphic_ctype': ctype,
        })


class _RelatedMediaManager(models.Manager):
    def __init__(self, through, model, instance, prefetch_cache_name):
        super().__init__()
        self.through = through
        self.model = model
        self.instance = instance
        self.prefetch_cache_name = prefetch_cache_name

    def is_cached(self, instance):
        return self.prefetch_cache_name in instance._prefetched_objects_cache

    def get_queryset(self, extra_filters=None):
        try:
            return self.instance._prefetched_objects_cache[self.prefetch_cache_name]
        except (AttributeError, KeyError):
            kwargs = extra_filters if extra_filters else {}
            return self.through.media_for(self.model, self.instance, **kwargs)

    def get_prefetch_queryset(self, instances, queryset=None):
        from ado.media.models import GenericMediaRelationBase

        if queryset is not None:
            raise ValueError("Custom queryset can't be used for this lookup.")

        instance = instances[0]
        db = self._db or router.db_for_read(type(instance), instance=instance)

        fieldname = (
            "object_id"
            if issubclass(self.through, GenericMediaRelationBase)
            else "object"
        )
        fk = self.through._meta.get_field(fieldname)
        query = {
            "%s__%s__in"
            % (self.through.media_relname(), fk.name): {
                obj._get_pk_val() for obj in instances
            }
        }
        join_table = self.through._meta.db_table
        source_col = fk.column
        connection = connections[db]
        qn = connection.ops.quote_name
        qs = (
            self.get_queryset(query)
            .using(db)
            .extra(
                select={
                    "_prefetch_related_val": "{}.{}".format(
                        qn(join_table), qn(source_col)
                    )
                }
            )
        )
        return (
            qs,
            attrgetter('_prefetch_related_val'),
            lambda obj: obj._get_pk_val(),
            False,
            self.prefetch_cache_name
        )

    def _lookup_kwargs(self):
        return self.through.lookup_kwargs(self.instance)

    def add(self, *items):
        from ado.media.models import MediaRelation
        for item in items:
            if self.through is not MediaRelation:
                if not isinstance(item, self.through.get_media_model()):
                    raise ValueError("Can only add items of type %s" % self.through)
            self.through.objects.get_or_create(item=item,
                                               **self._lookup_kwargs())

    def set(self, *items):
        self.clear()
        self.add(*items)

    def remove(self, *items):
        self.through.objects.filter(**self._lookup_kwargs()).filter(
            item__in=items).delete()

    def clear(self):
        self.through.objects.filter(**self._lookup_kwargs()).delete()


class RelatedMediaField(RelatedField):
    """
    A Field that provides access to RelatedMediaManager, which allows any
    model class to manage a set of MediaItem instances (can change/add/remove).
    """
    # Field flags
    many_to_many = True
    many_to_one = False
    one_to_many = False
    one_to_one = False

    _related_name_counter = 0

    def __init__(
        self,
        verbose_name='Media Items',
        through=None,
        related_name=None,
        to='media.MediaItem',
        manager=_RelatedMediaManager,
    ):
        from ado.media.models import MediaRelation

        self.through = through or MediaRelation

        if related_name is None:
            related_name = 'reverse_related_{}s+'.format(
                to.replace('.', '_').lower(),
            )

        rel = ManyToManyRel(self, to, related_name=related_name, through=self.through)

        super().__init__(
            verbose_name=verbose_name,
            blank=True,
            null=True,
            serialize=False,
            rel=rel,
        )

        self.swappable = False
        self.manager = manager

    def __get__(self, instance, model):
        if instance is not None and instance.pk is None:
            raise ValueError(
                "%s objects need to have a primary key value "
                "before you can access media items." % model.__name__
            )

        return self.manager(
            through=self.through,
            model=model,
            instance=instance,
            prefetch_cache_name=self.name,
        )

    def deconstruct(self):
        """
        Deconstruct the object, used with migrations.
        """
        name, path, args, kwargs = super().deconstruct()
        # Remove forced kwargs.
        for kwarg in ('blank', 'null', 'serialize'):
            del kwargs[kwarg]
        # Add arguments related to relations.
        # Ref: https://github.com/jazzband/django-taggit/issues/206#issuecomment-37578676
        rel = self.remote_field
        if isinstance(rel.through, str):
            kwargs["through"] = rel.through
        elif not rel.through._meta.auto_created:
            kwargs["through"] = "{}.{}".format(
                rel.through._meta.app_label, rel.through._meta.object_name
            )

        related_model = rel.model
        if isinstance(related_model, str):
            kwargs["to"] = related_model
        else:
            kwargs["to"] = "{}.{}".format(
                related_model._meta.app_label, related_model._meta.object_name
            )

        return name, path, args, kwargs

    def contribute_to_class(self, cls, name):
        self.set_attributes_from_name(name)
        self.model = cls
        self.opts = cls._meta

        cls._meta.add_field(self)
        setattr(cls, name, self)

        # Set up "through" relation
        if not cls._meta.abstract:
            if isinstance(self.remote_field.model, str):

                def resolve_related_class(cls, model, field):
                    field.remote_field.model = model

                lazy_related_operation(
                    resolve_related_class, cls, self.remote_field.model, field=self
                )
            if isinstance(self.through, str):

                def resolve_related_class(cls, model, field):
                    self.through = model
                    self.remote_field.through = model
                    self.post_through_setup(cls)

                lazy_related_operation(
                    resolve_related_class, cls, self.through, field=self
                )
            else:
                self.post_through_setup(cls)

    def get_internal_type(self):
        return "ManyToManyField"

    def post_through_setup(self, cls):
        from ado.media.models import MediaSubRelation

        # Add to media registry
        if not cls._meta.abstract:
            opts = self.through._meta
            if not hasattr(opts, '_media_registry'):
                opts._media_registry = []
            if cls not in opts._media_registry:
                opts._media_registry.append(cls)

        self.use_gfk = self.through is None or issubclass(
            self.through, MediaSubRelation
        )

        if not self.remote_field.model:
            self.remote_field.model = self.through._meta.get_field(
                "item"
            ).remote_field.model

        if self.use_gfk:
            related_items = MediaGenericRelation(self.through)
            related_items.contribute_to_class(cls, "related_%s" % self.name)

        for rel in cls._meta.local_many_to_many:
            if rel == self or not isinstance(rel, RelatedMediaField):
                continue
            if rel.through == self.through:
                raise ValueError(
                    "You can only have one RelatedMediaField "
                    "per model using generic relations"
                )

    def __set__(self, instance, value):
        if instance is not None and instance.pk is None:
            raise ValueError("%s objects need to have a primary key value "
                             "before you can access media items." % type(instance).__name__)
        manager = self.__get__(instance, instance.__class__)
        manager.set(*value)

    def formfield(self, **kwargs):
        return None

    def value_from_object(self, obj):
        if obj.pk is None:
            return []
        qs = self.through.objects.select_related('item').filter(
            **self.through.lookup_kwargs(obj)
        )
        return [i.pk for i in qs]

    def related_query_name(self):
        return self.model._meta.model_name

    def m2m_reverse_name(self):
        return self.through._meta.get_field("item").column

    def m2m_reverse_field_name(self):
        return self.through._meta.get_field("item").name

    def m2m_target_field_name(self):
        return self.model._meta.pk.name

    def m2m_reverse_target_field_name(self):
        return self.remote_field.model._meta.pk.name

    def m2m_column_name(self):
        if self.use_gfk:
            return self.through._meta.private_fields[0].fk_field
        return self.through._meta.get_field('object').column

    def m2m_db_table(self):
        return self.through._meta.db_table

    def bulk_related_objects(self, new_objs, using):
        if self.use_gfk:
            return self.through._base_manager.db_manager(using).filter(
                **self.through.bulk_lookup_kwargs(new_objs))
        return []


class RelatedImagesField(RelatedMediaField):
    def __init__(self, **kwargs):
        defaults = dict(
            verbose_name='Images',
            through='media.ImageRelation',
            to='media.Image',
        )
        defaults.update(**kwargs)
        super().__init__(**defaults)


class RelatedFilesField(RelatedMediaField):
    def __init__(self, **kwargs):
        defaults = dict(
            verbose_name='Files',
            through='media.FileRelation',
            to='media.File',
        )
        defaults.update(**kwargs)
        super().__init__(**defaults)


class RelatedVideosField(RelatedMediaField):
    def __init__(self, **kwargs):
        defaults = dict(
            verbose_name='Videos',
            through='media.VideoRelation',
            to='media.Video',
        )
        defaults.update(**kwargs)
        super().__init__(**defaults)
