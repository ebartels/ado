from django.core.management.base import BaseCommand
from django.conf import settings

from ado.media.models import Image
from ado.media import resizer

DEFAULT_IMAGE_SIZES = (
    ('thumb', '200x200'),
    ('largethumb', '360x300'),
)

IMAGE_SIZES = getattr(settings, 'SCALED_IMAGE_SIZES', DEFAULT_IMAGE_SIZES)


class Command(BaseCommand):
    requires_system_checks = False
    args = "<size_arg size_arg>..."
    help = ("Generate thumbs for all media.Image instances using the given size arguments. "
            "Size arguments like: 100x100 100x90")

    def handle(self, *size_args, **options):
        if not size_args:
            size_args = [s[1] for s in IMAGE_SIZES]

        verbose = int(options.get('verbosity', 1)) > 1
        if verbose:
            print('Generating thumbs at {0}'.format(size_args))

        images = list(Image.objects.all())
        n = len(images)
        for i, im in enumerate(images):
            if verbose:
                print('')
                print('Image {0} of {1}'.format(i + 1, n))
            for size in size_args:
                # Generate the thumbnail
                if verbose:
                    print('Generating thumb: {0} {1}'.format(size,
                                                             im.filename.name))
                resizer.get_resized(im.filename, size)
