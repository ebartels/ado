from django.db import models

from ..models import MediaRelationBase
from ..fields.related import (
    RelatedMediaField,
    RelatedImagesField,
    RelatedFilesField)


# Test models
class RegularSet(models.Model):
    """A model with associated media"""
    title = models.CharField(max_length=20)
    images = RelatedImagesField()
    files = RelatedFilesField()
    media = RelatedMediaField()


class RegularSet2(models.Model):
    """A model with associated media"""
    title = models.CharField(max_length=20)
    images = RelatedImagesField()
    files = RelatedFilesField()
    media = RelatedMediaField()


class ChildSet(RegularSet):
    """A child model using multi-table inheritance"""
    notes = models.TextField()


class AbstractSet(models.Model):
    """An abstract class with associated media"""
    title = models.CharField(max_length=20)
    images = RelatedImagesField()
    files = RelatedFilesField()
    media = RelatedMediaField()

    class Meta:
        abstract = True


class AbsChildSet(AbstractSet):
    """A child of an abstract class"""
    description = models.TextField(blank=True)


class AltNameModel(models.Model):
    """A model with associated media using a different field name"""
    image_attachments = RelatedImagesField()


class TestCollectionImage(MediaRelationBase):
    """A model to be used as a 'through' model with RelatedMediaField"""
    object = models.ForeignKey(
        'TestImageCollection',
        on_delete=models.CASCADE,
        related_name='related_images',
    )


class TestImageCollection(models.Model):
    """A model with related media using a 'through' model association"""
    title = models.CharField(max_length=20)
    images = RelatedImagesField(through=TestCollectionImage)


class ModelWithFileField(models.Model):
    """Model for testing FileField & storage"""
    file = models.FileField(upload_to='files/')
