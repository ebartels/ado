import os
import shutil
import random
import string
import tempfile
from PIL import Image as PILImage, ImageDraw
from io import BytesIO
import factory
from unittest.mock import Mock, patch
import requests

from django.test import TestCase, TransactionTestCase, override_settings
from django.core.files.base import ContentFile
from django.contrib.contenttypes.models import ContentType

from ado.utils.mockutils import patchif
from ado.signals import post_bulk_delete, post_bulk_update
from ado.media.models import (
    MediaItem,
    Image,
    File,
    Video,
    MediaRelation,
    ImageRelation,
    FileRelation,
)
from ado.media.admin.forms import ImageAdminForm, MediaUploadImageForm
from ado.media import oembed
from ado.media.tests import models as test_models


# Set to False to disable mocking of network responses
ENABLE_MOCKS = True


IMAGE_FORMATS = [
    ('jpeg', 'jpeg'),
    ('png', 'png'),
    ('gif', 'gif'),
    ('tiff', 'tiff'),
    ('jpeg2000', 'jp2'),
    ('webp', 'webp'),
]


def make_image(size=(100, 100), name=None, format=None):
    """
    Return an image file with the given size, name, format
    """
    im = PILImage.new('L', size)
    draw = ImageDraw.Draw(im)
    random_text = ''.join(random.sample(string.ascii_letters, 10))
    draw.text((0, 0), random_text, fill=128)
    if format:
        ext = format
    else:
        format, ext = random.choice(IMAGE_FORMATS)
    name = name or 'test.{}'.format(ext)
    buf = BytesIO()
    im.save(buf, format=format)
    file_ = ContentFile(buf.getvalue(), name=name)
    buf.close()
    return file_


# Model factories
class ImageFactory(factory.DjangoModelFactory):
    class Meta:
        model = Image

    title = 'test image'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        instance = model_class(*args, **kwargs)
        image = make_image()
        instance.filename.save(image.name, image)
        return instance


class FileFactory(factory.DjangoModelFactory):
    class Meta:
        model = File

    title = 'test file'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        instance = model_class(*args, **kwargs)
        image = make_image()
        instance.filename.save(image.name, image)
        return instance


class MediaUploadFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'media.MediaUpload'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        instance = model_class(*args, **kwargs)
        image = make_image(name='test.jpg', format='jpeg')
        instance.original_filename = image.name
        instance.media_type = 'image'
        instance.metadata = {
            'original_filename': image.name,
            'filesize': image.size,
            'filetype': 'image/jpeg',
        }
        instance.filename.save(image.name, image)
        return instance


@override_settings(
    DEFAULT_FILE_STORAGE='ado.utils.storage.overwrite.OverwriteFileSystemStorage',
)
class OverwriteFileSystemStorageTest(TestCase):
    """
    Test OverwriteFileSystemStorage class
    """
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.MEDIA_ROOT = tempfile.mkdtemp(prefix='django_test')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        for subdir in ('files', 'images'):
            try:
                shutil.rmtree(os.path.join(cls.MEDIA_ROOT, subdir))
            except FileNotFoundError:
                pass
        os.rmdir(cls.MEDIA_ROOT)

    def test_file_field_overwrites_existing_file(self):
        with override_settings(MEDIA_ROOT=self.MEDIA_ROOT):
            # save file
            instance = test_models.ModelWithFileField()
            instance.file.save('test.jpg', make_image(format='jpeg'))
            self.assertEqual('files/test.jpg', instance.file.name)

            # save a new file
            instance.file.save('test.jpg', make_image(format='jpeg'))
            self.assertEqual('files/test.jpg', instance.file.name)


class PostBulkDeleteTest(TestCase):
    """
    Test that bulk delete signal is fired
    """
    def setUp(self):
        pass

    def test_post_bulk_delete_fired(self):
        ImageFactory()
        ImageFactory()

        receiver_called = False

        def test_receiver(sender, *args, **kwargs):
            nonlocal receiver_called
            receiver_called = True

        post_bulk_delete.connect(test_receiver)
        Image.objects.all().delete()
        self.assertTrue(receiver_called, 'post_bulk_delete signal not fired')
        post_bulk_delete.disconnect(test_receiver)


class PostBulkUpdateTest(TestCase):
    """
    Test that bulk update signal is fired
    """
    def setUp(self):
        pass

    def test_post_bulk_update_fired(self):
        ImageFactory()
        ImageFactory()

        receiver_called = False

        def test_receiver(sender, *args, **kwargs):
            nonlocal receiver_called
            receiver_called = True

        post_bulk_update.connect(test_receiver)
        Image.objects.all().update(caption='test')
        self.assertTrue(receiver_called, 'post_bulk_update signal not fired')
        post_bulk_update.disconnect(test_receiver)


class MediaItemTest(TestCase):
    """
    Test MediaItem model
    """
    def test_uid_available_before_save(self):
        for factory_cls in (FileFactory, ImageFactory):
            instance = factory_cls.build()
            self.assertTrue(instance.uid)
            uid = instance.uid
            instance.filename.save('test.jpeg', make_image())
            self.assertEqual(uid, instance.uid)


class FileBasedMediaItemTestMixin:
    """
    Base test cases for file-based MediaItems
    """
    def setUp(self):
        self.instance = self.Factory.create()

    def test_create(self):
        instance = self.instance

        self.assertTrue(
            instance.filename.storage.exists(instance.filename.name),
            "instance file was not saved properly."
        )

        self.assertTrue(instance.title)
        self.assertIn(instance.uid.hex, instance.filename.name)

        instance.delete()

    def test_filesize_saved(self):
        instance = self.Factory()
        self.assertTrue(instance.filesize)

    def test_generation_string_added_on_replace_file(self):
        instance = self.instance
        filename = instance.filename.name
        instance.filename.save(instance.filename.name, make_image(format='jpeg'))

        self.assertNotEqual(filename, instance.filename.name)
        self.assertIn(instance.uid.hex, filename)
        self.assertIn(instance.uid.hex, instance.filename.name)

    def test_delete_instance_deletes_file(self):
        instance = self.instance
        filename = instance.filename.name
        instance.delete()

        self.assertFalse(
            instance.filename.storage.exists(filename),
            "File was not deleted after instance deletion.",
        )

    def test_updating_filename_deletes_old_file_in_storage(self):
        # get original filename
        instance = self.Model.objects.get(pk=self.instance.pk)
        orig_name = instance.filename.name
        storage = instance.filename.storage
        self.assertTrue(storage.exists(orig_name))

        # Save a new file
        instance.filename.save('overwrite.jpg', make_image(format='jpeg'))
        new_name = instance.filename.name

        # The original file should be deleted
        self.assertNotEqual(orig_name, new_name)
        self.assertTrue(storage.exists(new_name))
        self.assertFalse(storage.exists(orig_name),
                         'The original file was not deleted after replacement')

    def test_mimetype(self):
        instance = self.Factory.build()
        im = make_image(format='jpeg')
        instance.filename.save('test_file.jpg', im)
        self.assertEqual(instance.mimetype, 'image/jpeg')

    def test_subtype(self):
        self.assertEqual(self.instance.subtype, self.instance)

        item = MediaItem.objects.all()[0]
        self.assertEqual(item.subtype, self.instance)

        item = MediaItem.objects.non_polymorphic()[0]
        self.assertEqual(item.subtype, self.instance)
        self.assertEqual(item.subtype, self.instance)

    def test_source_image(self):
        self.assertEqual(self.instance.source_image, self.instance.filename)

    def test_media_url(self):
        self.assertEqual(self.instance.media_url, self.instance.filename.url)

    def test_source_url(self):
        self.assertEqual(self.instance.source_url, self.instance.filename.url)

    def test_automatic_title(self):
        self.instance.title = ''
        self.instance.save()
        self.assertTrue(self.instance.title)


class ImageTest(FileBasedMediaItemTestMixin, TransactionTestCase):
    """
    Test Image model
    """
    Factory = ImageFactory
    Model = Image

    def test_image_dimensions_saved(self):
        image = ImageFactory.build()
        width = 213
        height = 107
        im = make_image(size=(width, height))
        image.filename.save(im.name, im)
        self.assertEqual(width, image.width)
        self.assertEqual(height, image.height)

    @patch('ado.media.resizer.get_resized')
    def test_resized_images_generated_on_save(self, get_resized):
        image = ImageFactory.create()
        get_resized.assert_any_call(image.filename, '360x300')
        get_resized.assert_any_call(image.filename, '200x200')

    def test_svg_images_are_supported(self):
        image = ImageFactory.build()
        svg_data = b"""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
        <path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"/>
        <path d="M0 0h24v24H0z" fill="none"/></svg>
        """.strip()  # noqa
        file_ = ContentFile(svg_data, name="test.svg")
        image.filename.save('test.svg', file_)
        self.assertEqual(image.width, 24)
        self.assertEqual(image.height, 24)
        self.assertEqual(image.mimetype, 'image/svg+xml')


class FileTest(FileBasedMediaItemTestMixin, TransactionTestCase):
    """
    Test File model
    """
    Factory = FileFactory
    Model = File

    def test_unknown_mimetype(self):
        file = FileFactory.build()
        file.filename.save('test file', ContentFile(''))
        self.assertEqual(file.mimetype, 'unknown')

    def test_source_image(self):
        self.assertEqual(self.instance.source_image, None)


# Video locations
VIMEO_VIDEO_URL = 'http://vimeo.com/7100569'
YOUTUBE_VIDEO_URL = 'http://www.youtube.com/watch?v=bDOYN-6gdRE'


# Mocking
def _mock_vimeo_request(*args, **kwargs):
    """
    Mock a Vimeo oembed response
    """
    return {
        'is_plus': '0',
        'provider_url': 'https://vimeo.com/',
        'description': 'Test description',
        'uri': '/videos/7100569',
        'title': 'Brad!',
        'video_id': 7100569,
        'html': '<iframe src="https://player.vimeo.com/video/7100569"></iframe>',
        'author_name': 'Casey Donahue',
        'height': 720,
        'thumbnail_width': 1280,
        'width': 1280,
        'upload_date': '2009-10-16 11:37:32',
        'version': '1.0',
        'author_url': 'http://vimeo.com/caseydonahue',
        'duration': 118,
        'provider_name': 'Vimeo',
        'thumbnail_url': 'http://i.vimeocdn.com/video/29412830_1280.jpg',
        'type': 'video',
        'thumbnail_height': 720
    }


mock_vimeo_request = Mock(side_effect=_mock_vimeo_request)


def _mock_youtube_request(*args, **kwargs):
    """
    Mock a Youtube oembed response
    """
    return {
        'provider_url': 'https://www.youtube.com/',
        'author_name': 'schmoyoho',
        'title': 'Test title',
        'type': 'video',
        'html': '<iframe src="https://www.youtube.com/embed/bDOYN-6gdRE?feature=oembed"></iframe>',
        'thumbnail_width': 480,
        'height': 344,
        'width': 459,
        'version': '1.0',
        'author_url': 'https://www.youtube.com/user/schmoyoho',
        'provider_name': 'YouTube',
        'thumbnail_url': 'https://i.ytimg.com/vi/bDOYN-6gdRE/hqdefault.jpg',
        'thumbnail_height': 360
    }


mock_youtube_request = Mock(side_effect=_mock_youtube_request)


def _mock_oembed_error_request(*args, **kwargs):
    """
    Raise OembedError on request
    """
    raise oembed.OembedError


mock_oembed_error_request = Mock(side_effect=_mock_oembed_error_request)


def _mock_404_request(*args, **kwargs):
    """
    Mock a 404 response
    """
    raise requests.RequestException()


mock_404_request = Mock(side_effect=_mock_404_request)


class BaseOembedTest:
    """Base class for Oembed Tests"""
    pass


class OembedErrorTest(BaseOembedTest, TestCase):
    """
    Test oembed error conditions
    """

    @patchif(ENABLE_MOCKS, 'requests.get',
             Mock(side_effect=requests.ConnectionError()))
    def test_bad_url(self):
        with self.assertRaises(oembed.OembedError):
            oembed.fetch('notaurl')

    @patchif.object(ENABLE_MOCKS, requests, 'get',
                    Mock(side_effect=mock_404_request))
    def test_bad_oembed_source(self):
        with self.assertRaises(oembed.OembedError):
            oembed.fetch('http://vimeo.com/blah')


@patchif(ENABLE_MOCKS, 'requests.get',
         Mock(side_effect=RuntimeError('no network allowed')))
@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
         mock_vimeo_request)
class VimeoOembedVideoTest(BaseOembedTest, TestCase):
    """
    Test Vimeo Oembed response
    """
    def test_vimeo_response(self):
        data = oembed.fetch(VIMEO_VIDEO_URL)
        self.assertTrue(data)


@patchif(ENABLE_MOCKS, 'requests.get',
         Mock(side_effect=RuntimeError('no network allowed')))
@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
         mock_youtube_request)
class YoutubeOembedVideoTest(BaseOembedTest, TestCase):
    """
    Test Youtube Oembed response
    """
    def test_youtube_response(self):
        data = oembed.fetch(YOUTUBE_VIDEO_URL)
        self.assertTrue(data)


def _mock_fetch_image():
    """Mock Video.fetch_image"""
    return make_image()


mock_fetch_image = Mock(side_effect=_mock_fetch_image)


@patchif(ENABLE_MOCKS, 'requests.get',
         Mock(side_effect=RuntimeError('no network allowed')))
@patchif.object(ENABLE_MOCKS, Video, 'fetch_image',
                Mock(side_effect=mock_fetch_image))
class BaseVideoTestCase:
    """Base test cases for videos  (vimeo, youtube, etc)"""

    def test_metadata(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.data)
        self.assertEqual(video.thumbnail_url,
                         video.data['thumbnail_url'])
        self.assertEqual(video.data, video.info)
        self.assertEqual(video.title, video.data['title'])

    def test_thumbnails(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.thumbnail)
        self.assertTrue(video.screenshot)
        self.assertTrue(video.large_screen)

    def test_mimetype(self):
        video = Video(url=self.video_url)
        self.assertEqual(video.mimetype, 'unknown')

    def test_subtype(self):
        video = Video.objects.create(url=self.video_url)
        self.assertEqual(video.subtype, video)
        item = MediaItem.objects.all()[0]
        self.assertEqual(item.subtype, video)
        item = MediaItem.objects.non_polymorphic()[0]
        self.assertEqual(item.subtype, video)
        self.assertEqual(item.subtype, video)

    def test_source_image(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.source_image)
        self.assertEqual(video.source_image, video.video_image)
        self.assertTrue(video.screenshot)

    def test_media_url(self):
        video = Video(url=self.video_url)
        self.assertEqual(video.media_url, None)

    def test_source_url(self):
        video = Video(url=self.video_url)
        self.assertEqual(video.source_url, None)

    def test_automatic_title(self):
        video = Video(url=self.video_url)
        video.title = ''
        video.save()
        self.assertTrue(video.title)

    def test_image_fetch(self):
        video = Video.objects.create(url=self.video_url)
        self.assertTrue(video.video_image)

    @patch('ado.media.resizer.get_resized')
    def test_resized_images_generated_on_save(self, get_resized):
        video = Video.objects.create(url=self.video_url)
        get_resized.assert_any_call(video.video_image, '360x300')
        get_resized.assert_any_call(video.video_image, '200x200')

    def test_updating_video_image_deletes_old_file_in_storage(self):
        video = Video.objects.create(url=self.video_url)
        video.video_image.save('test.jpg', make_image(format='jpeg'))

        # get original filename
        video = Video.objects.get(pk=video.pk)
        orig_name = video.video_image.name
        storage = video.video_image.storage
        self.assertTrue(storage.exists(orig_name))

        # Save a new file
        video.video_image.save('overwrite.jpg', make_image(format='jpeg'))
        new_name = video.video_image.name

        # The original file should be deleted
        self.assertNotEqual(orig_name, new_name)
        self.assertTrue(storage.exists(new_name))
        self.assertFalse(storage.exists(orig_name),
                         'The original file was not deleted after replacement')


@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
         mock_vimeo_request)
class VimeoVideoTest(BaseVideoTestCase, TransactionTestCase):
    """
    Test vimeo videos
    """
    video_url = VIMEO_VIDEO_URL


@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
         mock_youtube_request)
class YoutubeVideoTest(BaseVideoTestCase, TransactionTestCase):
    """
    Test youtube videos
    """
    video_url = YOUTUBE_VIDEO_URL


@patchif(ENABLE_MOCKS, 'ado.media.oembed.send_request',
         mock_oembed_error_request)
class VimeoOembedErrorTest(TestCase):
    """
    Test vimeo oembed errors
    """
    def test_oembed_error_on_create(self):
        with self.assertRaises(oembed.OembedError):
            Video.objects.create(url='http://vimeo.com/nopenotavideo')

    def test_oembed_error_on_save_new(self):
        with self.assertRaises(oembed.OembedError):
            video = Video(url='http://vimeo.com/nopenotavideo')
            video.save()

    def test_oembed_error_on_save_existing(self):
        video = Video.objects.create(
            url=VIMEO_VIDEO_URL,
            data=_mock_vimeo_request(),
            video_image=make_image(),
        )
        video.save()


class ImageRelationTest(TestCase):
    """
    Test ImageRelation model
    """
    def setUp(self):
        self.images = []
        for x in range(0, 5):
            self.images.append(ImageFactory.create())

    def _do_relation_test(self, set_class):
        set = set_class.objects.create(title='TESTING')

        # Add a single image
        image = Image.objects.all()[0]
        set.images.add(image)
        self.assertEqual(image.mediarelation_set.count(), 1, "Add one image failed.")
        self.assertEqual(set.related_images.count(), 1)

        # Add another image
        image2 = Image.objects.all()[1]
        set.images.add(image2)
        self.assertEqual(set.images.count(), 2, "Add a second image failed.")
        self.assertEqual(set.related_images.count(), 2)

        # Test assigning image set in one go
        random_images = Image.objects.all().order_by('?')
        set.images = random_images  # set using descriptor
        self.assertListEqual(
            list(random_images),
            list(set.images.all()),
            "Adding random images failed.",
        )

        # Test removing an image
        set.images.remove(image)
        self.assertFalse(image in set.images.all(), "Image was not removed properly.")

        # Tests that related object deletion also deletes the ImageRelation
        set.delete()
        self.assertEqual(image.mediarelation_set.count(), 0)
        self.assertEqual(ImageRelation.objects.count(), 0)

        # Images themselves should not be deleted
        self.assertEqual(Image.objects.count(), len(self.images), "Image count is incorrect.")

    def test_image_relations(self):
        self._do_relation_test(test_models.RegularSet)

    def test_abstract_subclasses(self):
        self._do_relation_test(test_models.AbsChildSet)

    def test_subclass(self):
        self._do_relation_test(test_models.ChildSet)

    def test_media_registry(self):
        for klass in (test_models.RegularSet,
                      test_models.ChildSet,
                      test_models.AbsChildSet):
            self.assertTrue(ImageRelation.relates_to(klass),
                            "%s.%s was not found in the list of related models." % (
                            klass._meta.app_label, klass._meta.object_name))
        self.assertFalse(ImageRelation.relates_to(test_models.AbstractSet))

    def test_alt_field_name(self):
        a = test_models.AltNameModel.objects.create()
        a.image_attachments = Image.objects.all()
        self.assertEqual(a.image_attachments.count(), len(self.images),
                         "Failed with alternate field name")

    def test_object_deletion(self):
        set1 = test_models.RegularSet.objects.create(title='test 1')
        set1.images = Image.objects.all()
        image_count = Image.objects.count()
        self.assertEqual(set1.images.count(), image_count)
        set1.delete()
        self.assertEqual(Image.objects.count(), image_count)
        self.assertEqual(ImageRelation.objects.count(), 0)

        set1 = test_models.RegularSet.objects.create(title='test 1')
        set1.images = Image.objects.all()
        set2 = test_models.RegularSet2.objects.create(title='test 2')
        set2.images = Image.objects.all()
        self.assertEqual(set1.images.count(), set2.images.count())
        self.assertEqual(ImageRelation.objects.count(),
                         Image.objects.count() * 2)

        image_count = Image.objects.count()
        test_models.RegularSet.objects.all().delete()
        self.assertEqual(set2.images.count(), image_count)
        self.assertEqual(Image.objects.count(), image_count)

        set2.delete()
        self.assertEqual(Image.objects.count(), image_count)
        self.assertEqual(ImageRelation.objects.count(), 0)

    def test_pk_required(self):
        set1 = test_models.RegularSet(title='test 1')
        with self.assertRaises(ValueError):
            set1.images.all()
        with self.assertRaises(ValueError):
            set1.images = Image.objects.all()

    def test_duplicate_items_ignored_on_save(self):
        set1 = test_models.RegularSet.objects.create(title='test 1')
        all_images = Image.objects.all()
        set1.images = all_images
        self.assertEqual(set1.images.count(), len(all_images))

        # Adding an existing image relation should be silently ignored, count
        # should remain the same.
        ImageRelation.objects.create(
                content_type=ContentType.objects.get_for_model(set1),
                object=set1,
                item=all_images[0])
        self.assertEqual(set1.images.count(), len(all_images))


class FileRelationTest(TestCase):
    """
    Test FileRelation model
    """
    def setUp(self):
        self.files = []
        for i in range(0, 5):
            self.files.append(FileFactory.create())

    def _do_relation_test(self, set_class):
        set = set_class.objects.create(title='TESTING')

        # Add a single file
        file = File.objects.all()[0]
        set.files.add(file)
        self.assertEqual(file.mediarelation_set.count(), 1, "Add one file failed.")
        self.assertEqual(set.related_files.count(), 1)

        # Add another file
        file2 = File.objects.all()[1]
        set.files.add(file2)
        self.assertEqual(set.files.count(), 2, "Add a second file failed.")
        self.assertEqual(set.related_files.count(), 2)

        # Test assigning file set in one go
        random_files = File.objects.all().order_by('?')
        set.files = random_files  # set using descriptor
        self.assertListEqual(
            list(random_files),
            list(set.files.all()),
            "Adding random files failed.",
        )

        # Test removing a file
        set.files.remove(file)
        self.assertFalse(file in set.files.all(), "File was not removed properly.")

        # Tests that related object deletion also deletes the FileRelation
        set.delete()
        self.assertEqual(file.mediarelation_set.count(), 0)
        self.assertEqual(FileRelation.objects.count(), 0)

        # Files themselves should not be deleted
        self.assertEqual(File.objects.count(), len(self.files), "File count is incorrect.")

    def test_file_relations(self):
        self._do_relation_test(test_models.RegularSet)

    def test_abstract_subclasses(self):
        self._do_relation_test(test_models.AbsChildSet)

    def test_subclass(self):
        self._do_relation_test(test_models.ChildSet)

    def test_media_registry(self):
        for klass in (test_models.RegularSet,
                      test_models.ChildSet,
                      test_models.AbsChildSet):
            self.assertTrue(FileRelation.relates_to(klass),
                            "%s.%s was not found in the list of related models." % (
                                klass._meta.app_label, klass._meta.object_name
                            ))
        self.assertFalse(FileRelation.relates_to(test_models.AbstractSet))


class RelatedMediaFieldsTestCase(TestCase):
    """
    Test multiple related media fields on a single model.
    """
    set_class = test_models.RegularSet

    def setUp(self):
        self.files = []
        for i in range(0, 5):
            self.files.append(FileFactory.create())

        self.images = []
        for i in range(0, 5):
            self.images.append(ImageFactory.create())

    def test_multi_media_types(self):
        set = self.set_class.objects.create(title='TESTING')

        set.files = File.objects.all()
        self.assertEqual(set.files.count(), File.objects.count())
        self.assertEqual(set.files.count(), len(self.files))

        set.images = Image.objects.all()
        self.assertEqual(set.images.count(), Image.objects.count())
        self.assertEqual(set.images.count(), len(self.images))

        self.assertEqual(set.media.count(), len(self.images) + len(self.files))

        self.assertEqual(set.related_images.count(), len(self.images))
        self.assertEqual(set.related_files.count(), len(self.files))

        set.images.clear()
        self.assertEqual(set.images.count(), 0)
        self.assertEqual(len(set.images.all()), 0)
        self.assertEqual(set.files.count(), len(self.files))
        self.assertEqual(set.media.count(), len(self.files))

        set.images = Image.objects.all()
        self.assertEqual(set.media.count(), len(self.files) + len(self.images))

        set.media.clear()
        self.assertEqual(set.media.count(), 0)
        self.assertEqual(set.files.count(), 0)
        self.assertEqual(set.images.count(), 0)
        self.assertEqual(MediaRelation.objects.count(), 0)

        set.media.add(*tuple(Image.objects.all()))
        self.assertEqual(set.media.count(), len(self.images))
        self.assertEqual(set.images.count(), len(self.images))

        set.media.add(*tuple(File.objects.all()))
        self.assertEqual(set.media.count(), len(self.images) + len(self.files))
        self.assertEqual(set.files.count(), len(self.files))

        set.media.set(*tuple(Image.objects.all()))
        self.assertEqual(set.media.count(), len(self.images))
        self.assertEqual(set.images.count(), len(self.images))
        self.assertEqual(set.files.count(), 0)

    def test_bulk_related(self):
        set = self.set_class.objects.create(title='TESTING')
        set.images = Image.objects.all()
        self.assertEqual(set.related_images.count(), Image.objects.count())

        image_relation = set._meta.get_field('related_images')
        related_objects = image_relation.bulk_related_objects([set])
        self.assertEqual(len(related_objects), set.related_images.count())

        file_relation = set._meta.get_field('related_files')
        related_objects = file_relation.bulk_related_objects([set])
        self.assertEqual(len(related_objects), 0)

    def test_add_wrong_types(self):
        set = self.set_class.objects.create(title='TESTING')

        # Can't add the wront types
        self.assertRaises(ValueError, set.images.set, File.objects.all()[0])
        self.assertRaises(ValueError, set.files.add, Image.objects.all()[0])


class MultipleMediaAbstractTestCase(RelatedMediaFieldsTestCase):
    """
    Test related media fields on abstract model class
    """
    set_class = test_models.AbsChildSet


class CustomThroughModelTestCase(TestCase):
    """
    Test using a custom 'through' model for RelatedImagesField
    """
    set_class = test_models.TestImageCollection

    def setUp(self):
        self.files = []
        for i in range(0, 5):
            self.files.append(ImageFactory.create())

    def test_image_relations(self):
        collection = self.set_class.objects.create(title='TESTING')

        # Add a single image
        image = Image.objects.all()[0]
        collection.images.add(image)
        self.assertEqual(collection.images.count(), 1)
        self.assertEqual(test_models.TestCollectionImage.objects.count(), 1)

        # Add another image
        image2 = Image.objects.all()[1]
        collection.images.add(image2)
        self.assertEqual(collection.images.count(), 2)
        self.assertEqual(collection.related_images.count(), 2)

        # Test assigning image set in one go
        random_images = Image.objects.all().order_by('?')
        collection.images = random_images
        self.assertListEqual(
            list(random_images),
            list(collection.images.all()),
            "Adding random images failed.",
        )

        # Test removing an image
        collection.images.remove(image)
        self.assertFalse(image in collection.images.all(), "Image was not removed properly.")

        # Delete collection
        collection.delete()
        self.assertEqual(test_models.TestCollectionImage.objects.count(), 0)

        # Images themselves should not be deleted
        self.assertEqual(Image.objects.count(), len(self.files), "Image count is incorrect.")


class MediaTagTest(TestCase):
    """
    Test MediaItem tagging
    """
    def test_untagged_queryset(self):
        file = FileFactory.create()
        image = ImageFactory.create()
        untagged = MediaItem.objects.all().exclude(tags__isnull=False)
        self.assertEqual(len(untagged), 2)

        image.tags.add('testtag')
        untagged = MediaItem.objects.all().exclude(tags__isnull=False)
        self.assertEqual(len(untagged), 1)

        file.tags.add('testtag2')
        untagged = MediaItem.objects.all().exclude(tags__isnull=False)
        self.assertEqual(len(untagged), 0)


# Testing thumbnail deletion
class ThumbnailDeletionTest(TransactionTestCase):
    """
    Test thumbnail deletion/cleanup works
    """
    @patch('ado.media.resizer.delete_resized')
    def test_delete_image_deletes_thumbs(self, delete_resized):
        image = ImageFactory.create()
        image.delete()
        delete_resized.assert_called_once()

    @patch('ado.media.resizer.delete_resized')
    def test_thumbnail_not_deleted_if_image_file_not_changed(self, delete_resized):
        image = ImageFactory.create()
        image = Image.objects.get(pk=image.pk)
        image.save()
        delete_resized.assert_not_called()


class ImageAdminFormTest(TestCase):
    """
    Test ImageAdminForm
    """
    @patch('ado.media.resizer.delete_resized')
    def test_replace_image_deletes_thumbs(self, delete_resized):
        # Create image & thumb
        image = ImageFactory.build()
        image.filename.save('test.jpeg', make_image(format='jpeg'))

        # Form data
        form_files = {
            'filename': make_image(format='jpeg'),
        }
        form_data = {
            'title': image.title,
            'caption': '',
            'alt_text': '',
            'tags': '',
        }

        # Save form without a new file
        form = ImageAdminForm(instance=image, data=form_data)
        self.assertTrue(form.is_valid(), form.errors)
        form.save(commit=True)
        delete_resized.assert_not_called()

        # Save new file with form
        form = ImageAdminForm(instance=image, data=form_data, files=form_files)
        self.assertTrue(form.is_valid(), form.errors)
        new_image = form.save(commit=True)
        self.assertEqual(image.filename.name, new_image.filename.name)

        # Thumb should have been deleted
        delete_resized.assert_called_once()


class MediaUploadTest(TransactionTestCase):
    """
    Test MediaUpload model and forms
    """
    def test_create(self):
        upload = MediaUploadFactory.create()
        self.assertTrue(upload.filename)
        self.assertTrue(upload.media_type)
        self.assertTrue(upload.metadata)
        self.assertTrue(upload.metadata['original_filename'])
        self.assertTrue(upload.metadata['filesize'])
        self.assertTrue(upload.metadata['filetype'])

    def test_delete_deletes_file(self):
        upload = MediaUploadFactory.create()
        self.assertTrue(upload.filename.storage.exists(upload.filename.name))

        upload.delete()
        self.assertTrue(upload.filename.name)
        self.assertFalse(upload.filename.storage.exists(upload.filename.name))

    def test_delete_with_no_cleanup(self):
        upload = MediaUploadFactory.create()
        self.assertTrue(upload.filename.storage.exists(upload.filename.name))

        filename = upload.filename.name
        upload.delete(no_cleanup=True)
        self.assertTrue(filename)
        self.assertTrue(upload.filename.storage.exists(filename))

    def test_upload_admin_form_image(self):
        upload = MediaUploadFactory.create()
        self.assertTrue(upload.filename.storage.exists(upload.filename.name))

        form = MediaUploadImageForm(upload=upload, data={
            'upload_id': upload.id,
            'title': 'Test',
            'caption': '',
            'alt_text': '',
            'tags': '',
        })
        self.assertTrue(form.is_valid(), form.errors)
        image = form.save()

        self.assertTrue(image.id)
        self.assertTrue(image.filename.storage.exists(image.filename.name),
                        'Image does not exist after upload form save.')
